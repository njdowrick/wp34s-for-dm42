# WP34S calculator for the DM42(n) &mdash; a project to bring the WP34S calculator firmware to the DM42(n) hardware.

[TOC]

For information on the latest updates, click [here](#recent-changes).

## Introduction

The WP34S originally existed as firmware for the HP-20b and HP-30b
financial calculators, written by a small but enthusiastic group of
regulars on the HP Calculator Forum. Their aim was to produce the best
RPN programmable calculator that the hardware would support &mdash; by any
measure, they succeeded.

The HP-20b and HP-30b calculators are no longer manufactured, but the
WP34S lives on &mdash; on desktop computers, as an iOS app, an Android app,
and now on the DM42 hardware. With a few exceptions I have not tried
to extend the features of the original calculator, which was already
excellent: there are other projects (C43/47, db48x, the DM42 itself) that aim to exploit the
DM42 hardware to the full. My aim was to give the original WP34S,
which I loved, the benefit of the greatly improved display
and keyboard of the DM42. If you also love the WP34S, or you're simply curious,
this may be for you.

The original WP34S required a keyboard overlay and key stickers. I've
tried to design this program so that if you are familiar with the
original calculator you should be able to use it with the standard
DM42 faceplate, without an overlay (and certainly without
stickers!). If you haven't used the WP34S previously you should still
be able to find your way around it well enough to get things done.

This does mean that if you have permanently changed the faceplate of
your DM42 by adding an adhesive overlay for the C43, C47. or db48x,
you won't find it so easy to use the WP34S program. In fact the
unshifted and yellow-shifted functions on the C47 and db48x differ
only slightly from those on the DM42 original so you may still be ok. The C43
keyboard layout is probably too different for this program to be
usable without an overlay.

This project is now essentially complete. You can download the `.pgm`
file (about 250k) and the help file, copy them to your DM42, run
the program, and it will work. No QSPI code is required, so it won't
conflict with any of the other calculator programs that you may be
using. I'm unlikely to add any features, but I will do my best
to fix any bugs.

## Description and Documentation

This version of the WP34S calculator includes essentially all of the
functions of the original calculator, along with some extras allowed
by the hardware of the DM42. Specifically, it's got:

- Infra-red printing;

- A stopwatch;

- Complex Lock mode;

- Entry RPN;

- User-defined menus;

- File handling, including the ability to save and load the calculator
  state and programs;

- An update of the double exponential integration routine.

There is documentation: 

- If you are already familiar with the WP34S
there's a DM42-specific [help file](./help/wp34s_help.md) accessible
from the calculator, and an incredibly good [Quick Reference
Guide](./help/QRG-booklet_1.2.pdf) that briefly describes how this
DM42 version works. 

- If you need to learn about the features of the
original WP34S (or to remind yourself of what you once knew!) you should look
through the excellent original [WP34S manual
(V3.1)](./help/WP34S_Manual_V3_1.pdf), and a very friendly booklet called [WP34S
Beginner's Guide](./help/WP34S_Beginners_Guide.pdf).

- Both sets of users might wish to read [`Using.md`](./help/Using.md), which describes the extra features of this WP34S program in more detail.

All of the help documentation is available in the
[help/](./help/) folder.

## How the WP34S functions are laid out on the DM42 keyboard

A basic principle is that all unshifted keys on the DM42 keyboard do
exactly what they say. This is also true of the yellow-shifted keys on
the two rows below the menu keys (except for COMPLEX - see later),
along with ALPHA, LAST x, BST, SST, and OFF. For
example, pressing the yellow shift key followed by the `SIN` key carries
out the `ASIN` function. So
if you simply want to do some RPN calculations, scientific or
otherwise, you can go right ahead and it should all work.

But the WP34S is so much more than just a basic scientific
calculator. Its keyboard alone has many more
functions than most scientific calculators (far more than a single shift key could access), and on top of this
there are several function "catalogues", together containing many hundreds of operations. 

To make all of this work with the DM42 keyboard:

- There are two shift states - press the yellow key once for f-shift,
  twice for g-shift, and once more to cancel the shift state.

- The original keyboard functions have been collected into groups and
  made available as _menus_. A menu makes a group of related functions
  available using the DM42 menu keys.

- Most menus appear on f-shifted keys. The menu attached to
  each key is related to the shifted function of that key as printed
  on the DM42 keyboard. For example, `f` `PROB` brings up the
  Probability menu; `f` `PRINT` brings up the Printing menu.

- The WP34S catalogues nearly all appear on g-shifted keys, and once 
  again they are placed so that the keyboard text gives a clue to
  which catalogue is located there. For
  example, `g` `MODES` brings up the Modes catalogue; `g` `STAT`
  brings up the Statistics catalogue.

Given these rules, accessing the large function set of the WP34S is a
lot easier that you might expect, even without an overlay. Here is a link to an [image of the keyboard layout](./help/wp34s_layout_help.md) which shows how these rules work out in practice.

For more details about using the calculator &mdash; the differences between this program and the original calculator, and
the added features that this program has &mdash;  see [`Using.md`](./help/Using.md).

## Versions

There are three versions of the WP34S program which differ only in their
displays: "normal", "long", and "top".

-   __The "normal" version__ reproduces the original WP34S screen as
    closely as possible, apart from the necessary addition of a menu
    and the helpful re-naming of various annunciators (e.g., 360 to DEG) which is common to all three versions.
    This display is fully compatible with programs written for the original calculator.

<img src="help/screens/normal01.png" width="40%"></img>&nbsp;&nbsp;
<img src="help/screens/normal02.png" width="40%"></img>



<img src="help/screens/normal03.png" align="left" width="40%"></img>

<p></p>

- **The "long" version** has a longer alpha display. This allows a fuller display of the y-register contents (when this is turned on) and longer alpha prompts. It should still be fully compatible with programs written for the original calculator, but displays might not look identical.

<img src="help/screens/long01.png" width="40%">&nbsp;&nbsp;
<img src="help/screens/long02.png" width="40%">
<p></p>
<img src="help/screens/long03.png" align="left" width="40%">

<p></p>

-   **The "top" version** includes the longer alpha display and adds a
    short "top row", used to display the annunciators. This keeps the long alpha register free for Y-register display or long alpha prompts. I'm not aware of any programs that have problems with this version, but it isn't impossible.

<img src="help/screens/top01.png" width="40%">&nbsp;&nbsp; 
<img src="help/screens/top02.png" width="40%">
<p></p>
<img src="help/screens/top04.png" align="left" width="40%">

<p></p>

Details of the renamed annunciators can be found in the [help file](./help/wp34s_help.md#annunciators).

All versions run at the same speed and should be compatible with
the vast majority of existing software. I recommend the "top" version;
it looks nicer. If exact compatibility with older software matters,
you can always use "normal". (All three versions share the same memory files, so you can exit one and start the other with no problem.)

For each version you can choose to have either 2k or 4k of user
memory. Neither sounds like a lot, but with the 16k of library
memory (8k program steps!) you are really
unlikely to run out of space. (WP34S programs in library memory can be
executed without being loaded into user memory.) The advantage of the
2k version is that it's the same size as all other implementations of
the WP34S, so in principle copies of user memory can be exchanged
between them.

Note that the 2k and 4k versions use different files for user and backup memory; they do, however, share the same library memory file.

The `.pgm` files for the different versions are:

- 2k user memory, "normal" display: [`build/wp34s.pgm`](./build/wp34s.pgm)

- 2k user memory, "long" display: [`build/wp34s_long.pgm`](./build/wp34s_long.pgm)

- 2k user memory, "top" display: [`build/wp34s_top.pgm`](./build/wp34s_top.pgm)

- 4k user memory, "normal" display: [`build/wp34s_4k.pgm`](./build/wp34s_4k.pgm)

- 4k user memory, "long" display: [`build/wp34s_long_4k.pgm`](./build/wp34s_long_4k.pgm)

- 4k user memory, "top" display: [`build/wp34s_top_4k.pgm`](./build/wp34s_top_4k.pgm)

All of these files can be downloaded from the repository. Make sure you download them from the master branch.

Note that there are now `.pg5` files available (for the DM42n) with the same names as above but with a `.pg5` extension.

## To install the WP34S on your DM42 calculator:

- Make sure that the DM42 firmware file (or the firmware file of your
  usual calculator program) is on your calculator's flash drive, so that you
  can reload it when you choose to exit the WP34S firmware.

- Make sure that your DMCP version is up-to-date. This normally
  happens automatically when you update your DM42 version. (Unless your DMCP file is really old this shouldn't be a problem.)

- Choose the display style / memory size that you want to use (hint:
  top, 2k) and download the correct `.pgm` file (linked above) to your computer.

- Also download the calculator help file
  [help/wp34s_help.html](./help/wp34s_help.html) to your computer.

- Connect your calculator to your computer using a USB cable. Get into
  the DMCP menu on your DM42 and choose option 6, "Activate USB
  drive".
> The exact procedure for reaching the DMCP menu will depend on which calculator software you are running. For an unmodified DM42 you should first save your calculator's state. Then press SHIFT SETUP; choose option 5 (System menu); choose option 4 (Reset to DMCP menu); then choose option 6 (Activate USB drive).

- Copy the `.pgm` file you have chosen from your computer to the
  top folder of your DM42's flash drive.

- Copy the file [help/wp34s_help.html](./help/wp34s_help.html) to the
  HELP folder on your DM42's flash drive.

- Eject the calculator from your computer (do **not** eject
  it from the calculator end). Leave it connected to your computer for now
  to power the loading of the new program.

- From the DMCP menu on your DM42, select option **3 - Load Program**,
  select the `.pgm` file you wish to run, and follow the prompts. You
  should then find yourself with a WP34S calculator! (You can now unplug the USB cable.)

- The help file that you have copied can be viewed from the calculator by
  pressing `f` `SETUP` and then the menu key labelled HELP.

- **To exit the calculator**, for example to reload the DM42
    firmware, press `f` `SETUP` and then press `EXIT` (the leftmost menu key).
    This brings you back to the DMCP menu; from here you can reload
    `DM42-3.22.pgm` (or a different `.pgm` file if you wish) using menu
    option 3. Exiting the program in this way saves the WP34S memory regions (user, backup, and library)
    to files on disk, so that when you reload the WP34S program you can carry on from where you left off.

## Building the project

If you wish to build the project yourself, note that the makefiles have been written for use with Linux only. I was unable to get things to work on Windows, whereas the experience on Linux (a VirtualBox virtual machine running Ubuntu, and also a Ubuntu machine running in WSL2) was relatively painless. 

The following instructions rebuild everything from scratch.

- Go into the `src/` directory and type `make clean`. Return to the top-level directory.

- In the top-level directory, type `make source`. This builds autogenerated files (constants, catalogues, etc.) that are compiled and/or run on the host computer; the makefile at the top level uses these and the rest of the source code to build libraries and code for the DM42 hardware.

- Then, type either `make normal`, `make long`, or `make top` to build the 2k version of your choice. If you want more than one version you have to run `make` twice; typing just `make` is the same as `make top` (but see below for a quick way to make all three versions).

- To build the 4k versions, add `-f Makefile_4k` to the make commands. The extra memory is used exclusively for extra program steps; extra memory registers would require too much change to the code. It does seem to work but I have not tested it so thoroughly. Search for "4k" in the list of updates below.

- There are also `makeall_34s` and `makeall_4k` commands that do all of this for you. Typing `./makeall_34s && ./makeall_4k` builds all six versions in one go.

- There are now script files `./do_DM42` and `./do_DM42n` files in the root folder. Executing `./do_DM42n` copies the DMCP5 files to `dmcp/` and the correct makefiles and linker script too, so that `make`, `makeall_34s`, and `makeall_4k` will make `.pg5` program files for the DM42n. Executing `./do_DM42` reverses these changes.

---

## Recent changes

- Range reduction code for trigonometric functions in RAD mode has been extended. Previously the angle was only reduced modulo 2&pi;; now further code ensures that the angle passed to the Taylor series function lies between 0 and &pi;/4, making convergence much quicker (as already happens in DEG and GRAD modes). Accuracy should not have been compromised.

- Slight changes to the user menu system: 

  - Menu "program" files can now end with `RTN` as with `END`. This lets menus used by a program to be loaded and deleted along with that program in one operation.

  -  Calling a user menu twice no longer returns to the default menu - this lets one user menu call another.

  -  Changes to how labels work ensures that the preprocessor in the assembler suite generates menu code that works.

  - These and other minor changes are described in the `help/Using.md` file in the user menu section.

- Bug found and fixed in assembler preprocessor – see `help/Using.md` for details.

- I've rewritten Franz' time value of money program so that it uses the user menu system to enter and to solve for variables. The source file is [./wp34s/wp34s_programs/time_value_money_umen.wp34s](./wp34s/wp34s_programs/time_value_money_umen.wp34s); the `.dat` file for loading into the calculator is [./wp34s/wp34s_programs/DAT/time_value_money_umen.dat](./wp34s/wp34s_programs/DAT/time_value_money_umen.dat). Documentation for this is in the source file.

  ---

  

- There are now `.pg5` files as well as `.pgm` files available in the `build/` folder. I do not have a DM42n so I cannot personally test that these work, but it seems that `wp34s_top.pg5` works, or at least boots correctly.

- Documentation simplified and updated - new file [Using.md](./help/Using.md) describes most features. The main changes are some bug fixes when working with files; file error messages simplified; some very minor changes in alpha keyboard layout.

- "Long" display variant restored, because why not?

- Bug fix - I noticed that an extra decimal point was displayed when
  (i) a number has a four-digit exponent; (ii) the exponent is negative; (iii)
  the mantissa only has one digit; (iv) there's enough room for the
  space between number and - sign (which there always will be given
  condition (iii); (v) the number is being displayed in the 7-segment
  display - no problem with Y-register display. This bug is present in the original calculator firmware but since that doesn't have large exponent entry and display compiled in by default it seems that no-one has noticed it!

- A collection of programs from the WP34S Sourceforge site has been added. Includes both source code and .dat files that can be loaded directly into the WP34S; also a .lib file containing some of the best programs. Read about it [here](./help/Using.md#wp34s-programs)!

- Bug in integration code fixed - program could crash when a split point was entered.

- Changed the behaviour of PROMPT in the "top" version of the build so that, if Y-register display is off, the contents of alpha are displayed in the long alpha row rather than in the short top row, allowing longer messages. There's no change to the "normal" version, for obvious reasons.

- Code added to check battery status before attempting to write files (thanks to dlachieze for suggesting this). However, if a file isn't written it may get out of sync with memory contents, causing problems. Code has also been added to avoid such problems. Finally, error messages associated with reading and writing files have been improved. See [this link](./help/Using.md#low-battery-handling) for further discussion of these changes - they should be invisible to a user unless something goes wrong!

- An empty directory, needed for building, had gone missing. This has been restored.

- Bugfix: problems with GTO. caused by an earlier regression when code tidying. Worth updating for this.

- Improved decimal-to-fraction conversion code gives better answers in some cases. For example, with DENMAX=100, pi is now 3 14/99 instead of 3 1/7.

- It is now possible to enter more than 12 digits in integer modes. The "Too Long" error is thrown only when the number requires more bits than the current word size for storage, up to a maximum word size of 64 bits. Note that this is a change from previous WP34S behaviour, which limited input to 12 digits whether or not this exceeded the word size.

- The BASE menu has been changed: rightarrow and CPX have been replaced with Show<- and Show->, so that integers with more than 12 digits can be viewed without leaving the BASE menu. (The QRG has been updated with this change.)

- References to the "long" display version (long alpha display like top, but no top row) have been removed. It's still there if you want it!

- Markdown and pdf versions of some documentation files added; the Markdown makes it easier to browse the documentation from the gitlab site.

- Bug fix: return stack wasn't cleared when deleting program steps, which could cause a crash.

- No more 34C version - for a long time now the 34S has included full complex lock code and I assume that if anyone is using the WP34S then they are using the 34S, not the 34C.

- Bug fix: pressing +/- twice when entering a number would leave the minus sign in the display.

- Bug fix: pressing SHIFT EXIT when in a catalogue now searches correctly for commands beginning with the printer symbol.

- Bug fix: pressing 1 or 2 when using the label browser (CAT) now jumps to LIB or BUP.

- New: pressing F5 (right arrow key) when in a catalogue now searches for commands beginning with a right arrow.

- New: pressing SHIFT F when in a catalogue now searches for commands beginning with F (F is on the XEQ key).

- New: pressing STO or RCL followed by the +/- key now gives the commands STOM and RCLM (store mode and recall mode); MODE is written above the +/- key.

- Code changes behind the scenes:

  * Lots of comments added to the source code, to help me and anyone else interested.

  * Many compile-time options removed -e.g., IR_PRINTING is always defined. This simplifies the source code a lot. For a list of the removed options, see the file `defile.txt/` in `src/`.

  * Code rewriting for clarity and efficiency. For example, the file save/load code no longer acts as though writing to real flash memory; display updating for the stopwatch is now faster; numerous changes made to make things clearer (for me).

- Help files updated. In particular, the new `help\wp34s_help.html` should be copied to the DM42 `HELP\' folder.

- There is a new file in `src/` called `code_description.txt`. It gives details about what much of the source code does.

- The &alpha;HELP command has been changed to reflect the new F3 file-choosing feature of the DM42 help browser.

- The "=" sign is now consistently displayed when the stopwatch is running behind the scenes. It does not flash (although code to make it flash is a compile-time option, disabled to save battery power).

#### Older updates:

- The integration routine &int;dx has been updated. The original function used the Double Exponential (Tanh-Sinh) integration technique. The new function implements the updated version of this technique described in the document `qthsh.pdf`, linked to at the start of [this thread](https://www.hpmuseum.org/forum/thread-16549.html) on the hpmuseum forum. Although this routine is written in C it uses the decNumber library.

- Also included is an integration routine based on the QAGS (adaptive integration with singularities) and QAGI (adaptive integration of infinite integrals) routines from the [Gnu Scientific Library (GSL)](https://www.gnu.org/software/gsl/doc/html/integration.html#qag-adaptive-integration). This code is written in C and does not use the decNumber library, limiting precision to a maximum of 16 digits.

- In addition, there is an implementation of the Romberg integration routine similar to the one used by Free42 (and, I believe, by HP calculators). 

- More details about these routines are given in the file [Integration_routines.docx](help/Integration_routines.docx) in the HELP folder.

- A previous update prevented the Stopwatch application from displaying correctly. This is now fixed.

- New feature added - the command αHELP takes a filename from the alpha register and opens a help file with this name. More details in the (usual) help file or in the file "User Help Files" in the help/ folder.

- New feature added - the keys RAM and LIB in the User Menu menu (shift shift 2, then F5 or F6) bring up menus of alpha labels in RAM and LIB respectively, allowing each to be executed by pressing the appropriate menu key. This is documented in the file RAM and LIB menu notes.docx in the help/ folder. The calculator help file has also been updated.

- Code changed so that the processor enters sleep state correctly after each keypress. This will reduce battery drain significantly. Existing users are recommomended to upgrade!

- Slight re-design of 7-segment characters to make them look a little nicer, particularly in the exponent. Here's a sample:

![20210718-11243279](help/screens/20210718-11243279.png)

- Shift characters now shown when entering alpha labels.

- New display code. Previously the entire display was redrawn every time a key was pressed or when any part of the display changed. The DM42 hardware coped magnificently, but this method must surely increase power consumption if nothing else. The new code only redraws what has changed. I believe that all bugs have been corrected...if you want to stay with the previous version, I quite understand!

- The wp34c and wp34s ("master" branch) code has been merged. You can now find both the wp34s and wp34c .pgm files in the the build directory of the master branch. The old wp34c branch has not been updated with the new display code, nor with the few changes detailed below. The wp34c files in the master branch are fully feature-equivalent with the wp34s files.

- The printer graphics commands are now included. They appear to work.

- The version of the program with a "top" row now displays (most) short messages there, rather than in the alpha row.

- The "LdPRG" (load program) function now recognises and loads program files produced by the assembler as individual .dat files rather than as .lib library files.

- There is also an experimental "4k" version - you'll find the 4k program files in the build/ directory, where the wp34s and wp34c files are. This expands the RAM from 2k to 4k, giving over 1500 program steps! The maximum number of registers doesn't increase, but you can (for example) have 100 double-precision registers along with about 1000 program steps. The disadvantage is that the wp34s_4k.dat file that contains the calculator state cannot be opened by other wp34s emulators, although the programs in it can be disassembled without difficulty. This screenshot shows a 283-step program with 1252 steps still remaining!

![4k_version](help/screens/4k_version.png)

## Licencing and thanks

Nearly all of the source code comes directly from the WP34S project; a vital contribution has come from the SDKdemo calculator source code and header files provided by SwissMicros. The SwissMicros code is covered by the BSD 3-clause licence, found in the file LICENSE; the WP34S code and my additions to it are covered by the GNU GPL3 licence, found in the file src/licence.h. The DMCP documentation [here](https://technical.swissmicros.com/dmcp/doc/DMCP-ifc-html/index.html) has been incredibly helpful and is much appreciated.

This software is not provided by SwissMicros, and they will not provide any support.

You will find the documentation for the original WP34S project, including the assembler suite, [here at Sourceforge.](https://sourceforge.net/projects/wp34s/files/doc/). You can also access the original source code.

Thanks once again to Walter, Paul, and Marcus, and to everyone else who contributed to the initial project. Thanks also to Jaco Mostert for keeping me interested in the WP34S and for showing, with the C43/47 project, how a calculator can be adapted to the DM42 keyboard.

Many thanks also to Didier on the DM42 developers' forum for suggestions, bug finding, and the excellent quick reference guide.

Nigel (UK)

[Back to top](#introduction)