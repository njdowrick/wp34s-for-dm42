## Program Documentation

This is a list of WP34S programs taken from the [WP34S
Sourceforge website](https://sourceforge.net/projects/wp34s/), with some basic information for each program
about purpose and use. The `.wp34s` files in this folder each contain the
source code for one program, along with (in most cases) some
documentation and comments; the `DAT` folder contains a `.dat` file for each
program that can be directly loaded into the DM42 WP34S.

The file `DAT/progs34s.lib` contains the programs
`digamma`, `invgamma`, `coordinates`, `modified-AGM`, `elliptic`, `time_value_money`, `complex_solver`, `triangle_solver`, and `primefactors`.

The `.dat` files are created from the `.wp34s` files using the
assembler that has been part of the WP34S project for a long time. You
can find a copy of the assembler in the src/tools folder and its
documentation in the help folder; of course you can also find this on the 
WP34S Sourceforge website.

Some of the files have been edited so that they work on V3 of the
WP34S.
___
### File name: 8queens.wp34s

**Author:** Unknown

**Global labels:** 8Qu

**Purpose:** "8 queens" (the task of placing 8 queens on a chessboard so
that none attacks any other) was often used as a speed test for
calculators and small computers. This is such a program for the
WP34S. More details in the source file.

**Use:** Execute 8Qu.

___
### File name: E24.wp34s

**Author:** Nobby K

**Global labels:** E24

**Purpose:** Round a number to its nearest equivalent in the E24 series
of allowed component values.

**Usage:**
```
/*
Input: X reg
Output: X reg <- E24(Input)
E24: 1.0 1.1 1.2 1.3 1.5 1.6 1.8 2.0 2.2 2.4 2.7 3.0
     3.3 3.6 3.9 4.3 4.7 5.1 5.6 6.2 6.8 7.5 8.2 9.1
 */
```
___
### File name: berger.wp34s

**Author:** Not known

**Global labels:** BER

**Purpose:**
```
/* A simple performance test.
 * Find the four digit number ABCD siuch that A^B * C^D equals ABCD.
 *
 * This program runs in signed integer mode (8.7 seconds) and in real
 * mode (451.9 seconds).
 *
 * The result number is in register 30.  The time in ticks is returned in X.
 */
```
**Usage:** Execute BER.

___
### File name: circle.wp34s

**Author:** Not known.

**Global labels:** CIR

**Purpose:** Draw a circle.

**Usage:** Execute CIR.

___
### File name: code_breaker.wp34s

**Author:** Jeff O.

**Global labels:** CBK

**Purpose and use:**
```
// This is the classic four digit number guessing game.
// After each guess you get feedback based on the number of numbers that are correct
// and the number that are correct but are in the wrong position.
//
// To play: XEQ 'CBK' to execute the. The calculator will generate
// a random four digit number with no repeated digits, zero not allowed,
// then stop and display 0.0. Key in a four digit guess, (e.g. 1234, 9753,
// etc.) and press R/S. The calculator will compare your guess to the secret
// number it generated and return a score in the following format:
//
// 	x.y
//
// where x is the number of digits in your guess that are both in the secret
// number and in the correct position, and y is the number of digits which
// are in the secret number but not in the correct position. So, for example,
// if the secret number is 3214 and you guess 1234, the score will be 2.2,
// as the 2 and 4 are in the correct positions and the 1 and 3 are in the
// number but in the incorrect position in your guess. After each guess,
// pressing x[<->]y will show the number of guesses so far. Keep entering
// guesses until 4.0 is displayed, indicating you have solved the puzzle.
//
// The guess will be in register Z in case you forget what you just guessed.
//
```
___
### File name: complex_solver.wp34s

**Author:** M. César Rodríguez

**Global labels:** CSV

**Purpose:** An Ostrowski-Traub based complex solver. See the source file for
details of the algorithm.

**Usage:**
```
*   Input:
*     XY: contains an initial root estimation. X is the real part, Y the
*         imaginary one
*     alpha: contains the name of the program to be solved. This program
*         is called with the stack (an SSIZE8 one) filled with a complex
*         estimation of the root. The program must return a complex
*         number in XY (X real, Y imaginary)
*
*   Output:
*     XY: the root found or the value of the last estimation for the
*         root. X real, Y imaginary
*     ZT: the value of the program to be solved evaluated at the
*         returned complex value in XY. Z real, T imaginary
*     LI: the input XY value
*
*     If the user interrupts the program ([<-] key), or the program to
*       be solved evaluates to infinity or NaN, or the solver reaches
*       its maximum iteration number (800 iterations), it ends with the
*       message "No root Found"
*
*   The user can interrupt the program pressing the [<-] key. If the
*     user interrupts the program with [EXIT] or [R/S], the values in
*     the stack, ABCD, L & I registers and flag D are not defined. Also,
*     the size of the stack will be 8
*
*   Register usage:
*     ABCD are cleared (no matter if the user works in SSIZE4 or SSIZE8)
*     Stack size is forced to 8 levels, but it is restored to the value
*       prior to calling the solver upon program end
*     Flag D is set (so function to be solved returns infinite or NaN
*       instead of reporting errors). This flag is restored to the value
*        prior to calling the solver upon program end
*
```
___
### File name: complexsolve.wp34s

**Author:** Unknown

**Global labels:** [cmplx]SV ([cmplx] displays as a superscript "C" on the WP34S)

**Purpose:** To "find a real or complex root of an arbitrary equation with
real and/or complex coefficients starting from just one real or
complex initial guess" (quote from Valentin Albillo article mentioned below).

**Usage:**
```
/* Solve code.
 * Based on code by Valentin Albillo for the HP 35s available from:
 *
 *	http://membres.multimania.fr/albillo/calc/pdf/DatafileVA031.pdf
 *
 * and also published in Datafile.
 *
 *
 * On entry the stack looks like:
 *
 *	Y	Guess imaginary component
 *	X	Guess real component
 *
 *
 * On return the stack looks like:
 *
 *	Y/X	Complex result
 *
 * The rest of the stack is zeroed.
 */
```
I'm guessing that the program to be solved goes in LBL 01 after the
tests for SPEC? inputs, but I haven't tried this. (ND)

___
### File name: coordinates.wp34s

**Author:** Unknown

**Global labels:** 'G[->]C' and 'C[->]G'

**Purpose:** To convert a  a latitude, longtitude, height triple into ECEF
coordinate triple (G->C); the reverse conversion is done by C->G.

**Usage:**
```
/* G->C
 * Convert a latitude, longtitude, height triple into ECEF coordinate triple
 * 
 * On entry:
 *	Z: Latitude
 *	Y: Longitude
 *	X: Height
 *
 * On exit:
 *	Z: z
 *	Y: y
 *	X: x
 *
 * The rest of the stack is preserved and last X gets the initial height value.
 *
 * C->G:
 * Convert an ECEF coordinate triple into a latitude, longtitude, height triple
 *
 * On entry:
 *	Z: z
 *	Y: y
 *	X: x
 *
 * On exit:
 *	Z: Latitude
 *	Y: Longitude
 *	X: Height
 *
 * The rest of the stack is preserved and last X gets the initial x value.
 */
```
___
### File name: derivative.wp34s

**Author:** Unknown

**Global labels:** DRV

**Purpose:** This code is the source for the internal differentiation
routine. More details of algorithm in source file.

**Usage:**

	// The only change of note is the call to the user's routine is done as XEQ 00
	// instead of XEQUSR.
	// On entry the point to evaluate the derivate at is in the X register.
	// On exit, the point is in L and the estimate is in X.  Y, Z and T are
	// cleared.

___
### File name: digamma.wp34s

**Author:** Unknown. 

**Global labels:** [PSI] (Greek letter) and [cmplx][PSI]

**Purpose:** To calculate the digamma function (derivative of log of gamma
function) for real and complex values.

**Usage:** Put value in stack and execute either [PSI] (real) or
[cmplx][PSI] (complex).

___
### File name: elliptic.wp34s

**Author:** Eduardo Duenez

**Global labels:** EI2; ELC.

**Purpose:** EI2 finds the elliptic integral of the second kind for given
argument x; ELC finds the circumference of an ellipse with semimajor
axes x and y.

**Usage:** Put argument(s) on stack and execute either EI2 or ELC. Note that
the modified AGM program is also required.

___
### File name: fibonacci.wp34s

**Author:** Unknown.

**Global labels:** FIB.

**Purpose:**
```
// This routine returns the generalised Fibonacci function.
//
// It returns the correct value for all integer arguments and is defined
// over the entire real domain.
//
// The internal Fibonacci function (FIB) uses the same algorithm.
// Both complex and real versions of this function are included in the
// firmware.
```
**Usage:** Put argument on stack; execute FIB.

___
### File name: invgamma.wp34s

**Author:**
```
/* This program was written by Gerson W. Barbosa based on an algorithm
 * by Angel Martin as part of a discussion thread on the Museum of HP
 * Calculators entitled "SandMath routine of the week: Inverse Gamma
 * Function" from March 2013.
 */
```
**Global labels:** [GAMMA][^-1]

**Purpose:** To find the inverse gamma function of a real argument.

**Usage:** Put argument on stack; execute [GAMMA][^-1].

___
### File name: mandelbrot.wp34s

**Author:** Unknown.

**Global labels:** MAN

**Purpose:** Draws (and prints) the Mandelbrot set. Note:
```
/* Generate a Mandelbrot set for: -1.5 < x < 0.5 and -1 < y < 1
 * Very very slow.
 */
```
**Usage:** Execute MAN.

___
### File name: matrix.wp34s

**Author:** Unknown.

**Global labels:** M-1; M-O; ATR; TR; M-; M+; M-y; FNM; RNM; CNM; MFD; M[^];
M[v]; MIO; MRC.

**Purpose:** Matrix operations! Lots of them. M-1 and M-0 create identity
and zero matrices; ATR and TR are trace operations; M-, M+, subtract
and add two matrices; M-y multiplies a matrix by a scalar; FNM, RNM,
CNM are matrix norms; MFD finds an element in a matrix; M[^] and M[v]
find the maximum or minimum element; MIO is a basic matrix editor; MRC
allows an individual element to be edited. 

**Usage:** These routines use WP34S matrix descriptors. View the source file
for detailed instructions.

___
### File name: matrixedit.wp34s

**Author:** Unknown,

**Global labels:** MED

**Purpose:** Full-featured matrix editor.

**Usage:**
```
/*
 *  Interactive editor for matrices.
 *  Start the editor with a matrix descriptor in X.
 *  Use the arrow keys to navigate:
 *    up/down: previous/next column
 *    f-up/f-down: previous/next row
 *  Type a digit to enter a number. This will suspend
 *  the progrom. R/S will enter X at the current row/column.
 *  XEQ RCL restores the present value to X if you decide
 *  not to change the cell. Press R/S to continue.
 *
 *  Some hot keys:
 *   A will start over at (1,1).
 *     When used outside the editor you will need to specifiy
 *     a new descriptor first.
 *   B (labeled 1/x) will call M[^-1] on the matrix.
 *   C continues at the last position.
 *   D computes and displays the determinant. R/S or C continue.
 *
 *  We need a few registers for internal use and therefore 
 *  set the stacksize to 4 to free registers A to D.
 *  Press <- to restore the mode and exit the editor.
 *  Double precision mode will always be reset!
 *
 *  Register usage:
 *   A - Matrix descriptor
 *   B - Current register
 *   D - Old setup mode
 *   I, J - Row and column
 *   K last key pressed
 *
 *  Flags:
 *   A - Controls big "=" sign
 *   00 - Shift state
 *
 *  Fixed labels:
 *   A, 11 - Start over with new matrix ('A'gain)
 *   B, 12 - Compute the inverse ('1/x')
 *   C     - Continue with current matrix ('C'ontinue)
 *   14, D - Compute the determinant ('D'eterminant)
 *   22    - [RCL] recalls current matrix element
 *   24    - [f] toggles navigation from horizontal to vertical
 *   31    - [ENTER^] (same as recall)
 *   35    - [<-] Backspace to exit the editor
 *   51    - [^] Navigation up/left
 *   61    - [v] Navigation down/right
 */
```
___
### File name: matrixtestAMxx.wp34s

**Author:** Unknown.

**Global labels:** Either 'AMx' or 'A1x'

**Purpose:** These files contain code to create "Albillo matrices",
matrices deliberately designed by Valentin Albillo to be difficult for calculators or computers to work with. More details can be found online with a
search for "Albillo matrices".

**Usage:** Execute the label; the code returns a matrix descriptor.

___
### File name: mod.wp34s

**Author:** Paul Dale

**Global labels:** MOD

**Purpose:** To implement a HP 41/42 MOD operation

**Usage:** a ENTER b XEQ'MOD' returns a MOD b HP41/42s-style.

___
### File name: modified-AGM.wp34s

**Author:** Eduardo Duenez

**Global labels:** MAG

**Purpose and use:**
```
/*
 * Given x,y positive,
 * XEQ'MAG' returns the Modified Arithmetic-Geometric mean
 * of x,y (with parameter z0 = 0)
 * according to Adlaj's recursive formulas:
 *	http://www.ams.org/notices/201208/rtx120801094p.pdf
 *
 * Program by Eduardo Duenez.
 */
```
Possibly more accurate than the built-in AGM function?

___
### File name: primefactors.wp34s

**Author:** Unknown.

**Global labels:** PF

**Purpose:** Finds the prime factors of an integer (quite quickly).

**Usage:**
```
/*
  Prime Factorization: list of 'true' prime factors (1<pf<n)

  Input:  X:n (positive integer)
  Output: X:pf1 (primefactor1) Alpha:^exp1 (exponent1)
   [R/S]  X:pf2 (primefactor2) Alpha:^exp2 (exponent2)
   [R/S]  .....
   [R/S]  n
  Used:   XYZT
*/
```
___
### File name: product.wp34s

**Author:** Unknown.

**Global labels:** PRD

**Purpose and use:**
```
// This code is the source for the internal product routine.
//
// The only change of note is the call to the user's routine is done as XEQ 00
// instead of XEQUSR.
// The code is documented.
```
___
### File name: quadratic-fit.wp34s

**Author:** Andrew Nikitin

**Global labels:** QF

**Purpose:** To fit a quadratic polynomial to empirical data.

**Usage:**
```
Note: Stack size four is needed.
// Initialize:
//   XEQ'QF'
// Clears sums, but not coefficients

// Enter point:
//   y ENTER x R/S (or [A])
// Result:
//   number of points entered so far

// Calculate coefficients (needs at least 3 points):
//   [B]
// Result:
//   a=regA=regZ
//   b=regB=regY
//   c=regC=regX
//   (order compatible with SLVQ)

// Evaluate Ax[^2]+Bx+C polynomial:
//   x [C]
// Result:
//   Ax[^2]+Bx+C
```
___
### File name: quadratic.wp34s

**Author:** Gerson W. Barbosa

**Global labels:** QDR

**Purpose:** Solves a quadratic equation with real coefficients.

**Usage:**
```
// To solve the quadratic 2*x^2-3*x+1:
//
//         keystrokes               display
//
//  2 ENTER 3 +/- ENTER 1 A           0.5
//                     x<>y             1
//
//
// Additional error checking could be added to verify that Z (a) is
// not zero and that the SQRT on line 011 doesn't go complex.
```
___
### File name: savage.wp34s

**Author:** Unknown

**Global labels:** SVG

**Purpose:** Implementation of savage benchmark.

**Usage:** Execute SVG.

___
### File name: solve.wp34s

**Author:** Unknown

**Global labels:** SV

**Purpose:** Appears to be a non-XROM version of the internal solver.

**Usage:**
```
The function to be solved is F.
/* Solve code.
 *
 * On entry the stack looks like:
 *	Y	Guess a
 *	X	Guess b
 *
 * On return the stack looks like:
 *	L	0
 *
 *	T	0
 *	Z	f(X)
 *	Y	previous root estimate
 *	X	root estimate
 */
```
___
### File name: sum.wp34s

**Author:** Unknown

**Global labels:** [sigma][SIGMA][DELTA]

**Purpose:** This is the source for the internal sigma/sum routine.

**Usage:** The routine to be summed is at LBL 00. Otherwise it works the
same as the internal function.

___
### File name: sunrise.wp34s

**Author:** Jeff O.

**Global labels:** RS

**Purpose:** Calculates the times of sunrise and sunset for a particular location.

**Usage:** Enter date in current datemode (see main WP34S documentation) and
execute RS. For this to work, you need to inform the program of your
own latitude and longitude.
It would be easy to change the program so that other
latitudes/longitudes could be used without editing the program, but
that's how it is as present.

The code is very well documented.

___
### File name: time_value_money

**Author:** Franz Huber

**Global labels:** TVM

**Purpose:** An excellent TVM solver as found on financial calculators.

**Usage:** Execute TVM. You can then enter a number and press A followed by
another key to store that number, or just press A followed by a key to
calculate a value.

For example:
```
XEQ'TVM';
5 A A (sets no. of periods = 5);
3 A B (sets interest rate = 3%);
1000 A C (sets PV = 1000);
0 A D (sets PMT = 0);
A LN (calculates FV; displays -1159.27).
```
When A is pressed, the alpha register displays a list of what the five
keys represent.

From the file:
```
=================================
* Extended TVM-Solver for WP34s *      Ver. 3.1      
**Author:** Franz Huber
=================================

Keyboard-Layout:
----------------
    N       I      PV      PMT      FV
[A][A]  [A][B]  [A][C]  [A][D]  [A][->]

    NP        NI
[A][STO]  [A][RCL]

Flags/Registers/
**Global labels:**
-----------------------
       00     01   02   03   04   05   06   07   08   09   10   11  12
Flag   Entry? --   2I?  --   --   --   <0?  =0?  --   --   --   --  --
Reg.   Key?   N    I    PV   PMT  FV   NP   NI   i    k    im   I1  I2
Lbl.   Start  N?   I?   PV?  PMT? FV?  Init I->i TVM TVM' i->I  --  --

Remarks:
========
- [XEQ]'TVM' to initialize registers and parameters!
- enter values for parameters NP,NI if desired (default=1)
- all financial keys for N,I,PV,PMT,FV and NP,NI work as follows:
  input+key stores the value, key alone calculates the value
  (or just recalls it for NP and NI)
- [R/S] resets the ENTRY? flag (e.g. after own calculations)
- alternative method to use the program:
  [STO/RCL/XEQ]01-05 to store/recall/calculate N,I,PV,PMT,FV
  (result after [XEQ] is not stored automatically!)
  [STO/RCL]06-07 to store/recall NP,NI
- iterative solving for I is limited to [-.999,max(2,I%->i)], i.e.
  from -99.9% to 200% (or I%->i) interest rate per payment period
  (the upper limit I% can be changed with I [STO]02 or I [A][B])
- if solving for I returns 2 possible solutions ("I1="), then just 
  press [R/S] to switch between I1 and I2 (stored in R11 and R12)
- error messages are displayed if invalid values have been entered:
  (don't use [R/S] after any error message!)
    NP=0 -> "Invalid parameter"
    both N=0 and I=0 -> "Invalid data"
    calculate FV if N=0 -> "Bad mode"
    no solution found for I -> "No root found"
    other invalid entries -> "Domain error" or "+/- Infinity"

Values:
-------
N = total number of periods (or payments)
    N=0 for perpetual payments (N=infinite)
        (not allowed if I=0! -> Error: "Invalid data")

I = annual interest rate (in % per year, -99.9%..999%)
    I=0 for no compounding
        (not allowed if N=0! -> Error: "Invalid data")

PV  = present value (or payment)
PMT = periodical payment
FV  = future value (or payment)
      (not defined for N=0! -> Error: "Bad mode")

Parameters:
-----------
NP = payment frequency PF (payment periods per year)
     NP>0 postnumerand payments (at END of periods)
     NP<0 prenumerand payments (at BEGin of periods)
     NP=0 not allowed! -> Error: "Invalid parameter"

NI = compounding frequency CF (interest periods per year)
     NI>0 decursive interest (compound at end of periods)
     NI<0 anticipative discount (compound at begin of periods)
     NI=0 continuous (theoretical) compounding (NI=infinite)
     |NI|=1 -> effective interest rate
     |NI|>1 -> nominal interest rate
```


___
### File name: triangle_solver.wp34s

**Author:** Franz Huber

**Global labels:** TRI

**Purpose:** Classic triangle solver.

**Usage:** Execute TRI. Press R/S. "a=" is displayed. If you know side a,
enter it and press R/S. If you don't know side a, just press R/S.
The calculator then asks for b, c, A, B, and C - enter or skip each in
the same way. (Lower-case letters are sides; upper-case letters are
the opposite angles.)

Having done this, press R/S again. The value of a is displayed, either
entered or calculated. Pressing R/S then displays b, c, A, B, C,
perimeter, and area!
From the file:
```
/*
=============================
* Triangle-Solver for WP34s *
=============================

Flags/Registers/
**Global labels:**
-----------------------
        00   01  02  03   04    05    06     07  08  09   Stack
Flag   I/O   sol --  --   --    --    --     --  --  --
Reg.   tmp    a   b   c  alpha beta  gamma   known-vars
Lbl.   I/O   SWW SWS SSS  SSW  [-------formulas-------]

Remarks:
========
- [R/S] continues program execution
- input: exactly 3 values for known sides/angles
- output: sequence of 1 or 2 solutions
- program works in any angle mode (360/RAD/G)
- no check for illegal values (side/angle<0 or angles>180°)
- possible error messages:
  "Invalid data" -> too few/many values (or no side) entered
  "Domain error" -> no possible triangle
*/
```
___
### File name: vectors.wp34s

**Author:** Unknown

**Global labels:** Vec; V.V; V+V; V-V; V[<->]V; Vun; V[times]S; VAB; V+-; VLx;
V[times]V.

**Purpose:** Lots of (3D) vector-related functions.

**Usage:** Execute Vec to initialise (or just set stacksize 8).
Then each of the labels above carries out a vector operation. Vectors
are generally stored in XYZ and TAB and results are returned to the
stack.
There are lots of shortcuts available - e.g., XEQ x<>y will execute
the V[<->]V routine. There's a short extract from the documentation
below; look in the file for full details.
```
/*
This is a basic suite of three dimensional vector operations for the WP 34S.

As a rule a vector occupies a triple of stack registers usually X, Y & Z or
T, A & B.  Scalars occupy one stack level.  Operations save their first
argument to last X (L for scalars and L, I & J for vectors).

Shortcuts available:
LBL A      // Absolute value - so XEQ A will execute GTO'VAB
GTO'VAB'

LBL B      // Unit vector (couldn't find a better key)
GTO'Vun'

LBL C      // Cross product
GTO'V[times]V'

LBL D      // Dot product
GTO'V.V'

LBL 32     // Exchange (on x<>y key) - so XEQ x<>y (key 32) will
    	   // execute GTO'V[<->]V'.
GTO'V[<->]V'

LBL 33     // Sign change (on +/- key)
GTO'V+-'

LBL 34     // L, Last X
GTO'VLx'

LBL 55     // Scalar multiply on multiplication key
GTO'V[times]S'

LBL 65     // Subtraction on -
GTO'V-V'

LBL 75     // Addition on +
GTO'V+V'

*/
```
