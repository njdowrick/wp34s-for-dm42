/* This file is part of 34S.
 * 
 * 34S is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * 34S is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with 34S.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __KEYS_H__
#define __KEYS_H__

/* These are the WP34S keycodes that the DM42 keypresses get translated into.
   This is not the same as the WP34S original keycode enum in two ways.
   First, there are extra keycodes at the end.
   Second, the "shift" keys (9, 10, 11) are part of the enumeration.
   At present, I can't remember why!
*/

typedef enum {
	K00 = 0,  K01 = 1,  K02 = 2,  K03 = 3,  K04 = 4,  K05 = 5,
	K10 = 6,  K11 = 7,  K12 = 8,
	K13 = 9,		
	K14 = 10,		
	K15 = 11,
	K20 = 12, K21 = 13, K22 = 14, K23 = 15, K24 = 16,
	K30 = 18, K31 = 19, K32 = 20, K33 = 21, K34 = 22,
	K40 = 24, K41 = 25, K42 = 26, K43 = 27, K44 = 28,
	K50 = 30, K51 = 31, K52 = 32, K53 = 33, K54 = 34,
	K60 = 36, K61 = 37, K62 = 38, K63 = 39, K64 = 40,
	K_F = 41, K_G = 42, K_H = 43,
	K_ARROW = 44,
	K_CMPLX = 45,
	K_NOP = 46, // mapped to the NOP entry at the end of tables
	K_SETMENU = 47,
	K_SYS = 48, // only used to trigger system menu
	K_MULTI = 49, // used to send back "multiple" keys from remap()
	K_EXIT = 50, // used to exit loop
	K_SSHOT = 51, //triggers screenshot
	K_OP = 52, // means that shift is an opcode; used in menus to execute commands rather than just keystrokes
} keycode;

// DM42 MENU names
typedef enum {
  M_Last = -1,
  M_Blank = 0,
  M_Clear,
  M_Disp,
  M_Base,
  M_Angles,
  M_Misc,
  M_Swap,
  M_Program, // 7
  M_Setup1,
  M_Arrow, // 9
  M_Flags,
  M_Prob, // 11
  M_Stats,
  M_Setup2,
  M_Solve,
  M_Blank2,
  M_Alpha, // 16
  M_C_Lock,
  M_Clear_C,
  M_Swap_C,
  M_Arrow_C,
  M_Prob_C,
  M_Blank2_C,
  M_Misc_C, // 23
  M_Usermenu,
  M_Print,
  M_MEM = 98,
  M_User = 99,
} menu_name;


#define K_UNKNOWN	-1

#define K_UP K30
#define K_DOWN K40

extern unsigned int keycode_to_digit_or_register(const keycode);

struct _ndmap {
  unsigned char key_34s;
  int shift;
};

#include "menu.h"

extern void process_keycode_with_shift ( struct _ndmap remapped );
extern unsigned int get_alpha_state ( void );
extern int KeyDown;

#endif
