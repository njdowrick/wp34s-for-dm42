#define NUMPROG_FLASH	8190
#define NUMPROG_FLASH_MAX NUMPROG_FLASH
#define MAGIC_MARKER 0xA53C
#define LIBRARY_SIZE 16384 // in bytes

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned short int s_opcode;

typedef struct _flash_region {
        unsigned short crc;
        unsigned short size;
        s_opcode prog[ NUMPROG_FLASH ];
} FLASH_REGION;

FLASH_REGION *library_ram;

void init_mem (void);
int checksum_region (FLASH_REGION *fr, FLASH_REGION *header);
int test_checksum (const void *data, unsigned int length, unsigned short oldcrc, unsigned short *pcrc);
int load_prog_file (char *fname);
void store_program_from_buffer (FLASH_REGION* fr);
int save_lib_file (char *fname);
int flash_append (int destination_step, const s_opcode *source, int count, int size);
void *xcopy (void *d, const void *s, int n);
unsigned short int crc16 (const void *base, unsigned int length);

#define UserFlash (*library_ram)

void init_mem () { // called at start of program_main in console.c
  char *v;

  v = (char *) calloc(LIBRARY_SIZE, 1);
  if (v == NULL) {
    perror("calloc failed");
    return;
  }
  library_ram = (FLASH_REGION *) v;
}

void init_library (void) {
  if ( checksum_region( &UserFlash, &UserFlash ) ) { 
    UserFlash.size = 0; // copy these into this struct
    UserFlash.crc = MAGIC_MARKER;
  }
}

int checksum_region (FLASH_REGION *fr, FLASH_REGION *header)
{
  unsigned int l = header->size * sizeof( s_opcode );
	
  return l > sizeof( fr->prog ) || test_checksum( fr->prog, l, fr->crc, &(header->crc ) );
}

int test_checksum (const void *data, unsigned int length, unsigned short oldcrc, unsigned short *pcrc)
{
  unsigned short crc;
  crc = crc16( data, length );
	
  if ( pcrc != NULL ) {
    *pcrc = crc;
  }
  return crc != oldcrc && oldcrc != MAGIC_MARKER;
}

int load_prog_file (char *fname) {
  FLASH_REGION* fr;
  unsigned char* buffer;
  int n, size;
  FILE *fp = NULL;
  
  fp = fopen (fname, "rb");
  if (fp == NULL) {
    printf ("Can't find or open this file...\n\n");
    return 1;
  }

  // File is now open with correct permissions

  buffer = (unsigned char*) calloc (LIBRARY_SIZE, 1);

  if (!buffer) {// calloc failure!
    fclose (fp);
    printf ("Calloc for buffer failed ...exiting\n");
    exit (1);
  }

  
  fseek (fp, 0, SEEK_END); 
  size = ftell(fp);         
  fseek (fp, 0, SEEK_SET); 

  fread (buffer, sizeof (buffer[0]), size, fp);

  fclose (fp);

  fr = (FLASH_REGION*) buffer;
  if (checksum_region(fr, fr)) {// crc failed - wrong filetype?
    unsigned short correct_crc = fr->crc; // get crc from after program end
    unsigned short end_of_prog = 4 + (fr->size)*2;
    unsigned short possible_crc = buffer[end_of_prog] + buffer[end_of_prog+1]*256;
    if (possible_crc != correct_crc) { // if still no good, bail out
      printf ("CRC check failure in load_prog_file-d\nbe sure the program file is\na compiled (dat) file, not a .wp34s file!\n");
      free (buffer);
      exit (1);
    }
  }

  store_program_from_buffer (fr);
  free (buffer);
  return 0;
}

int save_lib_file (char* file_name) {
  FILE *fp = NULL;
  int n;
  
  fp = fopen (file_name, "wb");
  if (fp == NULL) {
    printf ("Can't open %s to write; will exit.\n",file_name);
    exit (1);
  }
  
  // File is now open
  n = fwrite ((char *) &UserFlash, sizeof (UserFlash), 1, fp);
  fclose (fp);
  if ( n != 1 ) {
    printf ("Error writing %s; %i items written; exiting\n", file_name, n);
    exit (1);
  }
  return 0;
}

void store_program_from_buffer (FLASH_REGION* fr)
{ // buffer contains all of program file
  int needed, free;

  /*
   *  Compute space needed
   */
  needed = fr->size;
  free = NUMPROG_FLASH_MAX - UserFlash.size;
    
  if ( needed > free ) {
    printf ("Not enough room! Library file is full.\n\n");
    exit (1);
  }
  flash_append (UserFlash.size, fr->prog, needed, UserFlash.size + needed);
}

int flash_append (int destination_step, const s_opcode *source, int count, int size)
{ /*
   *  destination_step - address in flash memory to write to
   *  *source - pointer to source of data
   *  count - steps to be copied
   *  size - new size (in steps) of flash after the append (which can be a removal)
   */ 

  char *dest = (char *) ( UserFlash.prog + destination_step );
  char *src = (char *) source;
  int error_code;
  
  count <<= 1; // now count is bytes to be copied

  xcopy (dest, src, count);
  
  UserFlash.size = size; 
  checksum_region( &UserFlash, &UserFlash ); // compute the checksum
}

void *xcopy (void *d, const void *s, int n) {
        char *dp = d;
        const char *sp = s;

        if (sp > dp)
                while (n--)
                        *dp++ = *sp++;
        else if (sp < dp)
                while (n--)
                        dp[n] = sp[n];
        return d;
}

unsigned short int crc16 (const void *base, unsigned int length)
{
  unsigned short int crc = 0x5aa5;
  unsigned char *d = (unsigned char *) base;
  unsigned int i;

  for ( i = 0; i < length; ++i ) {
    crc  = ( (unsigned char)( crc >> 8 ) ) | ( crc << 8 );
    crc ^= *d++;
    crc ^= ( (unsigned char)( crc & 0xff ) ) >> 4;
    crc ^= crc << 12;
    crc ^= ( crc & 0xff ) << 5;
  }
  return crc;
}

int main (int argc, char *argv[]) {
  FILE *fp = NULL;
  int n, i = 2, files = 0;;

  init_mem();

  if ( argc < 3 ) {
    printf ("This program adds assembled wp34s program files (.dat files)\nto a library file.\n\n");
    printf ("The first argument must be the name of the library file, normally with a .lib extension..\n");
    printf ("This should be followed by a list of file names including the extension (normally .dat)\n");
    printf ("that are to be added to the library file.\n\n");
    printf ("So there must be a minimum of two arguments.\n\n");
    return 1;
  }
  
  fp = fopen (argv[1], "rb");
  if (fp == NULL) {
    printf ("%s not found; will create it.\n\n", argv[1]);
    init_library ();
  }
  else { //read in file
    n = fread ((char *) &UserFlash, sizeof (UserFlash), 1, fp);
    if (n != 1) {
      printf("Error reading in %s: n = %i\n", argv[1], n);
      return 1;
    }
    printf ("Initial size of %s: %i\n", argv[1], UserFlash.size);
  }
  // Userflash either contains a new file or the file so far.
  // Now I can append the program.
  // Can I just use load_prog_file?
  do {
    printf ("File to add: %s\n",argv[i]);
    if (load_prog_file (argv[i]) == 0) {
      printf ("Success! %s added.\nNew size of %s: %i\n", argv[i], argv[1], UserFlash.size);
      files++;
      printf ("Saving library file...\n\n");
      save_lib_file (argv[1]);
    }
    i++;
  }  while (i < argc);
  printf ("Done: %i files added.\n", files);
  return 0;
}
