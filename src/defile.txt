#define	DM42
#define INFRARED
#define INCLUDE_STOPWATCH
#define INCLUDE_YREG_CODE
#define INCLUDE_SIGFIG_MODE
#define INCLUDE_C_LOCK
#define RP_PREFIX
#define MODIFY_BEG_SSIZE8


#undef DEBUG 
#undef CONSOLE
#undef DUMP
#undef DUMP1
#undef IOS 
#undef QTGUI
#undef REALBUILD
#undef TINY_BUILD 
#undef UNIVERSAL_DISPATCH
#undef _DEBUG
#undef _DEBUG_ 
#undef _MSC_VER
#undef _WIN32
#undef WIN32
#undef WINGUI
#undef USECURSES
#undef WP34STEST

#undef EXTRA_FLAGS
#undef C_VERSION
