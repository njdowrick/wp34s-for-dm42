/* This file is part of 34S.
 * 
 * 34S is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * 34S is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with 34S.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __STORAGE_H__
#define __STORAGE_H__


// The actual size will be shorter on the device
#define NUMPROG_FLASH	8190
// chosen to get length of user flash area multiple of 2k
// - short ints at start of structure are 2 bytes each, each step is 2 bytes too

#ifdef FOUR_K
#define RAM_SIZE 4096 // in bytes
#else
#define RAM_SIZE 2048 // in bytes
#endif

#define LIBRARY_SIZE 16384 // in bytes

#define NUMPROG_FLASH_MAX NUMPROG_FLASH
typedef struct _flash_region {
        unsigned short crc;
        unsigned short size;
        s_opcode prog[ NUMPROG_FLASH ];
} FLASH_REGION;


extern TPersistentRam *main_ram, *backup_ram;
extern FLASH_REGION *library_ram;

#define PersistentRam (*main_ram)
#define BackupFlash (*backup_ram)
#define UserFlash (*library_ram)

#define FPT ppgm_fp //use this as the file pointer

extern unsigned short int crc16(const void *base, unsigned int length);
extern int  checksum_ram(void);
extern int  checksum_backup(void);

extern void init_library(void); // only called at start

extern int  append_program(const s_opcode *source, int length);
extern void stoprog(opcode);
extern void delprog(void);
extern int  init_34s(void);
extern void clrprog(void);
extern void clpall(void);
extern void clrall(void);
extern void reset(void);

extern void flash_backup(enum nilop op);
extern void flash_restore(enum nilop op);
extern int  flash_remove( int step_no, int count );
extern void save_program(enum nilop op);
extern void load_program(enum nilop op);
extern void load_registers(enum nilop op);
extern void load_sigma(enum nilop op);
extern void load_state(enum nilop op);
extern void store_program(enum nilop op);
extern void recall_program(enum nilop op);

extern void init_mem(void);
extern int  check_create_wp34sdir(void);
extern void store_program_from_buffer (FLASH_REGION* fr);
extern void load_prog_file (void);
extern void save_prog_file (void);

extern int  check_lib_file_crc (void);
extern int  check_backup_file_crc (void);

extern int  save_lib_file(int i);
extern int  load_lib_file(int i);
extern int  save_ram_file(int i);
extern int  load_ram_file(int i);
extern int  load_backup_file(int i);
extern int  open_selected_file (const char * fpath, const char * fname, void * data); // could be static

extern void import_program (void);
extern int  load_program_file (const char * fpath, const char * fname, void * data);

#endif
