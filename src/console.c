/* This file is part of 34S.
 * 
 * 34S is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * 34S is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with 34S.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *  This file was originally the console emulator. It has been modified
 *  to work with the DM42.
 *  It contains the main loop, which reads keys.
 */
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "dmcp.h"

#define DM42SAFE // Look in notes.txt, line 390

#include "keys.h"
#include "display.h"
#include "lcd.h"
#include "int.h"
#include "consts.h"
#include "storage.h"
#include "catalogues.h"
#include "stopwatch.h"
#undef DM42SAFE

static struct _ndmap remap (const int c);

struct _ndmap do_multi (struct _ndmap r);
static long int start_ticks;

void shutdown( void );
int is_key_pressed(void);
int get_key(void);
int put_key( int k );
void start_key_timer (void);
long int keyticks (void);
void moveto (int line, int x);
void do_now (int key, int shift);

// _ndmap carries a WP34S keycode and a shift state. Defined in keys.h
extern const struct _ndmap mapping_none[];
extern const struct _ndmap mapping_fshift[];
extern const struct _ndmap mapping_gshift[];
extern const struct _ndmap mapping_alpha[];
extern const struct _ndmap mapping_running[];

const struct _ndmap no_key = {.key_34s = K_NOP, .shift = 0};
const struct _ndmap heartbeat = {.key_34s = K_HEARTBEAT, .shift = -1};
const struct _ndmap f_shift = {.key_34s = K_F, .shift = -1};
const struct _ndmap g_shift = {.key_34s = K_G, .shift = -1};
const struct _ndmap h_shift = {.key_34s = K_H, .shift = -1};
const struct _ndmap release = {.key_34s = K_RELEASE, .shift = -1};
// shift = -1 doesn't change the existing shift state when a key is processed.

int KeyDown = 0;

// This takes a DM42 keypress and returns a WP34S keypress.
static struct _ndmap remap (const int c) {

  struct _ndmap no_change = {.key_34s = (char) c, .shift = 0};

  // A series of isolated cases
  if (c == K_HEARTBEAT) return heartbeat;
  if (c == 99) return release; // turn KEY_DOUBLE_RELEASE into ordinary release
  if (c == 44 ) { // Screenshot
    struct _ndmap sshot = {.key_34s = K_MULTI, .shift = SSHOT};
    return sshot;
  }
  if (Running | Pause) {
    return mapping_running[c];
  }

  // In alpha mode, make sure that f-shift F5, F6 can summon alpha menus
  if ( get_alpha_state() && cur_shift()==SHIFT_F) {
    if (c == KEY_F5) return (struct _ndmap) { K_ARROW, 1 };
    if (c == KEY_F6) return (struct _ndmap) { K_CMPLX, 1 };
  }

  // Handle menu keys:
  struct _menu Ref = get_current_menu_ref();
  switch (c) {
  case KEY_F1:
  case KEY_F2:
  case KEY_F3:
  case KEY_F4:
  case KEY_F5:
  case KEY_F6:
    if (cur_shift() == SHIFT_N) return Ref.keys[c - KEY_F1].unshifted;
    if (cur_shift() == SHIFT_F) return Ref.keys[c - KEY_F1].shifted;
  }

  // Alpha mode
  if ( get_alpha_state() ) { // If in alpha mode ...
    if (c == KEY_SHIFT) { // deal with shift keys
      switch (cur_shift()) { 
      case SHIFT_N:
	return mapping_none[c]; // N -> F
	break;
      case SHIFT_F:
	return mapping_fshift[c]; // F -> G
	break;
      case SHIFT_G:
	return h_shift; // G -> H
	break;
      case SHIFT_H:
	return h_shift; // H -> N
      default:
	return no_change;
      }
    }
    return mapping_alpha [c]; // return WP34S key
  }

  // No special cases - return key according to current shift state
  switch (cur_shift()) {
  case SHIFT_N:    
    return mapping_none[c];
    break;
  case SHIFT_F:
    return mapping_fshift[c];
    break;
  case SHIFT_G:
    return mapping_gshift[c];
    break;
  default:
    return no_change;
  }
}

// Little functions that use DMCP to do things

void shutdown( void )
{
  SET_ST(STAT_PGM_END);
}

int is_key_pressed(void) 
{
  return !key_empty();
}

int get_key(void)
{
  return key_pop();
}

/*
 * k is a WP34s key code. This translates it to a DM42 keycode, and pushes it.
 * 
 */
int put_key( int k )
{
  static const char wp34s_to_DM42[] = {
    KEY_SIGMA, KEY_INV, KEY_SQRT, KEY_LOG, KEY_F5, KEY_F6, // 0-5
    KEY_STO, KEY_RCL, KEY_RDN, KEY_SHIFT, KEY_COS, KEY_TAN, // 6-11
    KEY_ENTER, KEY_SWAP, KEY_CHS, KEY_E, KEY_BSP, 0, // 12-17
    KEY_XEQ, KEY_7, KEY_8, KEY_9, KEY_DIV, 0, // 18-23
    KEY_UP, KEY_4, KEY_5, KEY_6, KEY_MUL, 0, // 24-29
    KEY_DOWN, KEY_1, KEY_2, KEY_3, KEY_SUB, 0, // 30-35
    KEY_EXIT, KEY_0, KEY_DOT, KEY_RUN, KEY_ADD,}; // 36-40
  
  if ( ( k >= K00 ) && ( k <= K64 ) ) {
    return key_push( wp34s_to_DM42 [k] );
  }
  else {
    return -1;
  }
}

void start_key_timer() {
  start_ticks = get_rtc_ticks();
  KeyDown = 1;
}

long int keyticks () {
  long int i;
  i = (get_rtc_ticks() - start_ticks)*10;
  return i >> 8;
}

/*
 * Debug function - print_debug displays two integers
 */
#if 0
#include "debug.c"
#endif

void do_now (int key, int shift) { // key and shift are like the output of remapped
  struct _ndmap temp;
  temp.key_34s = key;
  temp.shift = shift;
  process_keycode_with_shift (temp);
}

// This handles WP34S key presses that don't have a simple DM42 equivaltent.

struct _ndmap do_multi (struct _ndmap r) {
  switch(r.shift) {
  case ONSTO: // flash_backup()
    flash_backup(OP_SAVE);
    break;
  case ONRCL: // flash_restore()
    flash_restore(OP_LOAD);
    break;
  case WRLIB: // save library file
    save_lib_file(1);
    break;
  case LLIB: // load library file
    load_lib_file(1);
    break;
  case LLIBBAK: // load library file backup
    load_lib_file(2);
    break;
  case LRAMBAK: // load state file backup
    load_ram_file(2);
    break;
  case WRTST: // save state file
    save_ram_file (1);
    break;
  case LDST:
    load_ram_file(1);
    break;
  case LDPRG:
    load_prog_file();
    break;
  case SVPRG:
    save_prog_file();
    break;
  case HELP:
    run_help_file("/HELP/wp34s_help.html");
    break;
  case DOTS:
    do_all_dots();
    break;
  case SSHOT: // comes after f-key already pressed, but not released
    do_now(K_RELEASE,0); //release f-key; restore display
    do_now(K_SSHOT,0); //activates screenshot routine in keys
    break;
  case DEFMEN:
    toggle_default_menu();
    break;
  case KCPX:
    if (C_LOCKED) {
      set_menu(M_C_Lock);
    }
    else {
      do_now (K_CMPLX, 0);
      do_now (K_RELEASE, 0);
    }
    break;
  case TOGA:
    put_user_flag (regA_idx, !(get_user_flag (regA_idx)));
    do_now (K_RELEASE, 0);
    break;
  default:;
  }
  reset_shift();
  return no_key;
}
// Have changed this so that no_key is always returned
// and shift is always reset.
// Need to check COMPLEX and SSHOT to see if this has upset them.


/*
 *  Main loop
 */
void program_main(){
  int c = 0;
  struct _ndmap remapped;

  init_mem();
  init_RegionTab();
  init_34s();
  State2.flags = 1;  

  lcd_writeClr (t24);
  lcd_clear_buf ();
  
  if (check_create_wp34sdir() != 0) { // has to succeed!
    lcd_puts (t24, "Can't create wp34s dir");
    lcd_puts (t24, "Stopping program");
    lcd_refresh();
    return;
  }

  if (load_ram_file(0)) {
    c = 1; // extra use for c!
    lcd_puts (t24, "RAM file not found");
    lcd_refresh();
  }
  else {
    DispMsg = NULL; // get rid of "Erased" message from reset() in init_34s()
  }
  if (load_backup_file(0)) {
    c = 1;
    lcd_puts (t24, "Backup file not found");
    lcd_refresh();
  }
  if (load_lib_file(0)) {
    c = 1;
    lcd_puts (t24, "Library file not found");
    lcd_refresh();
  }
  init_library (); // checks library memory and initialises it if crc fails
  
  if (c) { // wait if there's been an error
    sys_timer_start(0, 1500);  // strangely, sys_delay() doesn't work here
    sys_sleep();
    sys_timer_disable(0); // stop timer if needed
  }
  
  clear_disp();

  t20->newln = 0;
  t20->lnfill = 0;
  t20->fixed = 1;
  t20->xspc = -2;
  t20->bgfill = 0;

  run_menu_item_app = run_menu_item;
  menu_line_str_app = menu_line_str;

  display();
  display_current_menu();
  lcd_refresh();
  JustDisplayed = 0;
  /*
    Returns a positive integer when a key is pressed. Doesn't return until a key is pressed.
    Sleeps peacefully while waiting for a key.
    Handles (in principle!) turning off and on.
    =================
    Main event loop
    =================

    Status flags:
    ST(STAT_PGM_END)   - program should turn off state (set by auto off timer)
    ST(STAT_SUSPENDED) - Program ready for off and doesn't need to be woken-up again
    ST(STAT_OFF)       - Program in off state (only [EXIT] key can wake)
    ST(STAT_RUNNING)   - OS doesn't sleep in this mode
  */
  for(;;) {

    if ( ST(STAT_PGM_END) && ST(STAT_SUSPENDED) ) // In off mode and suspended
      {
	CLR_ST(STAT_RUNNING);
	sys_sleep();
      }
    else if (!ST(STAT_PGM_END) && key_empty()) // Go to sleep if no keys available
    {
      CLR_ST(STAT_RUNNING);
#ifdef STOPWATCH_FLASH_LEQ
      // Only start timer if needed for pause or keypress or stopwatch
      if ( (Pause > 0) || KeyDown == 1 || StopWatchRunning ) {
#else
      // Only start timer if needed for pause or keypress
      if ( (Pause > 0) || KeyDown == 1 ) { 
#endif
	sys_timer_start(0, 100);  // Timer 0: wake up for heartbeat 
	sys_sleep();
	if (sys_timer_timeout(0)) { // If timer has completed...
	  key_push (K_HEARTBEAT);
	  if (Pause > 0) Pause--;
	}
	sys_timer_disable(0); // stop timer
      }
      else {
	sys_sleep();
      }	
    }
    
    // Wakeup in off state or going to sleep
    if (ST(STAT_PGM_END) || ST(STAT_SUSPENDED) ) {
      if (!ST(STAT_SUSPENDED)) {
        // Going to off mode
        lcd_set_buf_cleared(0); // Mark no buffer change region
        draw_power_off_image(1);

        LCD_power_off(0);
        SET_ST(STAT_SUSPENDED);
        SET_ST(STAT_OFF);
      }
      // Already in OFF -> just continue to sleep above
      continue; // note - this skips the rest of the loop and goes back to the beginning,
      // thus reaching the sleep()
    }

    // Well, we are woken-up
    SET_ST(STAT_RUNNING);

    // Clear suspended state, because now we are definitely reached the active state
    CLR_ST(STAT_SUSPENDED);

    // Get up from OFF state
    if ( ST(STAT_OFF) ) {
      LCD_power_on();
      key_pop_all(); //try clearing the ON key to stop double flashing of RPN
      rtc_wakeup_delay(); // Ensure that RTC readings after power off will be OK

      CLR_ST(STAT_OFF);

      if ( !lcd_get_buf_cleared() )
        lcd_forced_refresh(); // Just redraw from LCD buffer
    }
#ifdef STOPWATCH_FLASH_LEQ
    if ( StopWatchRunning ) { 
      dot( LIT_EQ, (Ticker & 8) );
      lcd_refresh();
    }
#endif
    // Key is ready -> clear auto off timer
    //    if ( !key_empty() )
    //  reset_auto_off(); - moved as key here might be heartbeat
    // Fetch the key
    //  < 0 -> No key event
    //  > 0 -> Key pressed
    // == 0 -> Key released
    c = key_pop();
    if ( KeyCallback != NULL ) {
      c = (*KeyCallback)( c );
    }
    if ( (c != K_HEARTBEAT) && (c >= 0) ) {
      reset_auto_off();
    }
    if (c >= 0) {
      remapped = remap(c);
      if (remapped.key_34s == K_SETMENU) {
	set_menu ( remapped.shift );
	display_current_menu ();
	reset_shift();
	remapped = no_key;
      }
      else if (remapped.key_34s == K_SYS) {
	SET_ST(STAT_MENU);
	handle_menu(&MID_MENU, MENU_RESET, 0); // App menu
	CLR_ST(STAT_MENU);
	reset_shift();
	remapped =  no_key;
	clear_disp();
	display_current_menu();
      }
      else if (remapped.key_34s == K_MULTI) {
	remapped = do_multi (remapped); // do_multi now always returns no_key;
	// if you don't want this, you might have to change "else if" back to "if"
	clear_disp();
	display_current_menu();
      }
      else if (remapped.key_34s == K_EXIT) {

	// check backup file crc - if this is not valid, try writing;
	int backup_ok = check_backup_file_crc ();
	int errcode = 0;
	if (backup_ok == 0) {
	  // try to write backup file from Backup - if successful, change backup_ok to 1
	  if ((errcode = save_ram_file (2)) == 0) {
	    backup_ok = 1;
	  }
	  else {
	    g_message ("Backup", "WwriTE Error", errcode);
	  }
	}

	// check lib file crc - if this is not valid, try writing;
	int lib_ok = check_lib_file_crc ();
	if (lib_ok == 0) {
	  // try to write lib file - if successful, change lib_ok to 1
	  if ((errcode = save_lib_file (0)) == 0) {
	    lib_ok = 1;
	  }
	  else {
	    g_message ("Library", "WwriTE Error", errcode);
	  }
	}

	// try writing RAM file
	int ram_ok = 0;

	if ((errcode = save_ram_file (0)) == 0) {
	  ram_ok = 1;
	}
	else {
	  g_message ("RAM file", "WwriTE Error", errcode);
	}
	
	// If any of these fail, offer to abort EXIT.
	if (!ram_ok || !lib_ok || !backup_ok) { // force failure to test next bit
	  clear_disp(); // clears dots array and buffer
	  lcd_refresh();
	  lcd_switchFont(fReg, 3);
	  fReg->inv = 0;
	  lcd_printAt(fReg,1, "Problem saving state");
	  lcd_printAt(fReg,2, "to file.");
	  lcd_printAt(fReg,4, "Press ENTER or R/S");
	  lcd_printAt(fReg,5, "to exit anyway,");
	  lcd_printAt(fReg,6, "or any other key to stay.");
	  lcd_refresh();

	  do {
	    sys_sleep(); // wait for key press
	    c = key_pop();
	  }
	  while (c == 0); // go back to sleep if it's a key release

	  if ( (c == KEY_ENTER) || (c == KEY_RUN) ) break;
	  clear_disp();
	  display_current_menu();
	  remapped = no_key; // new - no point in trying to process K_EXIT?
	}
	else {
	  break;
	}
      }
      process_keycode_with_shift(remapped);
      if ( ((remapped.key_34s != K_HEARTBEAT) && (remapped.key_34s != K_RELEASE)) || JustStopped || (KeyCallback != NULL) ) start_key_timer();
      // the KeyCallback check means that the key timer is started if a key is pressed when the stopwatch is being displayed.
    }
  }
  return;
}
