/* This file is part of 34S.
 * 
 * 34S is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * 34S is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with 34S.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This module handles all load/save operations in the real build or emulator
 * Original module written by MvC; DM42 additions / deletions by ND
 */

#include <stdio.h>
#include <stdlib.h>

#include "features.h"
#include "dmcp.h"

#define DM42SAFE
#include "lcd.h"
#include "xeq.h"
#include "storage.h"
#include "display.h"
#include "stats.h"
#include "alpha.h"
#undef DM42SAFE

#define SLCDCMEM
#define VOLATILE_RAM

#ifdef FOUR_K

#define STATE_FILE "wp34s/wp34s_4k.dat"
#define STATE_FILE_BACKUP "wp34s/wp34s_4k.bak"
#define BACKUP_FILE "wp34s/wp34s-backup_4k.dat"

#else

#define STATE_FILE "wp34s/wp34s.dat"
#define STATE_FILE_BACKUP "wp34s/wp34s.bak"
#define BACKUP_FILE "wp34s/wp34s-backup.dat"

#endif

#define LIBRARY_FILE "wp34s/wp34s-lib.dat"
#define LIBRARY_FILE_BACKUP "wp34s/wp34s-lib.bak"

// Text strings for error messages

//static const char Power_err[] = "PoWwEr Error";
static const char CRC_err[] =   "CrC Error";
static const char Open_err[] =  "OPEn Error  ";
static const char Write_err[] = "WwritE Error";
//static const char Close_err[] = "CLoSE Error ";
//static const char Backup_err[]= "bAckuP Error";
static const char Read_err[] =  "rEAd Error  ";
static const char Eof_err[] =   "End oF FiLE ";

#ifdef BIGGER_DISPLAY
static const char Lib_file[] =  "Library File";
static const char Ram_file[] =  "RAM File";
static const char Prog_file[] = "Program File";
static const char Backup_file[]="Backup File";
static const char Error_err[] = "File too long";
#else
static const char Lib_file[] =  "Lib File";
static const char Ram_file[] =  "Ram File";
static const char Prog_file[] = "Prg File";
static const char Backup_file[]="Bak File";
static const char Error_err[] = "Too long";
#endif

static int update_lib_file (char *dest, int bytes);
static int program_flash_new (void *destination, void *source, int bytes, int update);
/*
 *  Set up the persistent RAM
 */

TPersistentRam *main_ram, *backup_ram;
FLASH_REGION *library_ram;

void init_mem () { // called at start of program_main in console.c
  char *v;

  v = (char *) calloc(RAM_SIZE + RAM_SIZE + LIBRARY_SIZE, 1);
  if (v == NULL) {
    perror("calloc failed");
    return;
  }
  main_ram = (TPersistentRam *) v;
  v += RAM_SIZE;
  backup_ram = (TPersistentRam *) v;
  v += RAM_SIZE;
  library_ram = (FLASH_REGION *) v;
}

/*
 *  Data that is saved in the SLCD controller during deep sleep
 *  Both SLCDCMEM and VOLATILE_RAM are defined as blank.
 *  They are kept for sentimental reasons.
 */
SLCDCMEM TStateWhileOn StateWhileOn;

/*
 *  A private register area for XROM code in volatile RAM
 *  It replaces the local registers and flags if active.
 */
TXromParams XromParams;
VOLATILE_RAM TXromLocal XromLocal;

/* Private space for four registers temporarily
 */
VOLATILE_RAM REGISTER XromA2D[4];

//*************************************************************
//****                                                    *****
//*********** CRC functions **********************
//**** crc16, test_checksum, checksum_ram, checksum_backup, ***
//**** checksum_region, checksum_general_region. **************
//****                                                *********
//*************************************************************
/*
 *  The CCITT 16 bit CRC algorithm (X^16 + X^12 + X^5 + 1)
 */
unsigned short int crc16( const void *base, unsigned int length )
{
  unsigned short int crc = 0x5aa5;
  unsigned char *d = (unsigned char *) base;
  unsigned int i;

  for ( i = 0; i < length; ++i ) {
    crc  = ( (unsigned char)( crc >> 8 ) ) | ( crc << 8 );
    crc ^= *d++;
    crc ^= ( (unsigned char)( crc & 0xff ) ) >> 4;
    crc ^= crc << 12;
    crc ^= ( crc & 0xff ) << 5;
  }
  return crc;
}

/*
 *  Compute a checksum and compare it against the stored sum
 *  Stores computed value if fourth argment isn't NULL
 *  Returns non zero value if failure
 */
static int test_checksum( const void *data, unsigned int length, unsigned short oldcrc, unsigned short *pcrc )
{
  unsigned short crc;
  crc = crc16( data, length );
	
  if ( pcrc != NULL ) {
    *pcrc = crc;
  }
  return crc != oldcrc && oldcrc != MAGIC_MARKER; // so no crc error if oldcrc == MAGIC_MARKER
}

/*
 *  Checksum the persistent RAM area
 *  Returns non zero value if failure
 *  Stores computed checksum in Crc
 */
int checksum_ram( void )
{
  return test_checksum( &PersistentRam, sizeof( PersistentRam ) - sizeof( short ),
			Crc, &Crc );
}

/*
 *  Checksum the backup flash region
 *  Returns non zero value if failure
 *  Doesn't update BackupFlash with correct crc - should already have it.
 */
int checksum_backup( void )
{
  return test_checksum( &BackupFlash, sizeof( BackupFlash ) - sizeof( short ),
			BackupFlash._crc, NULL );
}

/*
 *  Checksum a flash region
 *  Returns non zero value if failure
 */
static int checksum_region( FLASH_REGION *fr, FLASH_REGION *header )
{
  unsigned int l = header->size * sizeof( s_opcode );
  // Fails if length > max. program size or checksum is wrong	
  return l > sizeof( fr->prog ) || test_checksum( fr->prog, l, fr->crc, &(header->crc ) );
}

// Two sorts of "regions" - library has crc in bytes 1+2, state file has crc in final two bytes.
// This checks to see if the region pointed to by fr is valid -
// either a library file, or a state file consisting only of programs.

static int checksum_general_region (FLASH_REGION *fr) {
  if ( checksum_region (fr, fr) ) { // crc of fr not valid
    // check to see if it's a statefile with crc at end
    unsigned short correct_crc = fr->crc; // get correct crc that checksum has just calculated
    unsigned short end_of_prog = 4 + (fr->size)*2; // first byte after end of programs
    unsigned char* buffer = (unsigned char*) fr;
    unsigned short possible_crc = buffer[end_of_prog] + buffer[end_of_prog+1]*256;
    if (possible_crc != correct_crc) { // if still no good, bail out
      return 1;
    }
  }
  return 0; // Success, one way or the other!
}


//*************************************************************
//********                                                   **
//********    Program editing, deleting, clearing, and reset **
//********                                                   **
//*************************************************************

/*
 *  Helper to store final END in empty program space
 */
static void stoend( void )
{
  ProgSize = 1;
  Prog[ 0 ] = ( OP_NIL | OP_END );
}

/*
 *  Clear the program space
 */
void clpall( void )
{
  clrretstk_pc();
  stoend();
}

/*
 *  Sanity checks for program (step) deletion
 */
static int check_delete_prog( unsigned int pc ) 
{
  if ( !isRAM( pc ) || ( pc == ProgSize && getprog( pc ) == ( OP_NIL | OP_END ) ) ) {
    report_warn(ERR_READ_ONLY);
  }
  else {
    return 0;
  }
  return 1;
}

/*
 *  Clear just the current program
 */
void clrprog( void )
{
  update_program_bounds( 1 );
  if ( nLIB( ProgBegin ) == REGION_LIBRARY ) {
    /*
     *  Program is in flash
     */
    flash_remove( ProgBegin, ProgEnd + 1 - ProgBegin );
  }
  else {
    if ( check_delete_prog( ProgBegin ) ) {
      return;
    }
    clrretstk();
    xcopy( Prog_1 + ProgBegin, Prog + ProgEnd, ( ProgSize - ProgEnd ) << 1 );
    ProgSize -= ( ProgEnd + 1 - ProgBegin );
    if ( ProgSize == 0 ) {
      stoend();
    }
  }
  set_pc( ProgBegin - 1 );
  update_program_bounds( 1 );
}
 
/*
 *  Clear all - programs and registers
 */
void clrall(void) 
{
  NumRegs = TOPREALREG;
  xeq_init_contexts();
  clrreg( OP_CLREG );
  clrstk( OP_CLSTK );
  clralpha( OP_CLRALPHA );
  clrflags( OP_CLFLAGS );
  clpall();

  reset_shift();
  State2.test = TST_NONE;

  DispMsg = NULL;
}


/*
 *  Clear everything
 */
void reset( void ) 
{
  xset( &PersistentRam, 0, sizeof( PersistentRam ) );
  clrall();
  init_state();
  printer_set_delay(1800);
  DispMsg = "Erased";
}

/*
 *  Store into program space.
 */
void stoprog( opcode c ) {
  const int off = isDBL( c ) ? 2 : 1;
  int i;
  unsigned int pc = state_pc();

  if ( pc == ProgSize && c != ( OP_NIL | OP_END ) )
    stoprog( OP_NIL | OP_END );

  if ( !isRAM( pc ) ) {
    report_warn( ERR_READ_ONLY );
    return;
  }
  clrretstk();
  xeq_init_contexts();
  if ( ProgFree < off ) {
    return;
  }
  ProgSize += off;
  ProgEnd += off;
  pc = do_inc( pc, 0 );	// Don't wrap on END
  for ( i = ProgSize + 1; i > (int) pc; --i ) {
    Prog_1[ i ] = Prog_1[ i - off ];
  }
  if (isDBL(c))
    Prog_1[pc + 1] = c >> 16;
  Prog_1[pc] = c;
  State.pc = pc;
}


/*
 *  Delete the current step in the program
 */
void delprog( void )
{
  int i;
  const unsigned int pc = state_pc();
  int off;

  if ( check_delete_prog( pc ) )
    return;
  if ( pc == 0 )
    return;

  clrretstk(); // ND change

  off = isDBL( Prog_1[ pc ]) ? 2 : 1;
  ProgSize -= off;
  ProgEnd -= off;
  for ( i = pc; i <= (int) ProgSize; ++i )
    Prog_1[ i ] = Prog_1[ i + off ];
  decpc();
}


/*
 *  Helper to append a program in RAM.
 *  Returns non zero in case of an error.
 */
int append_program( const s_opcode *source, int length )
{
  unsigned short pc;
  int space_needed = length - ProgFree;

  if ( ProgSize == 1 ) {
    /*
     *  Only the default END statement is present
     */
    --space_needed;
    --ProgSize;
  }
  if ( length > NUMPROG_LIMIT ) {
    return report_err( ERR_INVALID );
  }
  if ( length > NUMPROG_LIMIT - ProgSize ) {
    return report_err( ERR_RAM_FULL );
  }

  /*
   *  Make room if needed
   */
  clrretstk();
  if ( space_needed > 0 && SizeStatRegs != 0 ) {
    space_needed -= SizeStatRegs;
    sigmaDeallocate();
  }
  if ( space_needed > 0 ) {
    int regs;
    if (is_dblmode())
      regs = global_regs() - ( ( space_needed + 7 ) >> 3 );
    else
      regs = NumRegs - ( ( space_needed + 3 ) >> 2 );

    if ( regs < 0 ) {
      return report_err( ERR_RAM_FULL );
    }
    cmdregs( regs, RARG_REGS );
  }
  /*
   *  Append data
   */
  pc = ProgSize + 1;
  ProgSize += length;
  xcopy( Prog_1 + pc, source, length << 1 );
  set_pc( pc );
  return 0;
}

//*************************************************************
//*********                                         ***********
//********* Altering library memory and library file **********
//********* Writing to and reading from backup memory/file ****
//********* init_library, flash_append, flash_remove, *********
//********* program_flash_new, update_lib_file, ************
//********* flash_backup, flash_restore.             **********
//*********                                          **********
//*************************************************************

/*
 *  Library memory is a region of RAM. Any changes to it
 *  are immediately copied to wp34s-lib.dat
 *  if power allows. If it doesn't crc consistency checks allow correction
 *  of any discrepancy when powerm returns.
 */

/*
 *  Initialize the library to an empty state if it's not valid
 */
void init_library( void ) {
  if ( checksum_region( &UserFlash, &UserFlash ) ) { 
    UserFlash.size = 0; // copy these into this struct
    UserFlash.crc = MAGIC_MARKER; // test_checksum accepts this value as correct
  }
}
 

/*
 *  Add data at the end of user flash memory.
 *  Update crc and counter when done.
 *  All sizes are given in steps.
 */
static int flash_append( int destination_step, const s_opcode *source, int count, int size )
{ /*
   *  destination_step - address in flash memory to write to
   *  *source - pointer to source of data
   *  count - steps to be copied
   *  size - new size (in steps) of flash after the append (which can be a removal)
   */ 

  char *dest = (char *) ( UserFlash.prog + destination_step );
  char *src = (char *) source;
  int error_code;
  
  count <<= 1; // now count is bytes to be copied

  error_code = program_flash_new (dest, src, count, 1); // do the copy and write it to file
  // If there's a problem writing the data, error_code will be non-zero.
  // If it's a file error, then an error message has already been displayed.
  // If it's a power error, then a warning screen has been displayed.
  
  /*
   *  Update the library header to fix the crc and size fields.
   */
  UserFlash.size = size; // this is the size argument - it isn't computed, just used
  checksum_region( &UserFlash, &UserFlash ); // compute the checksum and update memory
  if (error_code) goto error;

  error_code = update_lib_file ((char *) &UserFlash, 4); // write first 4 bytes to file
  if (error_code) goto error;
  return 0;
 error:
  g_message (Lib_file, "nO UPdAtE", error_code);
  return error_code;
}


/*
 *  Remove steps from user flash memory.
 */
int flash_remove( int step_no, int count )
{
  const int size = UserFlash.size - count;
  step_no = offsetLIB( step_no );
  return flash_append( step_no, UserFlash.prog + step_no + count,
		       size - step_no, size );
}

/*
 * Copy bytes from source to destination;
 * if update is true, update the library file.
 * Compares library memory crc with library file crc before writing to file.
 * AFAICS never called with update = 0.
 */
static int program_flash_new( void *destination, void *source, int bytes, int update )
{
  char *dest = (char *) destination;

  // Copy the source to the destination memory
  xcopy( dest, source, bytes);

  // Update the library file
  if (update) {
    if ( check_lib_file_crc() ) {
      return update_lib_file (dest, bytes); // should be ok unless the power suddenly decides to fail
    }
    else {
      return save_lib_file (0); // try to write a complete new lib file
      // there's been a problem, but it should now be fixed
    }
  }
  return 0; // no file-writing attempted so no problem
}

/*
 * Used to update library file if power allows.
 */
static int update_lib_file (char *dest, int bytes) {
  int offset;
  uint x=0;
  FRESULT f;

  // power_check_screen: checks power, if low asks for USB connection;
  // returns 0 if ok, returns 1 if dialog cancelled or timed out
  if ( power_check_screen() ) { // 1, so there's a problem
    return 99; // 99 is the power problem code (greater than any file error code)
  }
  
  // Power is ok, or at least fixed.
  offset = dest - (char *) &UserFlash;
  sys_disk_write_enable(1);
  f = f_open( FPT, LIBRARY_FILE, FA_READ | FA_WRITE | FA_OPEN_ALWAYS ); // open region file for reading / writing; create if not there
  if ( f != FR_OK ) goto error;

  f_lseek( FPT, offset ); // go to correct place in file

  f = f_write( FPT, dest, bytes, &x); // write requested stuff to file
  if (f != FR_OK) goto error;

  f = f_close( FPT );
  if ( f != FR_OK ) goto error;

  sys_disk_write_enable(0);
  return 0;
 error:
  if (FPT) f_close (FPT);
  sys_disk_write_enable(0);
  return (int) f;
}

/*
 *  Simple backup / restore of user memory to / from backup memory
 *  Started with ON+STO or ON+RCL or the SAVE/LOAD commands
 */
void flash_backup( enum nilop op ) { 
  int error_code;
  
  if ( not_running() ) {
    process_cmdline_set_lift();
    init_state();
    checksum_ram();
    xcopy( &BackupFlash, &PersistentRam, sizeof( PersistentRam ) );
    error_code = save_ram_file (2); // writes Backup to backup file;
    if (error_code == 0) {
      DispMsg = "Saved";
    }
    else {
      g_message (Backup_file, Write_err, error_code);
    }
  }
}

void flash_restore( enum nilop op )
{
  if ( not_running() ) {
    if ( checksum_backup() ) {
      report_err( ERR_INVALID );
    }
    else {
      xcopy( &PersistentRam, &BackupFlash, sizeof( PersistentRam ) );
      init_state();
      DispMsg = "Restored";
    }
  }
}

/*
 * Two routines that check that crc codes in memory regions
 * match the crc codes in file,
 * plus a helper function.
 */
#define CHECK_RETURN_0(f) {\
  if ( f != FR_OK ) {\
    if (FPT) f_close (FPT);\
    return 0;\
  }\
}

#define CHECK_RETURN_1(f) {\
  if ( f != FR_OK ) {\
    if (FPT) f_close (FPT);\
    return 1;\
  }\
}

int check_lib_file_crc () {
  // returns 1 if crc of library memory
  // is the same as crc in library file
  // so confusingly returns 0 if there's an error.
  unsigned short file_crc = 0;
  uint x = 0;
  FRESULT f;
  
  f = f_open( FPT, LIBRARY_FILE, FA_READ); // open library file for reading
  if (f != FR_OK) goto error;
  
  f = f_read (FPT, (char *) &file_crc, 2, &x);
  if (f != FR_OK) goto error;
  
  if (FPT) f_close (FPT);

  if (file_crc == UserFlash.crc) return 1;

 error:
  if (FPT) f_close (FPT);
  return 0;
}

int check_backup_file_crc () {
  // returns 1 if crc of backup memory
  // is the same as crc in backup file
  unsigned short file_crc = 0;
  uint x = 0;
  FRESULT f;
  
  f = f_open( FPT, BACKUP_FILE, FA_READ); // open backup file for reading
  if (f != FR_OK) goto error;
  
  f = f_lseek (FPT, sizeof (PersistentRam) - sizeof(short)); // move to crc
  if (f != FR_OK) goto error;

  f = f_read (FPT, (char *) &file_crc, 2, &x);
  if (f != FR_OK) goto error;

  if (FPT) f_close (FPT);

  if (file_crc == BackupFlash._crc) return 1;
  return 0;

 error:
  if (FPT) f_close (FPT);
  return 0;
}


//*************************************************************
//********************* Various WP34S functions copying data **
//********************* from backup or library to user memory *
//*************************************************************

/*
 *  Load the user program area from the backup.
 *  Called by PLOAD.
 */
void load_program( enum nilop op )
{
  if ( not_running() ) {
    if ( checksum_backup() ) {
      /*
       *  Not a valid backup
       */
      report_err( ERR_INVALID );
      return;
    }
    clpall();
    append_program( BackupFlash._prog, BackupFlash._prog_size );
  }
}

/*
 *  Load registers from backup
 */
void load_registers( enum nilop op )
{
  int count;
  if ( checksum_backup() ) {
    /*
     *  Not a valid backup region
     */
    report_err( ERR_INVALID );
    return;
  }
  count = NumRegs;
  if ( is_dblmode() ) {
    // Don't clobber the stack in DP mode
    count -= EXTRA_REG + STACK_SIZE;
  }
  if ( count > BackupFlash._numregs ) {
    count = BackupFlash._numregs;
  }
  xcopy( get_reg_n(0), get_flash_reg_n(0), count << 3 );
}

/*
 *  Load the statistical summation registers from backup
 */
void load_sigma( enum nilop op )
{
  if ( checksum_backup() ) {
    /*
     *  Not a valid backup region
     */
    report_err( ERR_INVALID );
    return;
  }
  if ( ! BackupFlash._state.have_stats ) {
    /*
     *  Backup has no data
     */
    report_err( ERR_MORE_POINTS );
    return;
  }
  sigmaCopy( ( (char *)( BackupFlash._regs + TOPREALREG - BackupFlash._numregs ) - sizeof( STAT_DATA ) ) );
}

/*
 *  Load the configuration data from the backup
 */
void load_state( enum nilop op )
{
  if ( not_running() ) {
    if ( checksum_backup() ) {
      /*
       *  Not a valid backup region
       */
      report_err( ERR_INVALID );
      return;
    }
    xcopy( &RandS1, &BackupFlash._rand_s1, (char *) &Crc - (char *) &RandS1 );
    init_state();
    clrretstk_pc();
  }
}

/*
 *  Load a user program from any region. Called by PRCL.
 */
void recall_program( enum nilop op )
{
  if ( not_running() ) {
    if ( state_pc() == 0 ) {
      State.pc = 1;
    }
    update_program_bounds( 1 );
    append_program( get_current_prog(), ProgEnd - ProgBegin + 1 );
  }
}

// ********************************************
// ******                               *******
// *****  Lots of file routines for DM42 ******
// *****  check/create directory,        ******
// ****** save/load library file, *************
// ****** save/load user memory file, *********
// ****** load backup memory file, ************
// ****** save/load program file, *************
// ****** store program from buffer, **********
// ****** open selected file callback. ********
// ******                               *******
// ********************************************

/*
 * Create working directory if needed
 */
int check_create_wp34sdir(void) {
  int f;
  sys_disk_write_enable(1);
  f = check_create_dir ("/wp34s");
  sys_disk_write_enable(0);
  return f;
}


#define DISP_NEW 1
#define OVERWRITE_CHECK 1
#define NO_DISP_NEW 0
#define NO_OVERWRITE_CHECK 0
#define WRITE 1
#define READ 2

/*
 * Writes a library file to disk.
 * If i = 0 the current library file on disk is copied to a backup file,
 * and then writes library memory to disk.
 * Otherwise, the user enters a library file name to save.
 * Called from:
 * i = 0: console.c EXIT; program_flash_new; load_lib_file 3 times!
 * i = 1: console.c Multi; Error report needed;
 * i = 2: Never called??? No - the backup lib file is created by renaming the existing lib file.
 */
int save_lib_file ( int i ) {
  FRESULT f = FR_OK;
  int fss;
  int data = WRITE;
  uint x=0;

  if ( power_check_screen() ) { // 1, so there's a problem
    return 99; // 99 is the power problem code (greater than any file error code)
  }

  sys_disk_write_enable(1);
  if (i == 0) {
    /*
      Before renaming a file it must be closed, if open.
      There are errors if the file to be renamed does not exist
      and if the file that it is being renamed to does exist.
      Logic: close FPT.
      Is there a file to be renamed? If not, break out.
      Remove the .bak file.
      Rename the file.
     */
    if (FPT) f_close (FPT); // close any open file before renaming file
    if (file_exists(LIBRARY_FILE)) {
      f_unlink(LIBRARY_FILE_BACKUP);
      f = f_rename (LIBRARY_FILE, LIBRARY_FILE_BACKUP); // make backup	
    }
    if (f != FR_OK) goto error; // anything other than success or missing file

    f = f_open (FPT, LIBRARY_FILE, FA_CREATE_ALWAYS | FA_READ | FA_WRITE); //  // overwrites existing file
    if (f != FR_OK) goto error;
  }
  else {
    clear_dots();
    fss = file_selection_screen ("Save Library File", "/wp34s", ".lib", open_selected_file, DISP_NEW, OVERWRITE_CHECK, &data );
    if (fss != 1) return 0; // user exit - no need for message
  }
  // File is now open with correct permissions

  f = f_write (FPT, (char *) &UserFlash, sizeof (UserFlash), &x);
  if ( f != FR_OK ) {
    if (i == 1) g_message (Lib_file, Write_err, f);
  }
  if (FPT) f_close (FPT);
  sys_disk_write_enable(0);
  return (int) f;

 error:
  if (FPT) f_close (FPT);
  sys_disk_write_enable(0);
  return (int) f;
}

int restore_lib () {
  if (!load_lib_file (0)) { // lib reloaded successfully
    DispMsg = "Restored"; 
    return 0;
  }
  else {
    DispMsg = "Lib lost!";
    init_library ();
    return 1;
  }
}

/*
If i = 0, loads the library file from disk into library memory.
If i = 2, loads the backup library file into library memory.
Else, the user gets to choose a .lib file to load.
 */
int load_lib_file ( int i ) {
  FRESULT f;
  int fss;
  int data = READ;
  uint bytes_read = 0;

  // Open file - return 1 if file can't be opened.
  if (i == 0) { // only called at start of main() in console.c
    f = f_open (FPT, LIBRARY_FILE, FA_READ); // open the standard library file
    if (f != FR_OK) goto error;
  }
  else {
    if (!check_lib_file_crc()) { // does disk file match memory?
      f = save_lib_file (0); // write memory to disk if crc wrong
      if (f != FR_OK) { // Can't make backup - continue?
	clear_disp(); // clears dots array and buffer
	lcd_refresh();
	lcd_switchFont(fReg, 3);
	fReg->inv = 0;
	lcd_printAt(fReg,1, "Problem writing");
	lcd_printAt(fReg,2, "library backup.");
	lcd_printAt(fReg,4, "Press ENTER or R/S");
	lcd_printAt(fReg,5, "to load new lib anyway,");
	lcd_printAt(fReg,6, "or any other key not to.");
	lcd_refresh();
	int c;
	do {
	  sys_sleep(); // wait for key press
	  c = key_pop();
	}
	while (c == 0); // go back to sleep if it's a key release

	if ( (c == KEY_ENTER) || (c == KEY_RUN) ) goto onwards;
	return 1;
      }
    }
  onwards:
    if (i == 2) { // open the backup library file!
      f = f_open (FPT, LIBRARY_FILE_BACKUP, FA_READ);
      if (f != FR_OK) {
	g_message (Backup_file, Open_err, f);
	return f;
      }
    }
    else { // i = 1: only called from do_multi() in console.c
      clear_dots();
      fss = file_selection_screen ("Load Library File", "/wp34s", ".lib", open_selected_file, NO_DISP_NEW, NO_OVERWRITE_CHECK, &data );
      if (fss != 1) return 1;
    }
  }

  // Correct file is now open with correct permissions

  // Read sizeof(UserFlash) bytes into UserFlash. 
  f = f_read (FPT, (char *) &UserFlash, sizeof (UserFlash), &bytes_read); // this may be gibberish

  if ( f != FR_OK ) {
    if (FPT) f_close (FPT);
    if (i == 0) return f; // let calling code handle it
    g_message (Lib_file, Read_err, f); // here, i = 1 or 2
    if (bytes_read > 0) { // if UserFlash has been overwritten at all...
      return restore_lib ();
    }
    else {
      DispMsg = "No change";
      return 0;
    }
  }

  if ( !(f_eof(FPT)) ) { // File too long - might not be valid?
    if (FPT) f_close (FPT);
    if (i == 0) return 1; // not proper!
    g_message ("File too", "LonG", 255);
    if ( (8190 < UserFlash.size) && (UserFlash.size < 10000) ) { // might be ok?
      UserFlash.size = 8190; // don't allow stepping past the end!
      checksum_region (&UserFlash, &UserFlash); // fix checksum
      g_message ("File","trUnCAtEd", 255);
      goto happy;
    }
    else {
      return restore_lib ();
    }
  }

  if (FPT) f_close (FPT);
  if ( i == 0 ) return 0;

  // check loaded file
  if ( checksum_general_region(&UserFlash) ) { // not a valid program collection
    g_message (Lib_file, CRC_err, 0);
    return restore_lib ();
  }
 happy:
  if ( (f = save_lib_file (0)) != FR_OK ) { // writes the new library file to disk and backs up old one
    DispMsg = "Not saved";
  }
  else {
    DispMsg = "Loaded ok";
  }
  return 0;
 error:
  if (FPT) f_close (FPT);
  return f;
}

/*
If i = 0 user memory is copied to the standard file for user memory.
If i = 1 user memory is copied to the backup file for user memory.
If i = 2 backup memory is copied to the standard file for backup memory.
Else the user chooses a .dat file to copy user memory to.
 */

int save_ram_file ( int i ) {
  FRESULT f;
  int fss;
  int data = WRITE;
  uint x = 0;

  process_cmdline_set_lift();
  init_state();
  checksum_ram();

  if ( power_check_screen() ) { // 1, so there's a problem
    return 99; // 99 is the power problem code (greater than any file error code)
  }

  sys_disk_write_enable(1);
  if (i == 0) { // save to state file
    f = f_open (FPT, STATE_FILE, FA_CREATE_ALWAYS | FA_READ | FA_WRITE); // overwrites existing file
    if (f != FR_OK) goto error;
  }
  else if (i == 1) { // save to user-chosen file
    clear_dots();
    fss = file_selection_screen ("Save RAM File", "/wp34s", ".dat", open_selected_file, DISP_NEW, OVERWRITE_CHECK, &data);
    if (fss != 1) return 0;
  }
  else if (i == 2) { // save to backup memory file
    f = f_open (FPT, BACKUP_FILE, FA_CREATE_ALWAYS | FA_READ | FA_WRITE); // overwrites existing file
    if (f != FR_OK) goto error;
  }
  else { // RAM file backup - used when loading a RAM file
    f = f_open (FPT, STATE_FILE_BACKUP, FA_CREATE_ALWAYS | FA_READ | FA_WRITE); // overwrites existing file
    if (f != FR_OK) goto error;
  }

  // File is now open with correct permissions
  if (i == 2) {
    f = f_write (FPT, (char *) &BackupFlash, sizeof (PersistentRam), &x);
  }
  else {
    f = f_write (FPT, (char *) &PersistentRam, sizeof (PersistentRam), &x);
  }

  if ( (f != FR_OK) && (i == 1) ) g_message (Ram_file, Write_err, f);

 error:
  if (FPT) f_close (FPT);
  sys_disk_write_enable(0);
  return (int) f;
}

/*
If i = 0 the state file is copied to user memory.
If i = 2, loads the backup state file into user memory.
Else the user chooses a .dat file to copy into user memory.
 */
int load_ram_file ( int i ) { // this will load backup files too!
  FRESULT f;
  int fss;
  int data = READ;
  uint x=0;
  
  if (i == 0) { // only called at start of main() in console.c
    f = f_open (FPT, STATE_FILE, FA_READ);
    if ( f != FR_OK ) goto error;
  }
  else if (i == 2) { // load the backup state file!
    f = f_open (FPT, STATE_FILE_BACKUP, FA_READ);
    if ( f != FR_OK ) goto error;
  }
  else {
    // only called from do_multi() in console.c
    // back up user memory first in case the loaded file corrupts memory
    if (save_ram_file (3)) { // backup fails
      clear_disp(); // clears dots array and buffer
      lcd_refresh();
      lcd_switchFont(fReg, 3);
      fReg->inv = 0;
      lcd_printAt(fReg,1, "Problem writing");
      lcd_printAt(fReg,2, "user memory backup.");
      lcd_printAt(fReg,4, "Press ENTER or R/S");
      lcd_printAt(fReg,5, "to load anyway,");
      lcd_printAt(fReg,6, "or any other key not to.");
      lcd_refresh();
      int c;
      do {
	sys_sleep(); // wait for key press
	c = key_pop();
      }
      while (c == 0); // go back to sleep if it's a key release

      if ( (c == KEY_ENTER) || (c == KEY_RUN) ) goto onwards;
      return 1;
    }
  onwards:
    clear_dots();
    fss = file_selection_screen ("Load RAM File", "/wp34s", ".dat", open_selected_file, NO_DISP_NEW, NO_OVERWRITE_CHECK, &data );
    if (fss != 1) return 1;
  }
  // File is now open with correct permissions
  // First, check file size.
  if ( f_size(FPT) != sizeof (PersistentRam) ) { 
    if (i != 1) goto error; // don't allow if this is real user memory or backup memory
    if (f_size(FPT) > ProgMax*2) { // won't fit in program memory - ProgMax is in steps of 2 bytes
      g_message ("FileSize", "ErrOr", 255);
      goto error;
    }
    // if i == 1 and it does fit in program memory, just continue!
  }
  
  f = f_read (FPT, (char *) &PersistentRam, sizeof (PersistentRam), &x);
  if ( f != FR_OK ) {
    if (i == 1) g_message (Ram_file, Read_err, f);
    goto error;
  }
  if ( !(f_eof(FPT)) ) {
    if (i == 1) g_message (Error_err, Eof_err, 0);
    goto error;
  }
  if (FPT) f_close (FPT);
  return 0;
 error:
  if (FPT) f_close (FPT);
  return 1;
}

/*
If i = 0 the state file is copied to user memory.
 */

int load_backup_file ( int i ) { // goes into backup! Only called at program start.
  FRESULT f;
  uint x=0;

  if (i == 0) { // only called at start of main() in console.c
    f = f_open (FPT, BACKUP_FILE, FA_READ);
    if (f != FR_OK) goto error;
    if ( f_size(FPT) != sizeof (PersistentRam) ) {
      f = 2;
      goto error;
    }
  }
  else return 1;

  // File is now open with correct permissions
  f = f_read (FPT, (char *) &BackupFlash, sizeof (BackupFlash), &x);
  if ( f != FR_OK ) goto error;
  if ( !(f_eof(FPT)) ) goto error;
  if (FPT) f_close (FPT);
  return 0;
 error:
  if (FPT) f_close (FPT);
  return f;
}

/*
 * Saves the current program to "disk".
 * Uses a RAM-sized buffer to store the program
 * along with its size and CRC
 * and so writes it in one go.
 */
void save_prog_file () { 
  opcode lbl; 
  unsigned int pc;
  unsigned short steps, prog_crc;
  FRESULT f;
  int data = WRITE;
  int fss;
  uint x=0;
  char* buffer;
  FLASH_REGION* fr;

  
  if ( not_running() ) {
    /*
     *  Don't copy from XROM
     */
    pc = nLIB( state_pc() );
    if ( pc == REGION_XROM ) {
      report_err( ERR_ILLEGAL );
      return;
    }
    /*
     *  Check if program is labeled
     */
    update_program_bounds( 1 );
    lbl = getprog( ProgBegin );
    if ( !isDBL(lbl) || opDBL(lbl) != DBL_LBL ) {
      report_err( ERR_NO_LBL );
      return;
    }
    /*
     *  Compute steps and crc for program
     */
    steps = 1 + ProgEnd - ProgBegin;
    prog_crc = crc16( get_current_prog(), steps << 1 );
    /*
     * Get filename and open file
     */
    sys_disk_write_enable(1);
    clear_dots();
    fss = file_selection_screen ("Save current program", "/wp34s", ".dat", open_selected_file, DISP_NEW, OVERWRITE_CHECK, &data );
    if (fss != 1) return;

    // File is now open with correct permissions

    int fsize = steps * 2 + 4;
    buffer = calloc (((fsize>>8)+1)<<8, 1); // between 1 and 256 bytes too big
    
    if (!buffer) {// calloc failure!
      f_close(FPT);
      sys_disk_write_enable(0);
      DispMsg = "No memory";
      return;
    }

    fr = (FLASH_REGION*) buffer;
    fr->size = steps;
    fr->crc = prog_crc;

    xcopy (fr->prog, get_current_prog(), steps << 1);
   
    f = f_write (FPT, buffer, 2*sizeof(short)+(steps << 1), &x);
    free(buffer);
    
    if (f != FR_OK) { //odd?
      g_message ("Progfile", Write_err, f);
    }
    else {
      DispMsg = "Written";
    }
      
    if (FPT) f_close(FPT);
    sys_disk_write_enable(0);
    
  }
}

void load_prog_file () {
  int fss, fsize;
  FRESULT f;
  int data = READ;
  uint x=0;
  FLASH_REGION* fr;
  unsigned char* buffer;

  clear_dots();
  fss = file_selection_screen ("Load PROG File", "/wp34s", ".dat", open_selected_file, NO_DISP_NEW, NO_OVERWRITE_CHECK, &data );
  if (fss != 1) return;

  // File is now open with correct permissions

  fsize = f_size(FPT); // size in bytes
  if (fsize > LIBRARY_SIZE) {// too big! Wrong file?
    f_close(FPT);
    DispMsg = "File too big";
    return;
  }
  buffer = calloc (((fsize>>8)+1)<<8, 1);
  // rounds buffer up to next multiple of 256 bytes
  // not sure if this is still necessary now we don't use pages
  // but it does no harm
  if (!buffer) {// calloc failure!
    f_close(FPT);
    DispMsg = "Mem short";
    return;
  }

  f = f_read (FPT, buffer, fsize, &x);
  if (f != FR_OK) { //odd?
    free(buffer);
    f_close(FPT);
    g_message ("Progfile", Read_err, f);
    return;
  }
  if (!f_eof(FPT)) { //odd?
    free(buffer);
    f_close(FPT);
    g_message (Error_err, Eof_err, 0);
    return;
  }
  f_close(FPT);

  fr = (FLASH_REGION*) buffer;
    
  if (checksum_general_region(fr)) {// valid program collection?
    g_message (Prog_file, CRC_err, 0);
    free(buffer);
    return;
  }

  store_program_from_buffer (fr);
  free(buffer);
  DispMsg = "Loaded";
}

/*
 * This is only used by load_program above
 * Program always added to flash library, not to RAM.
 */
void store_program_from_buffer( FLASH_REGION* fr )
{ // buffer contains all of program file
  unsigned int pc;
  int space_needed, count, free;

  if ( not_running() ) {
    /*
     *  Check if program is labeled
     */
    opcode lbl = (fr->prog)[0];
    if ( isDBL(lbl) ) {
      lbl |= (fr->prog)[1] << 16;
    }
    
    if ( !isDBL(lbl) || opDBL(lbl) != DBL_LBL ) {
      DispMsg = "No label";
      return;
    }
    /*
     *  Compute space needed
     */
    count = space_needed = fr->size;
    free = NUMPROG_FLASH_MAX - UserFlash.size;
    
    /*
     *  Find a duplicate label in the library and delete the program
     */
    pc = find_opcode_from( addrLIB( 0, REGION_LIBRARY ), lbl, 0 );
    if ( pc != 0 ) {
      /*
       *  CLP in library
       */
      unsigned int old_pc = state_pc();
      set_pc( pc );
      space_needed -= 1 + ProgEnd - ProgBegin;
      if ( space_needed <= free ) {
	clrprog();
      }
      set_pc( old_pc );
    }
    if ( space_needed > free ) {
      report_err( ERR_FLASH_FULL );
      return;
    }
    // 3. Append program
    flash_append( UserFlash.size, fr->prog, count, UserFlash.size + count );
  }
}

/*
 *  Save a user program to the library region. Called by PSTO.
 */
void store_program( enum nilop op )
{
  opcode lbl; 
  unsigned int pc;
  int space_needed, count, free;

  if ( not_running() ) {
    /*
     *  Don't copy from library or XROM
     */
    pc = nLIB( state_pc() );
    if ( pc == REGION_LIBRARY || pc == REGION_XROM ) {
      report_err( ERR_ILLEGAL );
      return;
    }
    /*
     *  Check if program is labeled
     */
    update_program_bounds( 1 );
    lbl = getprog( ProgBegin );
    if ( !isDBL(lbl) || opDBL(lbl) != DBL_LBL ) {
      report_err( ERR_NO_LBL );
      return;
    }
    /*
     *  Compute space needed
     */
    count = space_needed = 1 + ProgEnd - ProgBegin;
    free = NUMPROG_FLASH_MAX - UserFlash.size;

    /*
     *  Find a duplicate label in the library and delete the program
     */
    pc = find_opcode_from( addrLIB( 0, REGION_LIBRARY ), lbl, 0 );
    if ( pc != 0 ) {
      /*
       *  CLP in library
       */
      unsigned int old_pc = state_pc();
      set_pc( pc );
      space_needed -= 1 + ProgEnd - ProgBegin;
      if ( space_needed <= free ) {
	clrprog();
      }
      set_pc( old_pc );
    }
    if ( space_needed > free ) {
      report_err( ERR_FLASH_FULL );
      return;
    }
    // 3. Append program
    flash_append( UserFlash.size, get_current_prog(), count, UserFlash.size + count );
  }
}

int open_selected_file (const char * fpath, const char * fname, void * data) {
  FRESULT f;

  if (*(int*)data == WRITE) {
    // open file to write
    f = f_open (FPT, fpath, FA_CREATE_ALWAYS | FA_READ | FA_WRITE);
  }
  else if (*(int*)data == READ) {
    // open file to read
    f = f_open (FPT, fpath, FA_READ);
  }
  else {
    DispMsg = "data=3??";
    return 3; // serious error!
  }
  if (f != FR_OK) {
    if (FPT) f_close (FPT);
    sys_disk_write_enable(0);
    g_message ("Selected", Open_err, f);
    return 2; // file can't be opened
  }
  else {
    return 1; // file opened successfully
  }
}

