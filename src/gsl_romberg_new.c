#include "nd_integration.h"

#define printf(...)
#define PF(A) printf (#A ": %.15e\n", A)
#define PI(A) printf (#A ": %i\n", A)

#define ROMB_K 5
#define ROMB_MAX 10

/* double f_direct (double x, void * params) { */
/*   decimal64 dx, s, temp; */
  
/*   db_to_dn (x, &dx);   */
/*   int_to_dn (&s, 1); */

/*   dn_add (&s, &s, decNumberSin (&temp, &dx)); */
/*   dn_add (&s, &s, decNumberCos (&temp, &dx)); */
/*   dn_multiply (&s, &s, &dx); */

/*   //  return x * (sin(x)+cos(x)+1); */
/*   return dn_to_db (&s); */
/* } */

/*
WP 34S commands are classified by their arguments.
Here, two types are relevant: RARG and multi.
A RARG (right argument) function here takes a local label.
A multi function takes a global label (up to three characters, hence "multi").
Most of the code is identical, but there are differences when it comes to
finding the location in program memory of the label.
In these function names, qag refers to the integration routine used.
 */

void cmd_romb_integrate ();
  
// This is the multi-version.
void cmd_multi_romb (const opcode o, enum multiops mopr) {
  func_addr = findmultilbl (o, FIND_OP_ERROR);
  if (func_addr) {
    cmd_romb_integrate ();
  }
  return;
}

// This is the RARG version.
void cmd_rarg_romb (unsigned int arg, enum rarg op) {
  func_addr = find_label_from(state_pc(), arg, FIND_OP_ERROR | FIND_OP_ENDS);
  if (func_addr) {
    cmd_romb_integrate ();
  }
  return;
}

/*
The following function is common to both multi and RARG.
Initially, it simply reads the integration limits from the stack.
a, b are the integration limits.
*/ 

void cmd_romb_integrate () {
  decNumber a_dn, b_dn, scale_dn;
  decNumber answer_dn, error_dn;
  double a, b, answer, error, epsrel, epsabs;
  int status = 0;
  int dispmode, pdigs;
  char q[] = "            "; // 12 spaces
  decNumber plus_inf, minus_inf;
  set_inf (&plus_inf);
  set_neginf (&minus_inf);
  
  gsl_function F;
  F.function = &f_user_double;
  //  F.function = &f_direct;
  F.params = NULL;

  int running = 0;
  unsigned int pc;
  if (Running) { // should work when called from a running program
    running = 1;
    pc = state_pc ();
  }
    
  // Set flagD, storing its previous state
  int flagD = get_user_flag (regD_idx);
  set_user_flag (regD_idx);

  dot (RCL_annun, 1);
  lcd_refresh ();

  //********************Set up a, b, scale
  //
  scale = 1.; // static variable; reset it just in case.
  /* scale is the third parameter if Flag A is set. */

  getY (&a_dn);
  getX (&b_dn);

  if (get_user_flag (regA_idx)) { // Flag A set - ZYX = SAB 
    if (!running) delay ( 500, "ZYX=s;ab");
    getRegister (&scale_dn, regZ_idx);
    scale = fabs (dn_to_db (&scale_dn));
    if (scale==0) { // zero scale not allowed
      delay (2500, "scale0!");
      dot (RCL_annun, 0); // turn off RUN	
      return;
    }
  }
  else { // YX = AB
    if (!running) delay ( 500, "YX=ab");
  }

  a = dn_to_db (&a_dn);
  b = dn_to_db (&b_dn);

  // Return zero if a == b
  if (a == b) {
    decNumberZero (&answer_dn);
    decNumberZero (&error_dn);
    goto finish;
  }
  // check order of a and b; Should I have sign?
  if (a > b) {
    double c = b;
    b = a;
    a = c;
  }
    
  //********************Set up epsrel and epsabs
  //
  dispmode = get_dispmode_digs (&pdigs);
  if (dispmode == MODE_STD) { // STD mode
    epsrel = 1e-13;
    epsabs = 0;
  }
  else if (dispmode == MODE_FIX) { // FIX mode
    epsabs = pow (10., -pdigs);
    epsrel = 1e-13;
  }
  else { // SCI etc. mode
    epsrel = pow (10., -pdigs);
    epsabs = 0;
  }

  // Allocate workspace; return on failure
  gsl_integration_romberg_workspace * w  = gsl_integration_romberg_alloc (14);
  if (!w) {
    delay (2500, "!Malloc");
    dot (RCL_annun, 0); // turn off RUN
    return;
  }
  unsigned int neval = 0;
  // Choose routine
  if (isfinite(a)) {
    if (isfinite(b)) { // a finite, b finite
      status = qromo_u (&F, a, b, epsabs, epsrel, &answer, &error, &neval, w);
    }
    else { // a finite, b infinite
      status = qromo_iu (&F, a, epsabs, epsrel, &answer, &error, &neval, w);
    }
  }
  else {
    if (isfinite(b)) { // a infinite, b finite
      status = qromo_il (&F, b, epsabs, epsrel, &answer, &error, &neval, w);
    }
    else { // a and b infinite
      status = qromo_i (&F, epsabs, epsrel, &answer, &error, &neval, w);
    }
  }

  // Free workspace
  gsl_integration_romberg_free (w);

  // Error message if error
  if (status != 0) {
    sprintf (q, "Err:%2i", status);
    delay (2500, q);
  }

 finish:  // Restore parameters
  if (get_user_flag (regA_idx)) { // Flag A set - ZYX = SAB 
    setRegister (regA_idx, &scale_dn);
  }
  setRegister (regT_idx, &a_dn);
  setRegister (regZ_idx, &b_dn);

  // Put error, answer into Y, X
  db_to_dn (error, &error_dn);
  setY (&error_dn);
  db_to_dn (answer, &answer_dn);
  setX (&answer_dn);

  // Restore flag D
  put_user_flag (regD_idx, flagD);

  // Restore previous state if called from program
  if (running) {
    set_running_on ();
    ext_raw_set_pc (pc);
  }
  dot (RCL_annun, 0); // turn off RUN
  cmdmsg (MSG_INTEGRATE, 0); // display integration sign
}

// **********************************************************************
// ****************** Workspace functions *************
gsl_integration_romberg_workspace * gsl_integration_romberg_alloc(const size_t n)
{
  gsl_integration_romberg_workspace *w;

  /* check inputs */
  if (n < 1)
    {
      GSL_ERROR_VAL ("workspace size n must be at least 1", GSL_EDOM, 0);
    }

  w = calloc(1, sizeof(gsl_integration_romberg_workspace));
  if (w == NULL)
    {
      GSL_ERROR_VAL ("unable to allocate workspace", GSL_ENOMEM, 0);
    }

  /* ceiling on n, since the number of points is 2^n + 1 */
  w->n = GSL_MIN(n, 30);

  w->work1 = malloc(w->n * sizeof(double));
  if (w->work1 == NULL)
    {
      gsl_integration_romberg_free(w);
      GSL_ERROR_VAL ("unable to allocate previous row", GSL_ENOMEM, 0);
    }

  w->work2 = malloc(w->n * sizeof(double));
  if (w->work2 == NULL)
    {
      gsl_integration_romberg_free(w);
      GSL_ERROR_VAL ("unable to allocate current row", GSL_ENOMEM, 0);
    }

  return w;
}

void gsl_integration_romberg_free(gsl_integration_romberg_workspace * w)
{
  if (w->work1)
    free(w->work1);

  if (w->work2)
    free(w->work2);

  free(w);
}

// ***********Calling functions
int qromo_i (gsl_function * f,
	     double epsabs, double epsrel,
	     double * result, double * error,
	     size_t * neval,
	     gsl_integration_romberg_workspace * workspace)
{
  int status;
  gsl_function f_transform;

  f_transform.function = &i_transform;
  f_transform.params = f;

  status = qromo (&f_transform, 0.0, 1.0, 
		  epsabs, epsrel, 
		  result, error,
		  neval,
		  workspace);

  return status;
}

int qromo_u (gsl_function * f,
	     const double a, const double b,
	     const double epsabs, const double epsrel,
	     double * result, double * error,
	     size_t * neval,
	     gsl_integration_romberg_workspace * workspace)
{
  int status;

  
  gsl_function f_transform;
  f_transform.function = u_transform;
  
  struct u_params transform_params;

  transform_params.a = a;
  transform_params.b = b;
  transform_params.f = f;

  f_transform.params = &transform_params;

  status = qromo (&f_transform, -1.0, 1.0, 
		  epsabs, epsrel,
		  result, error,
		  neval, workspace);
  return status;
}

int qromo_il (gsl_function * f,
	      double b,
	      double epsabs, double epsrel,
	      double * result, double * error,
	      size_t * neval,
	      gsl_integration_romberg_workspace * workspace)
{
  int status;
  gsl_function f_transform;
  struct il_params transform_params  ;

  transform_params.b = b ;
  transform_params.f = f ;

  f_transform.function = &il_transform;
  f_transform.params = &transform_params;

  status = qromo (&f_transform, 0.0, 1.0, 
		  epsabs, epsrel, 
		  result, error,
		  neval,
		  workspace);

  return status;
}

int qromo_iu (gsl_function * f,
	      double a,
	      double epsabs, double epsrel,
	      double * result, double * error,
	      size_t * neval,
	      gsl_integration_romberg_workspace * workspace)
{
  int status;
  gsl_function f_transform;
  struct iu_params transform_params  ;

  transform_params.a = a ;
  transform_params.f = f ;

  f_transform.function = &iu_transform;
  f_transform.params = &transform_params;

  status = qromo (&f_transform, 0.0, 1.0, 
		  epsabs, epsrel, 
		  result, error,
		  neval,
		  workspace);

  return status;
}


// **********************************************************************
// **** qromo, polint, midpnt *********

int polint ( double * xa, double * ya, double * result, double * error)
{
  int n = ROMB_K; // e.g., 5
  int max = n - 1; // e.g., 4
  
  double c[ROMB_K], d[ROMB_K];
  int ns = max; // last x-entry is closest to zero

  *result = ya[ns]; // initial approximation to result
  double w;
  
  for (int i = 0; i < ROMB_K; i++) {
    c[i] = ya[i];
    d[i] = ya[i];
  }

  ns--;
  double ninepow = 1.;
  for (int m = 1; m < n; m++) {
    ninepow *= 4.;
    for (int i = 0; i <= (max - m); i++) {
      /*
	This little section refers to code that has been deleted
	because what is needed here is not all that general.
	xa[i] = k.9^-i
	So den = k(9^-i - 9^-i.9^-m) = k.9^-i.(1-9^-m)
	Then den = w.(1/k).9^i.(1-9^-m)^-1
	d[i] goes to k.9^-i.9^-m.w.(1/k).9^i.(1-9^-m)^-1
	=(9^-m).w/(1-9^-m) = w/(9^m-1)
	c[i] goes to w/(1-9^-m) = 9^m.w/(9^m-1)
       */
      w = c[i+1]-d[i];
      d[i] = w / (ninepow - 1);
      c[i] = w * ninepow / (ninepow - 1);
    }
    *error = d[ns];
    ns--;
    *result += *error;
  }
  return 0;	
}

/* void midpnt (const gsl_function *f, const double a, const double b, */
/* 	     double *s, */
/* 	     const int n) { */
/*   static int points; */
/*   double x, sum, del; */
/*   if (n == 1) { */
/*     *s = (b-a) * GSL_FN_EVAL (f, (a+b)/2.); */
/*     points = 1; */
/*   } */
/*   else { */
/*     del = (b-a)/(3.*points); */
/*     x = a + del/2.; */
/*     sum = 0; */
/*     for (int j = 1; j <= points; j++) { // no. added to points is 2*points....  */
/*       sum += GSL_FN_EVAL (f, x); */
/*       x += del + del; */
/*       sum += GSL_FN_EVAL (f, x); */
/*       x += del; */
/*     /\*   if (! key_empty()) { *\/ */
/*     /\* 	if (key_pop() == KEY_BSP) { *\/ */
/*     /\* 	  key_push (KEY_BSP); *\/ */
/*     /\* 	  *s = (*s * (points-j)/points) + ((*s * j + (b-a)*sum)/points)/3.; // best guess *\/ */
/*     /\* 	  return; *\/ */
/*     /\* 	} *\/ */
/*     /\* 	key_pop_all (); *\/ */
/*     /\*   } *\/ */
/*     } */
/*     *s = (*s + (b-a) * sum / points) / 3.; */
/*     points *= 3; //..bringing the total no. of points to the final value of points! */
/*     char u[] = "          "; */
/*     sprintf (u, "%.7i", points); */
/*     delay (500, u); */
/*   } */
/*   return; */
/* } */

void midpnt (const gsl_function *f, const double a, const double b,
	     double *s,
	     const int n) {
  static int points;
  double x, sum, del;
  sum = 0;
  if (n == 1) {
    *s = (b-a) * GSL_FN_EVAL (f, (a+b)/2.);
    points = 2;
  }
  else {
    del = (b-a)/points;
    x = a + del/2.;
    sum = 0;
    for (int j = 1; j <= points; j++) { // no. added to points is 2*points.... 
      sum += GSL_FN_EVAL (f, x);
      x += del;
    }
    *s = (b-a) * sum / points;
    /* char u[] = "          "; */
    /* sprintf (u, "%.7i", points); */
    /* delay (500, u); */
    points *= 2;
  }
  return;
}

int qromo (gsl_function *f, const double a, const double b,
	   const double epsabs, const double epsrel,
	   double * result, double * error,
	   size_t * neval,
	   gsl_integration_romberg_workspace *w) {
  int j;
  decNumber dn;
  double *h = &(w->work1[0]); /* previous row */
  double *s = &(w->work2[0]); /* current row */
  h[0] = 1;
  double prev_int = 0.;
  double romb_err;
  //  double int_err;
  
  for (j = 0; j < (w->n); j++) {
    midpnt (f, a, b, &s[j], j+1);
    if (j == ROMB_K - 2) prev_int = s[j];
    // this is the (j+1)th step;
    if ( j+1 >= ROMB_K ) { // extrapolate once we have ROMB_K points and keep on doing so
      polint (&h[j+1-ROMB_K], &s[j+1-ROMB_K], result, &romb_err);
      *error = fabs (*result - prev_int);
      /* if (romb_err > int_err) { */
      /* 	*error = romb_err; */
      /* } */
      /* else { */
      /* 	*error = int_err; */
      /* } */
      //      *error = romb_err;
      if ((fabs(*error) < epsabs) || (fabs(*error) < epsrel * (*result))) {
	*neval = j;
	return 0;
      }
      prev_int = *result;
      setX (db_to_dn (*result, &dn));
      setY (db_to_dn (*error, &dn));
      cmdmsg (MSG_INTEGRATE, 0);
      if (! key_empty()) {
	if (key_pop() == KEY_BSP) {
	  break;
	}
	key_pop_all ();
      }
    }
    s[j+1] = s[j];
    h[j+1] = h[j] / 9.;
  }
  return 1; // possible error - has overrun?
}

