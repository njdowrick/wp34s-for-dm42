/*
This is the code for the updated version of the double exponential integration routine
found in the document qthsh.pdf.
It is close to unreadable because most of it uses decNumber arithmetic routines!
*/

#include "nd_integration.h"

//Declarations:
void cmd_ei2_with_c ();
void dbl_exp_int_abc (decNumber* a, decNumber* b, const decNumber* c_in, decNumber* answer, decNumber* error);
static decNumber* exp_sinh_opt_d (const decNumber* a, const decNumber* eps, decNumber* d);

/*
WP 34S commands are classified by their arguments.
Here, two types are relevant: RARG and multi.
A RARG (right argument) function here takes a local label.
A multi function takes a global label (up to three characters, hence "multi").
Most of the code is identical, but there are differences when it comes to
finding the location in program memory of the label.
In these function names, ei2 stands for 
(double) exponential integral, version 2.
 */

// This is the multi-version.
void cmd_multi_ei2 (const opcode o, enum multiops mopr) {
  func_addr = findmultilbl (o, FIND_OP_ERROR);
  if (func_addr) {
    cmd_ei2_with_c ();
  }
  return;
}

// This is the RARG version.
void cmd_rarg_ei2 (unsigned int arg, enum rarg op) {
  func_addr = find_label_from(state_pc(), arg, FIND_OP_ERROR | FIND_OP_ENDS);
  if (func_addr) {
    cmd_ei2_with_c ();
  }
  return;
}

/*
The following function is common to both multi and RARG.
It reads either two arguments (a, b) or three arguments (a, c, b)
from the stack.
a, b are the integration limits.
c is an extra parameter that allows more user control over the integration.
*/ 

void cmd_ei2_with_c () {
  decNumber a, b, c, answer, error;

  int running = 0;
  unsigned int pc_0 = 0;
  
  decNumberZero (&answer);
  decNumberZero (&error);
  
  if (Running) { // should work when called from a running program
    running = 1;
    pc_0 = state_pc ();
  }

  // Set flagD, storing its previous state
  int flagD = get_user_flag (regD_idx);
  set_user_flag (regD_idx);
    
  dot (RCL_annun, 1);
  lcd_refresh ();

  // Three arguments read if flag A is set (big "=" sign displayed)
  
  if (get_user_flag (regA_idx)) { // Flag A set - ZYX = ACB 
    getRegister (&a, regZ_idx);
    getY (&c);
    getX (&b);
    if (!running) delay ( 500, "ZYX=acb");
    if ( !(dn_gt (&c, &a) && dn_gt (&b, &c)) ) {
      if (!running) delay (1000, "Need a<c<b");
      return;
    }
    if (decNumberIsFinite (&a) && decNumberIsFinite (&b)) {
      // Both limits finite - c is split point; do two integrals
      if (!running) delay (500, "I1:");
      dbl_exp_int_abc (&a, &c, &const_NaN, &answer, &error);
      if (!running) delay (500, "I2:");
      dbl_exp_int_abc (&c, &b, &const_NaN, &answer, &error);
      // answer and error accumulated in function;
      // error should be added in quadrature, so this slightly overestimates it
      // Restore arguments
      setRegister (regA_idx, &a); 
      setRegister (regT_idx, &c); 
      setRegister (regZ_idx, &b); 
    }
    else {
      // at least one infinite limit - pass c directly
      dbl_exp_int_abc (&a, &b, &c, &answer, &error);
      setRegister (regT_idx, &a); 
      setRegister (regZ_idx, &b); 
    }
  }
  else { // YX = ab; flag A not set
    getY (&a);
    getX (&b);
    if (!running) delay (500, "YX=AB");
    dbl_exp_int_abc (&a, &b, &const_NaN, &answer, &error);
    setRegister (regT_idx, &a); 
    setRegister (regZ_idx, &b); 
  }

  setX (&answer);
  setY (&error);

  // Restore previous state if called from program
  if (running) {
    set_running_on ();
    ext_raw_set_pc (pc_0);
  }
  // Restore flag D
  put_user_flag (regD_idx, flagD);
  
  dot (RCL_annun, 0); // turn off RUN
  cmdmsg (MSG_INTEGRATE, 0); // display integration sign
}
    
/* 
   This function actually does the integration. It's called by the previous function.
   Arguments: func_addr - address of user function;
   a, b - integration limits; c_in - extra parameter;
   answer, error - integration result and error estimate are returned in these.
*/

void dbl_exp_int_abc (decNumber* a, decNumber* b, const decNumber* c_in, decNumber* answer, decNumber* error) {
  /*
    func_addr is the program step of the function being integrated.
    a, b are the integration limits
    c_in means:
    * in TS (tanh-sinh) mode (both limits finite) - split integration range here: do a->c + c->b.
      This is dealt with in the calling function, not here.
    * in ES (exp-sinh) mode (one infinite limit) - int(a->c) + int(c->inf) or int(-inf->c) + int(c->b)
    * in SS (sinh-sinh) mode (two infinite limits) - "c" - the central point of the integral.
    If c_in is NaN it's ignored, and the integral from a to b is done, 
    with no split (TS), auto-generated value of d (ES), or c=0 (SS).
   */

  decNumber c, d, s, e, result, v, h, y, eps;
  decNumber s1, s2, s3; // scratch variables

  int sign = 1, k = 0, mode = 0; // Tanh-Sinh = 0, Exp-Sinh = 1, Sinh-Sinh = 2

  int pdigs, dispmode, alogeps = -34;
  const int max_levels = 10;

  // Find eps
  if (!is_dblmode()) {
    dispmode = get_dispmode_digs (&pdigs);
    
    if (dispmode == MODE_STD) {
      alogeps = -15;
    }
    else {
      alogeps = -(pdigs+3);
    }
  }
  
  db_to_dn ((double) alogeps, &s1);
  dn_power (&eps, &const_10, &s1); // eps = 10^-34 or 10^-(dispdigs+3) or 10^-15
  
  decNumberZero (&c);
  decNumberCopy (&d, &const_1);
  decNumberCopy (&h, &const_2);
  
  if (decNumberIsNaN (a) || decNumberIsNaN (b)) {
    return;
  }
  if ( (decNumberIsFinite (a)) && (decNumberIsFinite (b))) { // TS - c_in dealt with in calling routine
    dn_add (&c, a, b);
    dn_divide (&c, &c, &const_2);

    dn_subtract (&d, b, a);
    dn_divide (&d, &d, &const_2);

    decNumberCopy (&v, &c);    
  }
  else if (decNumberIsFinite(a)) { // int from a to infinity
    mode = 1; // Exp-Sinh
    decNumberCopy(&c, a); // c = a

    if (decNumberIsSpecial (c_in)) {
      // use auto-generated d if c_in not provided
      decNumberCopy (&d, exp_sinh_opt_d(a, &eps, &d));
      dn_add(&v, a, &d); // v = a + d
    }
    else {
      decNumberCopy (&v, c_in); // use v = c_in
      dn_subtract (&d, &v, a); // d = v - a
    }
  }
  else if (decNumberIsFinite(b)) { // int from -infinity to b 
    mode = 1; // Exp-Sinh
    decNumberCopy (&c, b); // c = b
    sign = -sign;
    
    if (decNumberIsSpecial (c_in)) {
      decNumberCopy (&d, exp_sinh_opt_d(b, &eps, dn_minus(&s1, &d)));
      // use auto-generated d if c_in not provided; d now negative
      dn_add (&v, b, &d); // v = b + d
    }
    else {
      decNumberCopy (&v, c_in); // v = c_in (c_in less than b)
      dn_subtract (&d, &v, b); // d = v - b (so d is negative)
    }
    
  }
  else {
    mode = 2; // Sinh-Sinh

    if (decNumberIsSpecial (c_in)) {
      decNumberZero (&v);
    }
    else {
      decNumberCopy (&c, c_in);
      decNumberCopy (&v, &c);
    }
      
  }
  // Up to here, a, b, c, d, v, and mode have been set.
  // k has been set to zero; h is 2.

  call_user_function (&v, &s);
  // Now s = f(v) is set too.
  do {
    decNumber p, q, fp, fm, t, eh;
    decNumberZero (&p);
    decNumberZero (&fp);
    decNumberZero (&fm);

    dn_divide (&h, &h, &const_2);
    dn_exp (&eh, &h);
    decNumberCopy (&t, &eh);

    if (k > 0)
      dn_multiply (&eh, &eh, &eh);
    // This last bit was relevant to all routines.
    if (mode == 0) { // Tanh-Sinh
      do {
	decNumber u, r, w, x;

	dn_divide (&s1, &const_1, &t); // s1 stores 1/t
	dn_subtract (&u, &s1, &t);
	dn_exp (&u, &u); // u = exp(1/t-t)

	dn_add (&r, &u, &const_1);
	dn_divide (&r, &u, &r);
	dn_multiply (&r, &r, &const_2); // r = 2*u/(1+u);

	dn_add (&s2, &t, &s1);
	dn_multiply (&w, &r, &s2);
	dn_add (&s2, &u, &const_1);
	dn_divide (&w, &w, &s2); // w = (t+1/t)*r/(1+u);

	dn_multiply (&x, &d, &r); // x = d*r;

	dn_add (&s1, a, &x);
	if (dn_gt (&s1, a)) { // if too close to a then reuse previous fp
	  call_user_function (&s1, &y);
	  if (decNumberIsFinite (&y))
	    decNumberCopy (&fp, &y);  // if f(x) is finite, add to local sum
	}
	
	dn_subtract (&s1, b, &x);
	if (dn_lt (&s1, b)) { // if too close to a then reuse previous fp
	  call_user_function (&s1, &y);
	  if (decNumberIsFinite (&y))
	    decNumberCopy (&fm, &y);  // if f(x) is finite, add to local sum
	}
	
	dn_add (&s1, &fp, &fm);
	dn_multiply (&q, &s1, &w); // q = w*(fp+fm)
	dn_add (&p, &p, &q); // p += q
	dn_multiply (&t, &t, &eh); // t *= eh

	dn_multiply (&s2, &eps, dn_abs (&s1, &p)); // s2 = eps*abs(p)
      } while (dn_gt (dn_abs (&s1, &q), &s2));
    }

    else {
      dn_divide (&t, &t, &const_2);
      do {
	decNumber r, w, x;

	dn_multiply (&s1, &t, &const_4);
	dn_divide (&s1, &const_1, &s1);
	dn_subtract (&s1, &t, &s1);
	dn_exp (&r, &s1); // r = exp(t-0.25/t)
	
	decNumberCopy (&w, &r);

	decNumberZero (&q);
	
	if (mode == 1) { // Exp-Sinh
	  dn_add (&x, &c, dn_divide (&s1, &d, &r)); // x = c+d/r; // d/r in s1
	  if (dn_eq (&x, &c))
	    break;
	  call_user_function (&x, &y);
	  if (decNumberIsFinite (&y))  // if f(x) is finite, add to local sum
	    dn_add (&q, &q, dn_divide (&s1, &y, &w));
	}
	else { // Sinh-Sinh
	  dn_subtract (&s1, &r, dn_divide (&s2, &const_1, &r));
	  dn_divide (&r, &s1, &const_2); // r = (r-1/r)/2
	  
	  dn_add (&s1, &w, dn_divide (&s2, &const_1, &w));
	  dn_divide (&w, &s1, &const_2); // w = (w+1/w)/2
	  
	  dn_subtract (&x, &c, dn_multiply (&s1, &d, &r)); // x = c-d*r; // d*r in s1
	  call_user_function (&x, &y);
	  if (decNumberIsFinite (&y))  // if f(x) is finite, add to local sum
	    dn_add (&q, &q, dn_multiply (&s1, &y, &w));
	}

	dn_add (&x, &c, dn_multiply (&s1, &d, &r)); // x = c+d*r; // d*r in s1

	call_user_function (&x, &y);

	if (decNumberIsFinite (&y))  // if f(x) is finite, add to local sum
	  dn_add (&q, &q, dn_multiply (&s1, &y, &w));

	dn_divide (&s1, &const_1, dn_multiply (&s2, &t, &const_4)); //s1 = 1/(4t)
	dn_multiply (&q, &q, dn_add (&s2, &t, &s1)); // q = q * (t + 1/4t)
	dn_add (&p, &p, &q); // p += q;
	dn_multiply (&t, &t, &eh); // t *= eh;
	
	dn_multiply (&s2, &eps, dn_abs (&s1, &p)); // s2 = eps*abs(p)
      } while (dn_gt (dn_abs (&s1, &q), &s2)); // while abs(q) > eps*abs(p)
    }


    //    dn_multiply (&v, &s, &const_2); // v=2*s
    //    dn_add (&s, &s, &p); // s+=p

    //    dn_abs (&s1, &s); // s1 = abs(s)
    //    dn_abs (&s2, dn_subtract (&s3, &v, &s)); // s2 = abs(v-s)

    dn_subtract (&v, &s, &p); // v = s - p
    dn_add (&s, &p, &s); // s+=p

    dn_abs (&s1, &s); // s1 = abs(s)
    dn_abs (&s2, &v); // s2 = abs(v)
    ++k;
    // We want to calculate an estimate of the integral here.
    // It is sign*s*h*d.
    dn_multiply (&result, &d, dn_multiply (&s3, &s, &h));
    if (sign == -1)
      dn_minus (&result, &result);
    setX (&result);
    // We might as well have the relative error as well!
    //    dn_divide (&e, &s2, dn_add (&s3, &s1, &eps)); // e = abs(v) / (abs(s) + eps)
    dn_divide (&e, &s2, dn_add (&s3, &s1, &s2)); // e = abs(v) / (abs(s) + abs(v)) - ND change
    if (decNumberIsNaN (&e)) decNumberCopy (&e, &const_1); // only happens when v, s both zero
    setY (&e);
    cmdmsg (MSG_INTEGRATE, 0);
    if (! key_empty()) {
      if (IS_EXIT_KEY(key_pop())) {
	key_pop_all ();
	break;
      }
      key_pop_all ();
    }
  } while (dn_gt ( &s2, dn_multiply (&s3, &const_10, dn_multiply (&s3, &eps, &s1))) && k <= max_levels);
  dn_add (answer, answer, &result);
  dn_add (error, error, &e);
  return;
}

// return optimized Exp-Sinh integral split point d
// This code also comes from the qthsh.pdf document
// Much of it uses ordinary numbers - makes it easier to write
// and a little faster. decNumbers aren't needed.

/*
  Helper function called by exp_sinh_opt_d routine to obtain values
  of fl, fr, and h by calling the user function.
*/
static void call_user_adr (const decNumber* a, const decNumber* d, const decNumber* r,
			      decNumber* fl, decNumber* fr, decNumber* h) {
  decNumber s1;
  dn_divide ( &s1, d, r); // s1 = d/r
  dn_add (&s1, a, &s1); // s1 = a+d/r
  call_user_function (&s1, fl); // fl = f(a+d/r)
  
  dn_multiply (&s1, d, r); // s1 = d*r
  dn_add (&s1, a, &s1); // s1 = a+d*r
  call_user_function (&s1, fr); // fr = f(a+d*r)
  dn_multiply (fr, r, fr);
  dn_multiply (fr, r, fr); // fr = r*r*f(a+d*r)

  dn_subtract (h, fl, fr); // h = fl - fr
}

/*
  A couple of macros used in exp_sinh_opt_d
  and undefined afterwards.
*/

#define IS_INFINITE(X) (decNumberIsSpecial(X))
#define IS_FINITE(X) (decNumberIsFinite(X))
#define DN_SIGN(X) (decNumberIsZero(X) ? 0 : (decNumberIsNegative(X) ? -1 : 1))

/*
  exp_sinh_opt_d eturns optimized Exp-Sinh (i.e, semi-infinite) 
  integral split point d.
  This code comes from the qthsh.pdf document.
  It attempts to estimate the maximum value of x*f(x)
  and to use that as the split point, rather than the default value of 1.
  It works surprisingly well given the small number of function evaluations it uses.
  See the discussion towards the end of this thread:
  https://www.hpmuseum.org/forum/thread-16549-post-184118.html#pid184118
*/

static decNumber* exp_sinh_opt_d (const decNumber* a, const decNumber* eps, decNumber* d) {

  decNumber fl, fr, h2;
  decNumber r, h;
  decNumber lfl, lfr, lr, s;
  
  call_user_adr (a, d, &const_2, &fl, &fr, &h2);
  
  if ( IS_INFINITE(&h2) || (decNumberIsZero(&fl) && decNumberIsZero(&fr)) ) return d;
  // function undefined or zero - don't bother.
  // Not clear that the "zero" matters here.
  
  uint i = 1, j = 32; // j=32 is optimal to find r
  //  decNumber s1; // scratch variable 

  decNumberZero (&s); // 
  decNumberCopy (&lr, &const_2);
  do { // find max j such that fl and fr are both finite - will usually be 16.
    j /= 2;
    //    uInt32ToReal (1 << (i + j), &r);
    ullint_to_dn (&r, 1 << (i + j));
    call_user_adr (a, d, &r, &fl, &fr, &h);
  } while (j > 1 && IS_INFINITE(&h));

  if (j > 1 && IS_FINITE(&h) && (DN_SIGN(&h) != DN_SIGN(&h2))) {
    decNumberCopy (&lfl, &fl);
    decNumberCopy (&lfr, &fr);
    
    do { // bisect in 4 iterations
      j /= 2;
      ullint_to_dn (&r, 1 << (i + j));
      call_user_adr (a, d, &r, &fl, &fr, &h);
      if (IS_FINITE(&h)) {
	//	dn_abs (&s1, &h);
	//      dn_add (&s, &s1, &s); // sum |h| to remove noisy cases - probably not needed with decNumbers
	if (DN_SIGN(&h) == DN_SIGN(&h2)) {
	  i += j; // search right half
	}
	else { // search left half
	  decNumberCopy (&lfl, &fl);
	  decNumberCopy (&lfr, &fr);
	  decNumberCopy (&lr, &r);
	}
      }
    } while (j > 1);
    //    if (dn_gt (&s, &const_1e_32)) { // if sum of |h| > small ...
    dn_subtract (&h, &lfl, &lfr);
    decNumberCopy (&r, &lr);
    if (!decNumberIsZero (&h)) // if last diff != 0, back up r by one step
      dn_divide (&r, &r, &const_2);
    if ( dn_lt (dn_abs(&lfl,&lfl), dn_abs(&lfr,&lfr)) )
      dn_divide (d, d, &r); // move d closer to the finite endpoint
    else
      dn_multiply (d, d, &r);  // move d closer to the infinite endpoint 
    //    }
  }
  //  {
  //  double dd = dn_to_db (d);
  //  print_debug_cd ('d', dd);
  // }
  return d;
}
#undef IS_FINITE
#undef IS_INFINITE
#undef DN_SIGN

/*
  decNumber* exp_sinh_opt_d (decNumber* a_d, decNumber* eps_d, decNumber* d_d) {

  double a, eps, d;
  a = dn_to_db (a_d);
  eps = dn_to_db (eps_d);
  d = dn_to_db (d_d);

  double h2 = f_user_double (a + d/2, NULL) - f_user_double (a + d*2, NULL) * 4;

  int i = 1, j = 32; // j=32 is optimal to find r

  if (isfinite(h2) && fabs(h2) > 1e-5) { // if |h2| > 2^-16
    double r, fl, fr, h, s = 0, lfl, lfr, lr = 2;
    do { // find max j such that fl and fr are finite
      j /= 2;
      r = 1 << (i + j);
      fl = f_user_double (a + d/r, NULL);
      fr = f_user_double (a + d*r, NULL) * r * r;
      h = fl - fr;
    } while (j > 1 && !isfinite(h));
    if (j > 1 && isfinite(h) && (copysign(1., h) != copysign(1., h2))) {
      lfl = fl; // last fl=f(a+d/r)
      lfr = fr; // last fr=f(a+d*r)*r*r
      do { // bisect in 4 iterations
	j /= 2;
	r = 1 << (i + j);
	fl = f_user_double (a + d/r, NULL);
	fr = f_user_double (a + d*r, NULL)*r*r;
	h = fl - fr;
	if (isfinite(h)) {
	  s += fabs(h); // sum |h| to remove noisy cases
	  if (copysign(1., h) == copysign(1., h2)) {
	    i += j; // search right half
	  }
	  else { // search left half
	    lfl = fl; // record last fl=f(a+d/r)
	    lfr = fr; // record last fl=f(a+d*r)*r*r
	    lr = r; // record last r
	  }
	}
      } while (j > 1);
      if (s > eps) { // if sum of |h| > eps
	h = lfl - lfr; // use last fl and fr before the sign change
	r = lr; // use last r before the sign change
	if (h != 0) // if last diff != 0, back up r by one step
	  r /= 2;
	if (fabs(lfl) < fabs(lfr))
	  d /= r; // move d closer to the finite endpoint
	else
	  d *= r; // move d closer to the infinite endpoint
      }
    }
  }
  db_to_dn (d, d_d);
  return d_d;
}

*/
/*
#define DM42

#include "decn.c"
#include <stdio.h>
*/

/*
double dn_to_db (decNumber* a);
decNumber* db_to_dn (double d, decNumber* a);
void call_user_function (unsigned int func_addr, decNumber* x, decNumber* y);
double f_d (unsigned int func_addr, double x_db);
void cmd_multi_ei2 (const opcode o, enum multiops mopr);
void cmd_rarg_ei2 (unsigned int arg, enum rarg op);
void cmd_ei2_with_c ();
void dbl_exp_int_abc (unsigned int func_addr, decNumber* a, decNumber* b, decNumber* c_in, decNumber* answer, decNumber* error);
decNumber* exp_sinh_opt_d (unsigned int func_addr, decNumber* a_d, decNumber* eps_d, decNumber* d_d);
*/
