/*
 * Debug function - print_debug displays two integers
 * Very simple functions that let me display the value of an integer
 * when the program runs.
 * Inclusion controlled by an #if statement in console.c
*/

char spaces[22] = "                     ";	//21 spaces.
char print_string[22];
int n_p = 0;

void moveto(int line, int x) {//These are line number and character positions
  lcd_setLine(fReg, line-1); //line 0 is the top line
  lcd_setXY(fReg, 20 + (fReg->f)->width*(x-1), fReg->y);
}

void print_debug (int i, int j) {
  if (i==0 && j == 0) {
    sys_delay (1500);
  }
  else {
    strcpy( print_string, spaces );
    sprintf ( print_string, "Debug: %4i,%4i", i, j);
    moveto (3, 1);
    lcd_print (fReg , (const char*) print_string );
    lcd_refresh();
    sys_delay (1500);
    strcpy( print_string, spaces );
    moveto (3, 1);
    lcd_print (fReg , (const char*) print_string );
    lcd_refresh();
  }
}
void print_debug_cd (char c, double x) {
  strcpy( print_string, spaces );
  sprintf ( print_string, "%c: %.6e", c, x);
  moveto (3, 1);
  lcd_print (fReg , (const char*) print_string );
  lcd_refresh();
  sys_delay (1500);
  strcpy( print_string, spaces );
  moveto (3, 1);
  lcd_print (fReg , (const char*) print_string );
  lcd_refresh();
}

