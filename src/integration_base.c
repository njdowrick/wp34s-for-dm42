#include "nd_integration.h"

double scale = 1;
unsigned int func_addr = 0;

double dn_to_db (decNumber* a) { // decNumber to double
  char p[DECNUMDIGITS+14];
  decNumberToString (a, p);
  return atof(p);
}

decNumber* db_to_dn (double d, decNumber* a) { // double to decNumber
  char p[30];
  sprintf (p, "%.15e", d);
  decNumberFromString (a, p, &Ctx);
  return a;
}

void call_user_function (decNumber* x, decNumber* y) {
  setRegister(regX_idx, x);
  setRegister(regY_idx, x);
  setRegister(regZ_idx, x);
  setRegister(regT_idx, x); // fill XYZT

  set_running_on_sst ();
  gsbgto (func_addr, 1, 0);
  while (Running) {
    ext_xeq_single ();
  }

  dot (RCL_annun, 1);

  getX (y); // return result in y
  if (decNumberIsSpecial(y)) {
    decNumberZero(y); // do not return inf or nan
  }
}

// Displays alpha message for (short) time
void delay (int ms, char* text) {
  DispMsg = text;
  State2.disp_freeze = 0;
  display ();
  sys_timer_start (0, ms);
  sys_sleep ();
  sys_timer_disable (0);
}

// Calls a user function with double argument x; returns double y
// params is ignored; needed to allow f_user_double to be part of a gsl_function struct.
double f_user_double (double x_db, void * params) {
  decNumber x, y;
  db_to_dn (x_db, &x);
  call_user_function (&x, &y);
  return dn_to_db (&y);
}

// **********************************************************************
// *********** Transform functions: infinities and u
// **********************************************************************

// **********************************************************************
/* Evaluate an integral over an infinite range using the
   transformation,
   
   integrate(f(x),-Inf,b) = integrate(f(b-(1-t)/t)/t^2,0,1)

   */

double il_transform (double t, void *params)
{
  struct il_params *p = (struct il_params *) params;
  double b = p->b;
  gsl_function * f = p->f;
  double x = b - scale * (1 - t) / t;
  double y = GSL_FN_EVAL (f, x);
  return scale * (y / t) / t;
}

// **********************************************************************
/* Evaluate an integral over an infinite range using the
   transformation

   integrate(f(x),a,Inf) = integrate(f(a+(1-t)/t)/t^2,0,1)

   */

double iu_transform (double t, void *params)
{
  struct iu_params *p = (struct iu_params *) params;
  double a = p->a;
  gsl_function * f = p->f;
  double x = a + scale * (1 - t) / t;
  double y = GSL_FN_EVAL (f, x);
  return scale * (y / t) / t;
}

/* Evaluate an integral over an infinite range using the
   transformation

   integrate(f(x),-Inf,Inf) = integrate((f((1-t)/t) + f(-(1-t)/t))/t^2,0,1)

   */

double i_transform (double t, void *params)
{
  gsl_function *f = (gsl_function *) params;
  double x = scale * (1 - t) / t;
  double y = GSL_FN_EVAL (f, x) + GSL_FN_EVAL (f, -x);
  return scale * (y / t) / t;
}

// **********************************************************************
/* u transform: change intergral from a to b of f(x)
   to the integral from -1 to 1 of f(u).
   x' = (3u - u^3)/2; x = a + (b-a)(1+x')/2
   */

double u_transform (double u, void *params)
{
  struct u_params *q = (struct u_params *) params;
  const double a = q->a;
  const double b = q->b;
  const double d = b - a;
  double p = u * u;
  
  gsl_function *f = q->f;
  double x = a + d/2 * (1 + u*(3-p)/2);
  double y = GSL_FN_EVAL (f, x);
  return (y * (3*d/4) * (1-p));
}
