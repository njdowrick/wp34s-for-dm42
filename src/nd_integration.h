// Files to be included for integration routines

#include "dmcp.h"

#include <stdio.h>   // (s)printf
#include <string.h>
#include <stdlib.h>
#include <math.h>

#if __GNUC__ >= 4 && __GNUC_MINOR__ >= 6
#define GNUC_POP_ERROR
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Warray-bounds"
#endif

#define DM42SAFE
#include "xeq.h"
#include "keys.h"
#include "storage.h"
#include "decn.h"
#include "complex.h"
#include "stats.h"
#include "display.h"
#include "consts.h"
#include "int.h"
#include "date.h"
#include "lcd.h"
#include "xrom.h"
#include "alpha.h"
#undef DM42SAFE

#include "gsl_includes/config.h"
#include "gsl_includes/gsl_math.h"
#include "gsl_includes/gsl_errno.h"
#include "gsl_includes/gsl_integration.h"

// These are declared in xeq.c; they allow access to two static functions
extern void (*ext_raw_set_pc)(unsigned int);
extern void (*ext_xeq_single)(void);

// func_addr is used to store the address of the user function
// in WP34S memory.
extern unsigned int func_addr;

// ND change to allow integrals over infinite ranges to have some idea
// of the x scale over which the integral is concentrated.
extern double scale; 

#define decNumberIsFinite(dn) (!(decNumberIsSpecial(dn))) // false for infinity and NaN

double dn_to_db (decNumber* a);  // decNumber to double

decNumber* db_to_dn (double d, decNumber* a);  // double to decNumber

void call_user_function (decNumber* x, decNumber* y); // calls WP34S at func_addr

double f_user_double (double x_db, void * params); // calls WP34S with double

struct u_params {double a; double b; gsl_function * f;};

struct iu_params { double a ; gsl_function * f ; } ;

struct il_params { double b ; gsl_function * f ; } ;

double il_transform (double t, void *params);

double iu_transform (double t, void *params);

double i_transform (double t, void *params);

double u_transform (double u, void *params);

void delay (int ms, char* text);
