## How the WP34S functions are laid out on the DM42 keyboard

The unshifted keys all do exactly what they say. 

The picture below
shows the shifted functions of each key. <img src="WP34S_for_DM42_layout.png" width="500" style="float:right">

In the picture:

  * The **white** text on each key shows **what is printed** in yellow above that key on the DM42 faceplate.  (The unshifted key functions are *not* shown as they are identical in both the DM42 and WP34S programs.)
  * The **yellow** text above a key shows the **f-shifted function** of that
    key in the WP34S program. Often this is the same as what is printed on the faceplate,
    or at least related to it. 
  * The **blue** text above a key shows the **g-shifted function** of that key
    in the WP34S program. Again, this is often related to the printed text.
  * **Solid** underlining indicates a **menu**; **dashed** underlining indicates
    a WP34S **catalogue** (as on the original calculator).

Note how, in nearly every case, the f-shifted function of a key is either the same as or closely related to the f-shifted function of that key as printed on
the DM42 faceplate. The g-shifted functions are intended to be closely
related to the f-shifted functions. In this way the normal DM42
faceplate acts as a guide to the WP34S keyboard.

Also:

  * The red text shows three shift-hold functions (previous menu, the
    MISC menu, and screenshot).

* The two "f-alpha"s (yellow) and three "h-alpha"s (green) to the lower right of five
  keys indicate alpha catalogues. They are positioned as on the original WP34S keyboard. (In alpha-mode there is an h-shift; see the [`Using.md`](./Using.md) file for more details of alpha mode.)

* Complex lock mode is entered by pressing `F5` followed by `F6` (again, more details of this in [`Using.md`](./Using.md)).
