This file explains how to use the WP34S program for the DM42 hardware. For those with some knowledge of the original calculator, differences and additions are clearly described.

[TOC]
<div style="page-break-after: always;"></div>

# Using the calculator

How the keyboard is arranged:

- All unshifted keys on the DM42 keyboard do
  exactly what they say.

- This is also true of the yellow-shifted keys on
  the two rows below the menu keys (except for `COMPLEX` &mdash; see later),
  along with `ALPHA`, `LAST x`, `BST`, `SST`, and `OFF`. For
  example, pressing the yellow shift key followed by the `SIN` key carries
  out the `ASIN` function.

- There are two shift states &mdash; press the yellow key once for f-shift,
  twice for g-shift, and once more to cancel the shift state.

- The original WP34S keyboard functions have been collected into groups and
  made available on the DM42 as _menus_. A menu makes a group of related functions
  available using the DM42 menu keys. More about menus below.

- Most menus appear on f-shifted keys. The menu attached to
  each key is related to the shifted function of that key as printed
  on the DM42 keyboard. For example, `f`&nbsp;`PROB` brings up the
  Probability menu; `f`&nbsp;`PRINT` brings up the Printing menu.

- The WP34S keyboard has lists of operations called _catalogues_ (see below). In this program catalogues nearly all appear on g-shifted keys, and once 
  again they are placed so that the keyboard text gives a clue to which catalogue is located there. For
  example, `g`&nbsp;`MODES` brings up the Modes catalogue; `g`&nbsp;`STAT`
  brings up the Statistics catalogue.

Remembering these rules makes accessing the large function set of the WP34S on the unmodified DM42 keyboard a
lot easier that you might expect ... so long as you are familiar with the original calculator! If you aren't, you can have fun exploring while still getting basic calculations done, but there is no substitute for working through a copy of the original documentation (links in the main [`README.md`](../README.md) file). The differences between this program and the original calculator are mostly rather small, so the original documentation remains useful.

Here are some more details of keyboard operation:

- The WP34S keyboard has <span><code>&rarr;</code></span> and `CPX` keys. Nearly every menu contains these keys as unshifted `F5` and `F6`. Pressing `f`&nbsp;`COMPLEX` (shifted `STO`) also acts as `CPX`, except in complex lock mode where it brings up the COMPLEX menu.

- As on the real calculator, the first four keys of the top row
  execute `XEQ`&nbsp;`A`, `XEQ`&nbsp;`B`, `XEQ`&nbsp;`C`, 
  and `XEQ`&nbsp;`D` when pressed unshifted, if these local labels (A-D) are defined in the
  current program. To access a key's normal unshifted function when a label is defined, use
  g-shift.
  - For example, if `LBL`&nbsp;`D` is defined, pressing `LOG` does `XEQ`&nbsp;`D`; `f`&nbsp;`LOG`
    does `10^x`; `g`&nbsp;`LOG` (always) does `LOG`. 
  
- `f`&nbsp;`SHOW` shows all 16 digits of a single-precision number. To see 
  a number in full double precision, use the Show commands in the Exchange menu (see below).

- `f`&nbsp;`PRGM` enters and leaves Program mode; `g`&nbsp;`PRGM` brings up the program
  menu, which includes useful programming-related commands.

- Holding down the shift key and pressing `DISP` takes a screenshot.

## Menus and Catalogues

In these lists, the text following the shift key refers to the yellow text on the DM42 faceplate above the key to be pressed.

Nearly all of the operations in these menus and catalogues exist on the original WP34S and are described in the original WP34S documentation. This document concentrates on the commands and functions not present on the original calculator.

Between them, the menus include every operation present on the original calculator keyboard (and some new ones). However, the `BASE` menu (for example) doesn't include _every_ base-related operation; many more can be found in the catalogues.

## Menus

Menus use the top row of function keys, unshifted and f-shifted, to
provide the full set of WP34S keyboard functions along with some
additions.

- Almost every menu has  <span><code>&rarr;</code></span> and
`CPX`  on unshifted `F5` and `F6`.

- Calling the same menu twice brings up the *default menu*. The default menu can also be reached by pressing `f`&nbsp;`ASSIGN`.

- Initially the default menu displays only <span><code>&rarr;</code></span> and `CPX` keys.

- Pressing `f`&nbsp;`ASSIGN` when the default menu is
  displayed toggles between this version and one that includes
  Σ+ , 1/x , √ , and LOG ,
  which is helpful when labels A-D are assigned to these keys.

- Pressing
  SHIFT-hold-▲
  goes to the previous menu.

Most menus are on f-shifted keys; a few are on g-shifted keys.
See the sections on [Alpha mode](#alpha-mode) and [Complex Lock
mode](#complex-lock-mode) for menus in these modes.

### Menus on f-shifted keys
<table>
<thead>
</thead>
<tbody>
<tr><td>COMPLEX menu</td><td> f
    COMPLEX</td><td>Functions useful in <a href="#complex-lock-mode">complex lock mode</a>; only available in that mode.
</td>
</tr>
<tr><td>ALPHA menu</td><td> f
    ALPHA</td><td>Functions useful in <a href="#alpha-mode">alpha mode</a>; automatically displayed when alpha mode is entered. x&rarr;&alpha;; &alpha;&rarr;x; &alpha;IP; &alpha;LENG; &alpha;TIME; &alpha;DATE; &alpha;DAY; &alpha;MONTH.
</td>
</tr>
<tr>
<td>ANGLES menu</td>
<td>f
    MODES</td>
    <td>Angle modes, H.MS / H.d,
    co-ordinate conversions and hyperbolic prefixes. This menu is also
    available from g&nbsp;SIN, g&nbsp;COS , g&nbsp;
TAN .
</td>
</tr>
  <tr><td>DISPLAY menu</td><td> f
    DISP</td><td>Display modes, fraction modes,
    radix choice, and H.MS / H.d.
</td>
</tr>
  <tr><td>CLEAR menu</td><td> f
    CLEAR</td><td>Clear program, clear stats,
    clear x, etc.
</td>
</tr>
  <tr><td>INTEGRATE menu</td><td> f
    ∫ f(x)</td><td>SLV, ∫ dx, ∫ old, ∫ QAG, TOG_A;
    SUM, PROD, f'(x), f''(x).
</td>
</tr>
  <tr><td>STATS menu</td><td> f
    STATS</td><td>Xbar, Yhat, s (variance) and r
    (correlation coefficient), a link to the SUMS catalogue, etc.
</td>
</tr>
  <tr><td>BASE menu</td><td> f
    BASE</td><td>BIN, OCT, DEC, HEX, not, and, or,
    xor, and RMDR.
</td>
</tr>
  <tr><td>FLAGS menu</td><td> f
    FLAGS</td><td>SF, CF, flag tests, comparison
    functions.
</td>
</tr>
  <tr><td>PROBABILITY menu</td><td> f
    PROB</td><td>Factorial, permutations,
    combinations, random numbers, the normal cdf and its inverse.
</td>
</tr>
  <tr><td>SETUP 1 menu</td><td> f
    SETUP</td><td>Exit to the DMCP menu, System,
    Help, OnSTO and OnRCL, save state to and load state from named file.
</td>
</tr>
  <tr><td>MISC menu</td><td>
    SHIFT-hold-▼
    </td>
    <td>
    Functions that did not fit anywhere else! |x|, RND, IPart, FPart,
    2<sup>x</sup>, Log<sub>2</sub> x, llel, delta percent.
</td>
</tr>
  <tr><td>CUSTOM menu</td><td> f
    CUSTOM</td><td>Displays the current user menu.
</td>
</tr>
  <tr><td>PRINT menu</td><td> f
    PRINT</td><td>A range of printing functions.
</td>
</tr>
</tbody>
</table>


### Menus on g-shifted keys

<table>
<thead>
</thead>
<tbody>
  <tr><td>EXCHANGE menu</td><td>
  g&nbsp;x<->y</td>
  <td>Show&larr;, Show&rarr;, and various exchange functions.
</td>
</tr>
  <tr><td>SELECT USER MENU menu</td><td> g&nbsp;CUSTOM</td><td>Functions for choosing a
    <a href="#user-defined-menus">user-defined menu</a>.
</td>
</tr>
  <tr><td>PROGRAM FUNCTION menu</td><td>g&nbsp;PGM.FCN</td><td>LBL, RTN, Pause, GTO, DSE, ISG.
</td>
</tr>
  <tr><td>SETUP 2 menu</td><td> g SETUP</td><td>Save library to / load library from
    named file; save program to / load program from named file.
</td>
</tr>
</tbody>
</table>

## Catalogues

The WP34S makes most of its functions available via catalogues, each of which is a long list of related functions. This is
where to find the catalogues on the DM42 keyboard.

### f-shifted catalogues:

|        |     |           |                  |
| ------ | --- | --------- | ---------------- |
| MATRIX catalogue |     | f MATRIX  | Matrix functions. |
| CONV catalogue   |     | f CONVERT | Unit conversions. |
| P.FCN catalogue  |     | f PGM.FCN | Programming functions. |
| CAT catalogue    |     | f CATALOG | Browse global labels.  |

### g-shifted catalogues:

|        |     |           |                  |
| ------ | --- | --------- | ---------------- |
| MODE catalogue |     | g MODES   | A very large number of commands to set various modes &mdash; e.g., double precision, Y-register display, and many more. |
| SUMS catalogue |     | g MATRIX  | Sums such as Σx<sup>2</sup>, Σy<sup>2</sup>, Σxy, ... | 
| STAT catalogue   |     | g STAT    | Various statistical functions &mdash; see manual. |
| CONST catalogue  |     | g&nbsp;CONVERT | List of physical constants. |
| TEST catalogue   |     | g FLAGS   | Lots of tests such as FS?, INTM?, ODD?, ...
| PROB catalogue   |     | g PROB    | Probability distributions &mdash; mass, cumulative, inverse.
| X.FCN catalogue  |     | g&nbsp;PGM.FCN | A great variety of eXtra functions &mdash; the functions change with mode.
| SHOW catalogue   |     | g EXIT    | A register browser.
| STATUS catalogue |     | g CATALOG | Shows memory status and user flags.


## Alpha Mode

Alpha mode on the WP34S is a complex beast, with some keys having as
many as six functions! This program remaps alpha mode to fit in with the letters printed on the DM42 faceplate, but apart from the changes implied by this remapping (some of which are rather subtle) it is my intention that alpha mode in this program should work in the same way as alpha mode on the WP34S. This means that the original WP34S documentation remains a useful guide to alpha mode.

To activate alpha mode, press `f`&nbsp;`ALPHA` (the `ENTER` key). This enters alpha mode and displays the ALPHA menu.

- Unshifted keys produce the character to the right of the key on the DM42 keyboard. For example, `SIN` produces "J", and the decimal point produces a comma. Keys with no letters next to them generally do nothing, with the exception of `ENTER` and `EXIT`, both of which leave alpha mode.

- In general, if there's a character printed on the key then f-shift produces it. For example, `f`&nbsp;`8` produces "8". I have extended this a little so that, for example, `f`&nbsp;`1/x` produces&nbsp;&nbsp;<sup>-1</sup>. Also note that `f`&nbsp;<span><code>R&darr;</code></span> produces &pi;, and `f`&nbsp;<span><code>&larr;</code></span> clears the alpha register.

- g-shifted keys generally produce capital Greek letters (many of which are the same as the Roman equivalents).

- There is h-shift in alpha mode, reached by pressing the shift key three times. There's a mixture of stuff here &mdash; various symbols and some catalogues.

- To get to lowercase mode, press `f` `EXIT`; a down-arrow annunciator appears. Once this is done each letter key produces lowercase letters. There are a couple of other changes &mdash; colon produces a semicolon, and comma produces a full stop.

- In lowercase mode, g-shifted keys produce lowercase Greek characters. 

- In lowercase mode, f-shifted keys and h-shifted keys produce the _same_ character as in uppercase mode. So `f`&nbsp;`8` produces "8" in both upper- and lowercase modes. 

Alpha mode can be exited by pressing `f`&nbsp;`ALPHA`, `ENTER`, or `EXIT`.

This table shows the functions of every key of the WP34S alpha keyboard on the DM42, row by row. Each row of the table shows the characters that a key produces in each of the six possible states.

Key combinations shown as blank do nothing.

<img src="./wp34s_alpha_keyboard.png" width="65%"></img>
<p></p>

The key combinations in shaded boxes don't enter characters, but have other functions.

- `ENTER` and `EXIT` leave alpha mode.

- <span><code>&larr;</code></span> backspaces and deletes a character.

- <span><code>&alpha;STO</code></span> and <span><code>&alpha;RCL</code></span> store and recall up to six alpha characters in registers; <span><code>VW&alpha;+</code></span> is a command that stops a program to display text. See the WP34S manual for details of these and other alpha commands.

- `f` `EXIT` enters and leaves lowercase mode.

- `f` `R/S` runs a program (!). (This is present on the original calculator.)

- The remaining boxes are WP34S catalogues &mdash; five containg further characters, and two with functions and commands. The X.FN catalogue in particular has all of the alpha-related commands in it. The catalogues containing characters are positioned as on the original calculator, and you can read about them in its documentation. If you can't find a symbol on the keyboard you should find it in one of the alpha catalogues, if it exists!

  - The catalogue on `f` `LN` contains arrows and maths symbols.
  - The catalogue on `f` `XEQ` contains non-ASCII alphabetic characters (accents, etc.).
  - The catalogue on `h` <span><code>R&darr;</code></span> contains sub- and superscripts,
  - The catalogue on `h` `1` contains relation symbols and delimiters.
  - The catalogue on `h` `.` contains punctuation, currency, and other symbols.

The purpose of alpha mode is to let programs provide prompts for inputs and labels for results. On the "top" version of this program, these displays will by default appear on the (shorter) top line of the display. If you wish to use the longer middle line for your prompts and labels you must turn off the Y-register display (`YDOFF` in the `MODES` catalogue).

<div style="page-break-after: always;"></div>

# How this program differs from the original calculator

My main aim when writing this program was to reproduce the original calculator as closely as possible. For this reason, the differences between the two are rather small. A very few features of the original are missing; other things are just different (for example, the keyboard layout); and there are improvements and added features that take advantage of the DM42 hardware.

The keyboard and alpha mode are discussed above.

## Display

As described in the [README.md](../README.md) file there are three versions of the program that differ only in their displays. Here are three images, showing each version with every display element turned on (the bottom two black bars are the menu item pixels):

<img src="./screens/all_dots_normal.bmp" width="40%"></img> 
<img src="./screens/all_dots_long.bmp" width="40%"></img>

<img src="./screens/all_dots_top.bmp" align="left" width="40%"></img>

The display elements in the "normal" version (image 1) are identical to those on the original calculator, except for the addition of the menu text. The "long" and "top" versions (images 2 and 3) have a longer dot matrix display and the "top" has an extra dot matrix display line at the top, used for annunciators such as `f` and `g`. This leaves the line below free to display the Y-register contents or prompt messages.

In all three versions the annunciators differ from those on the original calculator; details [here](./wp34s_help.md#annunciators).

If you want program prompts to appear in the longer alpha register of the "top" version, you or your program must turn off the Y-register display (`YDOFF` in the `MODES` catalogue) first.

## Extra functions

A few extra functions have been included from the original function set of the WP34S. Memory pressure in the original machine meant that not all functions were included by default; here, nearly all functions are present.

- In double precision mode, large exponents (from -6143 to +6144) can be entered and displayed. So if you press `5` `E` `2` `3` `4` `5` you will see 5 2<sup>345</sup> in the display.

- "Significant figure" display mode is available. In the DISP menu, press `FIX` and then either <span><code>&uarr;</code></span> for `SF` or <span><code>&darr;</code></span> for `SF0`. `SF` followed by a number _n_ displays a number with (_n_+1) significant digits of precision, without forcing scientific notation. `SF0` followed by _n_ does the same thing but includes trailing zeros.

- The digamma function &psi;(x) is included; you should find it in the X.FN catalogue (`g` `PGM.FCN`).

- A function to give the date of Easter Sunday (in Western churches) is included, again in the X.FN catalogue. With a year in the X-register, executing `EASTER` gives the date of Easter Sunday in that year.

- There are now several integration algorithms to choose between.. More details are given [later](#integration-routines).

- Complex lock mode is included &mdash; see a [later section](#complex-lock-mode) for details of this.

## Improvements

- The code that converts decimals to fractions has been improved, so that for a given value of DENMAX the fraction returned genuinely is the closest approximation to the decimal. This improved code is also now available in the WP34S calculator firmware. For example, having set `DENMAX` to 100 in the MODES catalogue, entering <span><code>&pi;</code></span>  and then `d/c` (in the `DISP` menu) now gives 311/99 instead of 22/7.

- When entering numbers in an integer mode you are no longer limited to 12 digits. Instead you can enter digits until you exceed the word size setting. This improvement is also now available in the WP34S calculator firmware.

- Various minor bug fixes (also ported back to WP34S calculator).

- The double exponential integration routine now includes the latest improvements (not ported back to the WP34S calculator due to lack of space there).

- On the WP34S, range reduction for RAD mode is limited to simply taking mod (2&pi;) of the argument. This saves space that would be required for high-precision constants but slows evaluation of trig functions significantly as the Taylor series takes longer to converge. Space isn't an issue on the DM42 hardware, so range reduction is now carried out for RAD mode in the same way as for DEG and GRAD modes.

## Missing functions

Some functions have been removed. The only ones that I am aware of removing deliberately are connected with serial operation &mdash; Print mode 3, along with commands such as `SOPEN`, `SCLOSE`, etc. However, as these weren't included in the standard build you won't miss them unless you installed the hardware needed for serial communication.

## Program library

The original WP34S came with a selection of programs already loaded into its library memory. This one doesn't. However, a library file containing these programs is available if you wish to use it. This is described [later](#wp34s-programs).

## Minor changes

- You can enter the fraction "a/b" by pressing `a`&nbsp;`.`&nbsp;`.`&nbsp;`b`, as on the HP-32SII (for example).

- Pressing `E` (the exponent key) or entering a decimal number exits fraction mode.

- Stack size is shown as `S:4` or `S:8` in place of the `BEG` annunciator.

# Bigger changes

Let's start with some features which were available on the original calculator if you installed some extra hardware and/or a non-standard firmware version. As such, these features will be new to many of you!

## IR Printing

This program supports infra-red printing to the HP&nbsp;82240B printer. I
haven't tested it on an&nbsp;HP 82240A printer, but so far as I can tell none
of the special features of the HP&nbsp;82240B printer are used, so (certainly
in modes 1 and 2) I would expect it to work.

All of the printing commands described in the WP34S documentation are
supported (except for Print mode 3). These commands are in the P.FCN and X.FCN catalogues; a
number of the most-used commands are available from the PRINT menu,
`f`&nbsp;`PRINT`.

Printing with the original calculator is described in the manual. There are some differences between what is described there and this implementation:

-   Printing can be enabled and disabled with the `P.ON` and `P.OFF`
    commands (here, `P.` represents the printer icon), found in the `MODE`
    catalogue and in the `PRINT` menu. 

-   When printing is enabled, the `PRT` annunciator appears in the display.

-   The printing of a multi-line listing can be interrupted with the `EXIT` key.

-   You can have double-line-spaced printing with the `P.DBSP` command:
    an argument of 1 turns this on, while an argument of 0 turns it
    off. 

-   Print mode 0 uses the printer character set where possible (this
    may not work reliably with the older printer, as the character
    sets differ). Print modes 1 and 2 use the printer's graphics mode
    to produce the WP34S character set &mdash
    mode 1 is normal width, mode 2 is compressed. Print mode 3 (serial
    port) is not supported. 

-   The DM42 hardware sets a default delay time of 1.8 seconds, which
    works fine with my (battery-powered) printer. You can change this
    with the `P.DLAY` command. 

-   Printing the stack or a block of registers in complex lock mode
    now prints in complex form by default, respecting the `RECT` or
    `POLAR` display setting.

-   Outside of complex lock mode a similar effect can be achieved by
    pressing `CPX` before either the stack or register printing
    commands.

## The Stopwatch

To use the WP34S stopwatch, execute the `STOPW` command in the `X.FCN` menu (`g`&nbsp;`PGM.FCN`). Its features are described in the WP34S manual.

(For added convenience, you may wish to add this command to a
user-defined menu.)

I believe that all of the stopwatch features described in the WP34S manual
work on the DM42, with the exception of the flashing "=" sign: I
couldn't find a way to do this without an unacceptable increase in
power consumption (when a timer is running, there's no power reduction
when sleeping between keypresses).

Note that the stopwatch continues to "run" even if the DM42 is
turned off. This doesn't drain the battery &mdash; the stopwatch works by
recording the tick count from the real-time clock when it starts, and
then subtracting this from the new tick count whenever a time has to be
displayed. When a time isn't displayed, nothing special is
happening. 

If the stopwatch is running in the display, auto-power-off does not
occur. If the stopwatch is running in the background the calculator
turns off in the usual way. This doesn't affect the operation of the 
stopwatch as the DM42 real-time clock continues to run.

## Entry RPN (eRPN)

Normally, on an RPN calculator, pressing `2`&nbsp;`ENTER` would lead to "2" being in both the X-register and the Y-register, and stack lift would be disabled.
But on an "Entry RPN" calculator, pressing `2`&nbsp;`ENTER` leaves "2" in the X-register only, and stack
lift enabled. This is similar to what happens with number entry on an RPL calculator
(e.g., the HP-48 series). (C47 users will already be familiar with this!)

What difference does this make? On the plus side, the number in the
T-register isn't pushed off the top of the stack until absolutely
necessary. However, squaring a number by pressing
"ENTER" "×" no longer works.

If you like eRPN (and it is a logical system) you can enable it by going into the MODE menu (`g`&nbsp;`MODES`)
and selecting `eRPon`. Having tried it you can turn it off again with `eRPoff`. When eRPN is enabled, the RPN annunciator changes to "eRPN".

Thanks to Jaco Mostert (Jaymos) of C47 fame for this, both the idea and the code that makes it happen.

## Complex Lock Mode

This was also available on the original calculator, but you needed to
load a special version of the firmware. Even without this mode the
calculator supports complex numbers supremely well. All that
complex lock mode does is to make them easier to enter and to display, and
to make all appropriate functions complex by default.

If you rarely use complex numbers or you feel that the WP34S is fine as it is, complex lock mode won't get in your way. It needs to be
enabled before it can be turned on and you are unlikely to do this by
accident.

A great summary of Complex Lock Mode is the
[Complex_Lock_quick_reference_guide.pdf](./Complex_Lock_quick_reference_guide.pdf) file; a longer description
can be found in the `Complex_Lock_Mode_Details` file ([pdf](./Complex_Lock_Mode_Details.pdf) or [markdown](Complex_Lock_Mode_Details.md)).

## WP34S Programs

The original WP34S calculator came with a collection of programs,
written by the developers and users, already loaded into its library
memory. There was (and still is) a bigger collection of programs on
the [WP34S Sourceforge
website](https://sourceforge.net/p/wp34s/code/HEAD/tree/library/).

Some of these programs are generally (and genuinely!) useful (e.g.,
triangle solution, finance calculations). Others expand functions that
the calculator already has (matrix operations) or provide insight into
how some of the calculator functions work (the solver code).

These programs are now available as part of this WP34S
build. Their source code can be found as `.wp34s` files in the folder
`wp34s_programs/`. In this folder the file
[info.md](../wp34s_programs/info.md) gives brief information about
each program and how to use it. In some cases the program's source
code is well documented and relevant extracts are included in the
`info.md` file; in other cases I've had to guess how the program
should be used and of course I may be wrong.

Files for loading into the calculator can be found in the folder
`wp34s_programs/DAT/` as `.dat` files. Each of these contains one program. To load these, first copy the
.dat files you are interested in to the folder `/wp34s/` on your DM42
(or a subfolder of this). Then use the `LdPRG` command on the `SETUP 2`
menu (`g` `SETUP`) to load them into library memory.

The `wp34s_programs/DAT/` folder also contains a file called `progs34s.lib`, which
contains many of the programs that the real WP34S came loaded
with. (There's a list in the second paragraph of [info.md](../wp34s_programs/info.md).)

To load this file:

- First, save any programs currently in your library memory that you may wish to reload. Use `SvCPRG` in `SETUP 2` to do this.

- You may also wish to to save a
copy of your current library memory using `SvLIB` in `SETUP 2`.

- Now go into the DMCP menu (`SETUP 1` then `EXIT`) and choose Option 6 (Activate USB Drive). 
Copy `wp34s_programs/DAT/progs34s.lib` into the `wp34s/` folder on your DM42. Unmount the calculator from the computer and use Option 2 (Run Program) to restart the WP34S calculator program.

- Use `LdLIB` in `SETUP 2` menu to load `progs34s.lib`. The programs in it take up 1799 of the 8190 steps available, so there's plenty of room for your own programs.

- If you wish you can reload the programs you saved earlier using `LdPRG`. 

- Or you can use `PrvLIB` in `SETUP 2` to swap back and forth between the new and old libraries as the fancy takes you.

Most people won't end up using many of the programs on a regular
basis, but you should find some that you like. Personally, I like the
triangle solver and the time value of money programs &mdash; they are both
useful, very clearly written, well documented, and are great examples of
what this calculator can do. You too now have the chance to
experience history first-hand.

## User-defined Menus

This is a genuinely new feature &mdash; the original WP34S didn't have any menu keys!

The WP34S on the DM42 makes heavy use of menus to deliver the
huge range of keyboard functions available on the original calculator.
Each menu has up to twelve functions (in practice, keys `F5` and `F6` are
nearly always bound to `→` and `CPX`). This system is a virtue as well
as a necessity: the program menu, for example, brings together lots of
useful program-related commands.

Naturally, many commands remain buried away in catalogues. It isn't
practical to put every command into a menu, but a user-defined menu
could make some frequently-used commands easier to access, or provide an interface to a program.

The WP34S now offers a way to produce such menus. The idea and much of the code is based
on a feature of the WP34S code that, so far as I know, never saw the
light of day &mdash; a user-defined catalogue. This generated a catalogue
from the commands in a program, that had to be called `CAT`. The present
code generates a menu rather than a catalogue, and the program used need
not be called `CAT`!

### What you can do

You can have your own menus with up to 12 entries each. Menu entries can be catalogue items (e.g., `LNBETA` or `STOPW`), or things like `STO 05` or `XEQ'CAT'`. You can enter your own labels or have the calculator generate them automatically. The automatically-generated labels may be too long; the calculator attempts to shorten them &mdash; e.g., `XEQ'CAT'` would appear as `XQ'CAT` &mdash; but simply labelling this as `CAT` might be best. (Or `DOG`, if you prefer.)

### How to do it

Simply write a program starting with a global label, with each step
being a function that you want to appear in your menu. This might be
something like

```
  LBL'FUN'
  CPXI
  CPXJ
  SSIZE4
  SSIZE8
  STOPW
  NOP
  END
```

Don't run the program! Instead, press `g`&nbsp;`CUSTOM` and select `UMEN` on `F1`. This invites you to enter a
program name, so press `F`&nbsp;`U`&nbsp;`N`. This processes the program and creates and displays the user menu. If there were more than six items in the program, there would be shifted functions too.

If you leave the user menu, you can return to it by pressing `f`&nbsp;`CUSTOM`. Note that the user menu is _not_ part of the calculator state, so if you `EXIT` and reload the WP34S program you'll have to use `UMEN` once more to rebuild the menu.

What about `NOP`? `NOP` is a command in the P.FCN menu; it means "do
nothing". If you have a NOP in the `F5` or `F6` position in your program you get either `→ ` or `CPX` inserted automatically. You can also use `NOP` to leave blanks in your layout, if you don't want things filled simply from left to right, bottom to top. See [Rules for NOPs](#rules-for-nops) below.

In the `g`&nbsp;`CUSTOM` menu, as well as `UMEN` there are `UM1`,...,`UM6`. (The commands `RAM` and `LIB` are covered in a section below.) These keys run `UMEN'UM1'`, etc., giving you quick access to six user menus defined in programs with these names.

Note that these user-menu programs can be in user memory or in library memory, so they needn't get in the way of normal calculator use.

Either `END` or `RTN` ends the user-menu program. Using `RTN` is useful when a program that you write defines and displays its own menus. Ending the user-menu programs with RTN and only having an END at the end of the entire program set means that the main program and its user-menu programs are all in the same program area, and so can be saved and loaded as a unit. 

### Functions with arguments

Many functions have an argument that is part of the function name,
rather than being taken from the stack. Examples are `STO 17`, `SF 7`,
`XEQ'CAT'`. You can enter these into your program just as they are, and
they will appear in your menu.

You may wish to have "`SF`" (set flag) in your menu as a command with no
argument. If you have zero as the argument (`STO 00`, `SF 0`) the
argument is ignored and you can type in your own. This does mean that
you can't have `SF 0` (etc.) in a user menu, but...well. This doesn't work for `STO`, `RCL`, and `XEQ` since these are on the keyboard already and you might reasonably wish to have `STO 00` in a menu.

### Providing your own labels

In the example above, suppose that we wanted to call STOPW "Timer"
instead. To do this:

```
  LBL'FUN'
  CPXI
  CPXJ
  SSIZE4
  SSIZE8
  α T
  α i
  α m
  α e
  α r
  STOPW
  NOP
  END
```

To add the lines starting with "α" to the program:

-   If you haven't done so, go into program mode and enter everything up to `SSIZE8`. (The four commands are all in the MODES menu, `g` `MODES`.)

-   Still in program mode, press `f`&nbsp;`ALPHA` once to enter alpha mode, and then press `T`&nbsp;`f EXIT`&nbsp;`i`&nbsp;`m` `e` `r`.(The `f`&nbsp;`EXIT` leads to lower-case alpha mode.) Then press `ENTER` to leave alpha mode.

-   Enter the `STOPW` and `NOP` commands if necessary.

-   Press `EXIT` to leave program mode, then run `g`&nbsp;`CUSTOM`, `UMEN` again to rebuild the user menu.

The five alpha characters take up five words of program memory. The WP34S also offers alpha group mode, which packs three characters into two words. You can read
about it in the WP34S manual if you aren't already familiar with it.

The rules for labels in a user menu are:

-   At most six characters can appear in a label. If more than six are provided, only the first six are used; the rest are ignored. With 6 characters, the last character doesn't always fit!
-   If one of the characters is a space you can use `h`&nbsp;`+`
    to enter a thin space character. (This
    character doesn't seem to appear elsewhere on the keyboard or in the alpha catalogues, for some reason.)

### Rules for NOPs

- In menu positions 5 and 6, a NOP without a preceding label causes either  <span><code>&rarr;</code></span> or `CPX`  to be added to that key. If these is a label, that label is used and the key is assigned to NOP. So using a space as a label will leave that menu entry completely blank.
- NOPs in other menu positions work in the same way, except that if no label is provided then "NOP" is displayed.
- As mentioned below, a NOP immediately after the global label at the start of a menu means that the label will not be displayed in the RAM and LIB menus.

## RAM and LIB Menu generator

The existing CATALOG function on the WP34S (`f`&nbsp;`CATALOG` on this DM42
version) lets you scroll through programs stored in user memory (RAM), in library memory
(LIB), and in the backup memory (BUP). Each program can be
saved (to library memory), loaded (to user memory), deleted, or executed.

I've now added a menu-based version of this idea. It generates a menu of defined global labels (such as `LBL'DOG'` or `LBL'123'`, rather than
local labels such as `LBL A` or `LBL 01`); pressing each menu key executes
the code starting at that label. It doesn't allow loading, saving, or
deletion of programs, but it does make finding and executing a
particular program a lot quicker! (After all, most programs are executed
far more often than they are loaded, saved, or deleted.)

To get a listing of the global labels in user memory or library memory, press `g`&nbsp;`CUSTOM`, followed by `RAM` (F5) or `LIB` (F6). The calculator starts
at the beginning of the memory region and looks for global labels. Each that it finds &mdash; for example, `LBL'A1'` &mdash; is used to label a menu key
(`A1`), and binds that key to a command to execute that label (`XEQ'A1'`).
This continues until the end of memory is reached, or twelve labels have
been found.

### Things to know

When one of these menus is displayed, the ▼ (down-arrow key) moves to the
next twelve labels, if they exist. Pressing ▲ goes back to the start of
memory and displays the first twelve labels again.

You may not want a particular global label to appear in the menu. For
example, the list of commands for a user-defined menu starts with a
global label but it wouldn't make sense to execute it! If the first
command following the global label is `NOP` (no operation &mdash; it's in the
P.FCN catalogue) then the label is skipped. (You can insert `NOP` at the
start of a user-defined menu without causing trouble. If you **want** a
`NOP` at the start of a user-defined menu, enter it twice.)

If you go to another menu, you can use `SHIFT-hold-uparrow` to go back to
the `RAM` or `LIB` menu in the usual way.

If you want to include RAM or LIB in one of your own programs or as part
of a user-defined menu, you can use these commands:

-   `RAM` is the command `UMEN'RAM'`

-   `LIB` is the command `UMEN'LIB'`

-   `down-arrow` is the command `UMEN'NXT'`

-   `up-arrow` is the command `UMEN'TOP'`.

This does mean that you cannot have a user-defined menu named RAM, LIB, NXT, or TOP.

## User Help files

It isn't possible to display much text on the WP34S screen, and typing
on the DM42 keyboard isn't straightforward either. The WP34S allows the
creation of substantial programs that may require documentation,
especially if they aren't regularly used.

There is now (certainly in DMCP 3.24) an option to open help files other
than the main WP34S help file. To do this, first open the main help file
(`HELP` in the `SETUP 1` menu) and then press `F3`. Choose the file you want
to read from the file list and there you are. `EXIT` returns you to the
calculator, not to the previous help file.

Note that the next time you press `HELP` it is the new help file that will
be displayed. If this is not what you want, use `F3` to return to the
original (or any other) help file.

Preparing your own help file is straightforward:

-   Use a text editor to avoid hidden control characters. The minimum
    HTML that seems to be needed is `<body>...</body>` tags at the 
    start and end of the file, and
    `<br>` to start a new line. The DM42 browser only understands basic
    HTML.

-   For example:
```
  <body>
  This is a very short help file.
  <br>
  To use my program, put x and y in the X and Y registers, and run the
  program.
  <br>
  The answer is returned in units of furlongs per lunar month.
  </body>
```
-   You must break up text with `<br>` &mdash; I don't know the exact limit, but the
    browser seems to choke on more than a few hundred characters without a break.
    
-   The name of your file can be anything you like. Use `.htm` or
    `.html` as the file name extension.

-   File names do not seem to be case-sensitive.

-   Put your file in the `HELP/` folder on the DM42. (Naturally, keep the
    original safe on another device!)

-   Now, start your calculator program, open `HELP` and press `F3`; the name of your file should appear in the list. (This should work whichever calculator program you are using.)

In previous versions of WP34S for DM42 there was a command `αHELP`
(found in the `P.FCN` catalogue) which allowed a program to open a
user-provided help file in the DM42 html browser, either from the
keyboard or from a program. This command used a rather contorted
series of file renaming operations to cope with the fact that once one
help file has been opened, it isn't possible to open a new one with a
different name.

Given the `F3` option, the command `αHELP` still exists but it no longer attempts to open a
particular help file. Instead, it displays the alpha register contents and invites the user to choose the file by
pressing `F3` as described above.

Operation is straightforward. Put your filename in the alpha register;
if it doesn't have an extension, `.html` is automatically added. `αHELP`
then displays a message as described above, and opens `HELP`. To make this
as straightforward as possible in a program, you might choose to limit
your filenames to three characters and to use `.html` as the extension.
(Remember that you can only add alpha characters to the alpha register
in groups of three!)

## Integration Routines

The WP34S calculator uses the Double Exponential method to evaluate numerical integrals. This is a relatively modern integration technique. It is adaptive (i.e., it takes samples at more points until the requested precision is reached) and, for quite a wide class of functions, performs well compared with other more traditional techniques. 

A [discussion](https://www.hpmuseum.org/forum/thread-16549.html) on the HP forum some time ago alerted me to an updated version of the Double Exponential method. I considered replacing the existing routine on the WP34S hardware, but I found writing XROM code on this scale to be beyond me. However, writing the new routine in C is straightforward. This works on the DM42 hardware, but for reasons of size it can’t really be backported to the physical machine in this form.

While doing this I took the opportunity to add a couple of other integration routines, listed below. The old WP34S routine is still there as well.

Here are the available integration routines in the &int;`f(x)` menu.

- <span><code>&int;dx</code></span>: this is the updated double exponential routine. Compared to the previous routine it is faster and otherwise just as good. This should be your default, unless your integrand oscillates a lot or has singularities in the range of integration.

- <span><code>&int;old</code></span>: this is the previous version of the double exponential routine, the one still used on the original WP34S. Included for comparison.

- <span><code>&int;QAG</code></span>: this is an adaptive Gauss-Konrod routine that is intended to cope with singularities within the range of integration. When increasing the number of samples it is able to focus on particular_ ranges, rather than the whole interval. It is based on routines from the GNU scientific library.

- <span><code>&int;ROM</code></span>: this is a Romberg integration routine, similar to the routines used by Free42 and (I believe) by the HP-15C. It is slow but (reasonably) sure, so long as the interval does not include singularities.

Note that <span><code>&int;QAG</code></span> and <span><code>&int;ROM</code></span> do not use decNumbers (the high-precision numbers used by the WP34S code) and so their precision is limited to maybe 15 digits.

The new integration routines also have some "extra features" which may prove useful. For more details please see the `Integration_routines` file ([pdf](./Integration_routines.pdf) or [markdown](Integration_routines.md)).

<div style="page-break-after: always;"></div>

# Basic File handling

I believe that it was possible to send files over the serial interface
on the original calculator if a USB interface was added, but dealing with files on the DM42 is much more straightforward.

There are two aspects to file handling &mdash; first, saving, backing up, and reloading
the calculator's state; and second, saving and loading programs. Let's
take these in turn.

## Saving and loading the calculator's state

The original WP34S calculator has 2 kB of non-volatile RAM (*user
memory*) and 128 kB of flash memory. 2 kB of this flash is used to
store a backup of user memory (*backup memory*). Most of the remaining
flash is used to store the firmware, with any remaining space
(*library memory*) being available for storing user programs. One
particularly nice feature of the original calculator (and this one
too!) is that programs in library and backup memory can be executed directly,
without first copying them into user memory.

This WP34S program is set up to work in a similar way, except that user
memory, backup memory, and library memory are all structures in
RAM. Normally user memory and backup memory are each 2 kB, but the 4k
build of the program doubles both of these. Library memory is always 16 kB in size. 

The contents of RAM are unaffected when the calculator is turned
off and may even survive battery replacement if done
promptly (no guarantees!). However, it would be nice to be able to quit the WP34S
program (perhaps to run a different calculator) without losing data,
and also to feel that data is secure when the battery is running
low. So user memory, backup memory, and library memory also exist as
three disk files, all found in the `wp34s/` folder of the DM42 disk.

- User memory is stored in `wp34s.dat`. User memory changes all the time as
  the calculator is used, so it's not practical to keep the file
  constantly updated. Instead, user memory is written to this file when
  the user exits the WP34S program via the `EXIT` command. (It
  can also be saved manually with the `SvRAM` command in the Setup 1 Menu.)
  This file contains everything stored in user memory &mdash; the programs stored in RAM, the data registers,
  flags, and system data. 

- Backup memory is stored in `wp34s-backup.dat`. The `OnSTO` command (in
  the SETUP 1 menu) copies the current state of user memory to backup
  memory, and also to this file. Regular use of this command will ensure
  that the contents of user memory are secure.

- Note that the files for user memory, backup memory, and user memory
  backup (see `LdRAM` later) for the 4k version of the program all
  have `_4k` appended to their file names.

- Library memory is stored in `wp34s-lib.dat`. Whenever library memory
  changes (i.e., when a program is added or deleted) the
  changes are immediately written to the file. Only the parts of the
  file that change are re-written, which saves both battery power and
  wear on the flash memory. Library memory contains programs that the
  user copies to it with `PSTO`, or loads with `LdPRG` (both SETUP 2).

So after using the calculator for a while and then `EXIT`ing it, these
three files may be found in the wp34s/ folder of the DM42 flash
drive. (The backup and library files will only be present if the user has written to them.) The next time that the WP34S software starts it will load them
all and be back in its previous state.

The WP34S calculator is **not** crash-proof: if the program crashes for
any reason there will not _automatically_ be a copy of user memory saved
to file. (Backup and library memory should be fine.) I considered saving user
memory each time the calculator is switched off, but on balance I
decided that the gain in security was more than outweighed by the
power used and the extra wear on the calculator's flash memory. The
option to save user memory manually, either with `SvRAM` or with `OnSTO`
should be enough. The calculator is reliable. If you are concerned,
use OnSTO regularly.

As well as the automatic writing of the calculator's state to
files, there are commands in the SETUP 1 and SETUP 2 menus that
allow the user to save and load such files directly.

* `OnSTO`, `OnRCL`:
  
  `OnSTO` copies the present contents of user memory to backup memory,
  and also to the backup memory file.

  `OnRCL` copies the contents of backup memory into user memory. OnRCL
  **overwrites** user memory; if this matters, save user memory with
  `SvRAM` beforehand. 
  
  Note that if you just need to load _part_ of backup memory, there are standard WP34S
  commands to let you access it. For example `PRCL` recalls the current program to user memory from backup or library; `LOADR` loads just the memory registers from backup to user memory. See the actual WP34S manual for details.

  `OnSTO` and `OnRCL` take their names from the key sequences that invoked
  them on the original hardware.

* `SvRAM`, `LdRAM`, `PrvRAM`: 
  
    `SvRAM` copies the current calculator state (user memory)
    to a named file with a .dat extension;
  
    `LdRAM` loads a named file with a .dat extension into
  user memory. Although `LdRAM` **overwrites** user memory, it first makes a backup copy of
  it in a file called `wp34s.bak`. You may still wish to make
  your own backup copy of user memory first, using either `SvRAM` or `OnSTO`.

  `PrvRAM` loads `wp34s.bak` into user memory; used to recover when `LdRAM` has overwritten something important. `PrvRAM`
    **overwrites** user memory and doesn't make a backup copy first,
    so think before you use it.
  
* `SvLIB`, `LdLIB`, `PrvLIB`: 
  
    `SvLIB` copies the current calculator library memory to a
  named file with a .lib extension.
  
  `LdLIB` loads a named file with a .lib extension into
  library memory. Although `LdLIB` **overwrites** library memory,
  it does make a backup copy in a file called `wp34s-lib.bak`.
  You may still wish to make
  your own backup copy of user memory first with `SvLIB`.

  `PrvLIB` loads `wp34s-lib.bak` into library memory; used to
  recover when `LdLIB` has overwritten something important. After
  executing `PrvLIB` the previous contents of library memory can be
  found in `wp34s-lib.bak`, so repeated executing of `PrvLIB` switches
  between two different libraries.

## Saving and loading programs

- The `SvCPRG` command (Save Current Program) writes a copy of the
  current program to a named file with a .dat extension. The current
  program must start with a global label &mdash; if not, it won't be
  written.

- The `LdPRG` command (Load Program) loads a program from a named file
  with a .dat extension
  into _library_ memory (_not_ into user memory). From there the program can be
  copied into user memory if desired, using the `PRCL` command. 

These two commands are found in the SETUP 2 menu.

There is more about saving and loading programs, and about how the
calculator deals with low battery when writing files, in the section below.

# The Assembler Tool Suite

The WP34S assembler tool suite was written long ago as part of the original WP34S calculator project. It allows programs written with a text editor on a computer to be "assembled" into a form which can be loaded into the WP34S. It also allows programs saved by the WP34S to be "disassembled" into readable source code. This is particularly useful with versions of the WP34S that can actually save and load files &mdash; e.g., the PC emulator, and of course this DM42 program.

First, some details.

## Op-codes

Programs are stored in WP34S memory not as readable text (e.g.,
`LBL'SN2' SIN 2 y^x RTN`) but as a series of two-byte _opcodes_. The
files written and read by `SvCPRG` and `LdPRG` contain these opcodes,
rather than text.
This means that you cannot save a program directly as readable text, nor can you load a text file containing a program directly into the calculator.

In an ideal world these opcodes would mean
the same thing for every instance of the WP34S software, but this is
not the case. The problem isn't unique to this DM42 program; it arose
during the development of the original calculator as new commands were
added (and old ones were removed), and different people chose to
include or exclude different groups of commands.
This means that you
cannot safely load a program file containing opcodes into this or
another WP34S calculator unless that program has been saved on a system running the
same or a very similar version of the WP34S software.

In practice, the proportion of opcodes that differ between this
program and the latest version of the physical WP34S is small, and if
you are transferring a program that only uses basic calculator features it
will work. However, the assembler tool suite provides a general
solution to this problem.

## Assembler Tool Suite to the rescue!

This is a suite of programs that can assemble WP34S source code into opcodes
and disassemble opcodes into WP34S source code, whilst taking into account
the differing meanings of opcodes between versions. The suite is
written in Perl and so should be usable on any system that supports
this. The suite can be found either on the original WP34S Sourceforge
site in the `trunk/tools` folder or here in the `src/tools` folder. 

Documentation is in the file [WP34s_Assembler_Tool_Suite.pdf](./WP34s_Assembler_Tool_Suite.pdf). This explains things very clearly; what follows is a brief summary of some of the information in that document.

If you neither have Perl installed nor the inclination to install it, you have taste. The
`trunk/tools` folder on the original WP34S Sourceforge site contains
executable versions of each tool, which appear to still run on Windows 11.

The idea is that each "build" of WP34S has an associated file, normally
called `wp34s.op`, that can be found in the `tools/` folder (`trunk/tools` on Windows, `src/tools` here), which is also where
the assembler tool suite lives. This file
contains a human-readable list of opcodes and the corresponding commands. Using this
file the disassembler can convert a program file
containing opcodes into source code, so that it can be printed and
read. This source code can then be assembled to work on a different
system, using the `wp34s.op` file for that system.
Or you can write a program for the WP34S using a text editor and assemble it yourself so that it can be loaded.

## State files and Library files

Before describing how to do this, some details about how the
WP34S system stores programs may be helpful.

The calculator stores programs in user memory, backup memory, and
library memory. Program memory has the same structure in user and backup memory, but library memory is slightly different. For this reason the commands in
the tool suite need to know what sort of file they are reading or
writing.

The tool suite documentation refers to a file containing a program (or
programs) in user memory format as a *state file*, and a file with a
program (or programs) in library memory format as a *library file*.

A library file (maximum size 8190 steps on the DM42) can store many
more programs than a state file (maximum size 928 steps), but a state
file can contain other data (registers, flags, etc.) in addition to
programs.

Single programs saved by `SvCPRG` are saved in library file format.

Both `wp34s.dat` and `wp34s-backup.dat` are state files; `wp34s-library.dat`
is a library file. (Yes, it ought to be called `wp34s-library.lib` but I didn't think of this until it was too late.)

## The commands

Note: when running the following commands, the file `wp34s.op` should
be present in the same folder as the Perl scripts or Windows executables. If more than one
opcode file is being used, the option `-op filename` on the command
line makes the command use `filename` as the opcode file.

### `wp34s_asm.pl`

The program **`wp34s_asm.pl`** allows state files and library files to be
disassembled to produce WP34S source code. It also allows WP34S source
code to be assembled to produce a state file or library file
containing one or more programs.

Examples:

- `wp34s_asm.pl -dis wp34s.dat > progs.wp34s` disassembles the programs in
  the state file `wp34s.dat` and puts their source code in `progs.wp34s`. 

- `wp34s_asm.pl -lib prog1.wp34s prog2.wp34s -o my_libfile.lib` assembles
  `prog1.wp34s` and `prog2.wp34s` and produces the library file
  `my_libfile.lib`.

  The `-lib` option makes the assembler produce a library
  file &mdash; this file can be loaded into the WP34S program by using
  the command `LdLIB`.

  Note that by default `wp34s_asm.pl` produces a state file; this is not clear in the official documentation. See the comments in the Perl script for details. 
  
- `wp34s_asm.pl progs.wp34s -o my_libfile.dat` assembles the programs in
  `progs.wp34s` and produces the state file `my_libfile.dat`. 

  With no `-lib`
  option the output file has the same format as the program section of a
  state file, and it can be
  loaded into the WP34S program by using the command `LdRAM`. (See later for more on this, including warnings!)

### `wp34s_pp.pl`

The program `wp34s_pp.pl` is a preprocessor for WP34S source code. This is useful when you are writing a program for the WP34S in a text editor. It 
can replace jumps to labels with hard-coded jumps (to increase speed)
and also allows alpha strings to be entered more than three characters
at a time. See the pdf documentation for more details. 

Normally there is no need to use it directly &mdash; using the option `-pp`
with `wp34s_asm.pl` means the preprocessor is automatically run on
source files before assembling them. 
Including `-pp` on the command line when the preprocessor is not needed does no harm.

However, there is a bug in the preprocessor! It reads the source file into an array called `@src`, then works through it using a while loop that starts `while ($line = shift @src)`. The problem is that if a line of the source file contains a single zero (which it might) the loop exits, as zero is false! To work around this, either make sure that this doesn't happen (e.g., by using line numbers) or change the Perl source code by replacing the line (this only occurs once)

```
while ($line = shift @src) {
```

with

```
foreach (@src) {
	$line = $_;
```

This has already been done in the included Perl file.



### `wp34s_lib.pl`

The program `wp34s_lib.pl` is a program for manipulating the programs in
state files and library files. It can produce a catalogue of the
programs in a file; it can add or remove programs; and like the
assembler it can be used to build a library of programs from files
containing source code.

Examples:

- `wp34s_lib.pl -cat -ilib my_libfile.lib` produces a list of the names of programs stored in `my_libfile.lib`.

- `wp34s_lib.pl prog1.wp34s prog2.wp34s -olib my_libfile.lib` assembles
  the programs `prog1.wp34s` and `prog2.wp34s` and produces the library file
  `my_libfile.lib`. 
  

`wp34s_lib.pl` produces library files by default. It can produce state files with `-state` on the command line.

Many more examples can be found in the assembly suite documentation
referred to above. 

## Using `LdRAM` to load a single program

As described above, `LdRAM` loads a `.dat` file into user memory. Some
`.dat` files are copies of user memory &mdash; those created by `SvRAM`, for
example &mdash; so loading these presents no difficulty. However, other files also have a `.dat` extension: a state file containing only
programs with no data registers or flags, perhaps produced by the
assembler program; a single program in library file format saved by
`SvCPRG`; or the library file itself, `wp34s-lib.dat`.  What happens
if `LdRAM` is used to load one of these `.dat` files?

User memory has a size of either 2kB or 4kB. If the size of a `.dat` file is bigger than this it is not loaded. If it is less than this it isn't loaded either, *unless* its size is less than or equal to the current available space for programs in user memory. in this event the contents of the `.dat` file overwrite the section of user memory that holds programs. This allows `LdRAM` to load a program (or programs) directly into user memory.

This works whether the `.dat` file is state file format or library file format.

Any programs previously in user memory are lost, so back them up first in some way! They are replaced by the program (or programs) contained in the `.dat` file.

If the `.dat` file doesn't contain programs the calculator will get confused, but it shouldn't crash.  At this point you can: clear program memory; or load your user memory backup (if you made one!);
or load the file `wp34s.dat`; or use the command `PrvRAM`
to load the copy of user memory saved by `LdRAM` immediately before
loading the `.dat` file.  Backing up before you load a dubious `.dat`
file is sensible. Of course, backing up is a good
thing to do in general!

The message is that if you simply want to load one program into user memory and you
don't care about losing the other programs in user memory, using `LdRAM`
is fine. (`LdPGM` always loads a program into library memory.)

## File error messages 

File errors should be rare. If the files `wp34s.dat`, `wp34s-backup.dat`,
and `wp34s-lib.dat` don't exist, they are created when needed. Unless
the calculator's disk space is full there shouldn't be any problems
with this, assuming that there are no bugs in the code!
However, the file-handling code checks for errors and
displays error messages when something does go wrong.

These messages mostly consist of a description of the file being read or written, an error code,
and the file action being attempted: for example, "RAM file", 16,
"Open Error". This means that error 16 occurred when trying to open
the RAM file, which is normally `wp34s.dat`.

You can find the details of the error codes at the bottom of this
page:
[technical.swissmicros.com](https://technical.swissmicros.com/dmcp/doc/DMCP-ifc-html/group__disk__fatfs.html).

## Low Battery handling

Before writing data to disk, the program calls a DMCP function to check whether the battery
is likely to be able to do this. If this test fails, a message advises
the user to connect to USB power.  If the user decides not to do this,
the data is written to the relevant part of calculator
memory (e.g., backup memory or library memory) but not to the matching
disk file.

What happens next depends on the situation.

Recall that when the user quits the calculator via the `EXIT` command user memory is written to disk. A failed power test here allows the user to choose not to
exit until power is restored, or to exit anyway (thus losing data).

The calculator also checks that library and backup memory are in sync
with the disk copies when exiting the calculator; if they aren't, memory is written to
disk. If this fails the user is informed of this and can choose not to
exit until power is restored.

Something similar happens when `LdRAM` tries to write a backup file before
loading a new file into user memory: if the file can't be written, the user can choose to cancel the operation before harm is done, or to proceed anyway.

Whenever a change is made to library memory, the same change is
written to disk. If this can't be done then the contents of library memory will diverge from the copy on disk. Once power is restored the code can tell that this has happened and the
next time that library memory is changed a complete copy is written to
disk, bringing memory and disk back into sync.

Cheaper batteries tend to have a higher internal resistance, which means that their voltage drops more rapidly with increased current draw. Such batteries may be unable to supply the current needed to write to disk without a serious voltage drop, which can corrupt the disk and/or crash the calculator completely. Although the calculator does check battery state before writing it is not infallible. Use decent batteries; if unsure about battery state, connect to USB power before writing.