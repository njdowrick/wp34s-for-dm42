## Introduction

[Back to README](../README.md#description)

This document explains how to use **complex lock mode**. Complex lock mode was
originally written for the physical WP34S calculator. Because
some WP34S features had to be left out to make room for the extra
code, the firmware containing complex lock mode was given a new
name: WP34C.

On the DM42 hardware, space is not a problem – there is more than enough
room for everything\! So complex lock mode is a standard part of the
WP34S program for the DM42. If you don’t use complex numbers, don’t
worry. Complex lock mode won’t get in your way, but it’s there if you
are interested.

The idea of complex lock mode is to make entering and working with complex
numbers on the WP34S as easy as possible. The WP34S already has superb support for complex
numbers but they are slightly awkward to enter, and the need to press
`CPX` before every complex operation is not ideal. In complex lock mode
you enter real and imaginary parts in that order, with
prompts. You can also choose to enter and display complex numbers in
“Length” and “Angle” form with no further effort on your part. Every
relevant key on the keyboard is processed as with a `CPX` prefix
automatically.

Complex lock mode has limitations: while it is turned on there are no
program functions, no statistics functions, no matrix functions, and no
integer (base) functions. (All of these features are still available
outside of complex lock mode.) If you can live with this when working with
complex numbers, you should find complex lock mode useful.

## Complex Lock Mode

To enable complex lock mode, execute `CPXYES` (in the mode
catalogue). Once this is done, complex lock mode can be turned on or off by pressing
`F5` (`→`) followed by `F6` (`CPX`) (when in a menu with `→`
and `CPX` assigned to these keys). `C_LK` should appear at the top right of
the display when complex lock mode is turned on. 

Now pressing (for example) `5` `CPX` `12` `ENTER` should enter 5
into the X-register and 12 into the Y-register, representing the complex
number (5+12i). Pressing the square root key then displays the number (3+2i).

When complex lock mode is entered the `COMPLEX` menu is displayed. This contains a lot of complex-mode-specific functions; the `MISC` menu (`f`-hold-<code><span>&xdtri;</span></code>) does too. Other menus change in complex lock mode. Some are missing completely; others are modified so that their entries are relevant. 
To re-display the `COMPLEX` menu at any time, press `f` `COMPLEX` (the `STO` key).

Apart from the WP34S features that don’t work at all in this mode,
everything else works more-or-less as you would expect. The table that
follows sets it out in full detail. It looks like a lot, but if you need
to use complex numbers regularly it’s probably worth a look.

It might also help to look at the [quick reference
guide](https://gitlab.com/njdowrick/wp34s-for-dm42/-/blob/master/help/Complex_Lock_quick_reference_guide.pdf)
booklet for complex lock mode (available in the /help folder).

In these instructions:

  - X, Y, Z, T, A, B, C, D, I, J, K and L refer to the storage
    registers.

  - x, y, z, t, a, b, c, d, l, i, j, and k refer to the register
    contents.

  - `f` `KEY` means pressing the yellow shift key followed by KEY.

  - `g` `KEY` means pressing the yellow shift key twice, followed by KEY.

<table>
<thead>
</thead>
<tbody>
<tr class="header">
<td><code>CPXYES</code> – found in the MODE catalogue, accessed by <code>g</code> <code>+/-</code> .</thd>
<td>Executing this allows the <code>→</code> <code>CPX</code> key combination to activate complex lock mode. It has no other affect on the operation of the calculator. <strong><code>CPXYES</code> must be executed before complex lock mode can be entered for the first time.</strong></td>
</tr>
</thead>
<tr class="odd">
<td><code>CPXNO</code> – also in the MODE catalogue.</td>
<td>Executing this command prevents the WP34S from entering complex lock mode when the <code>→</code> <code>CPX</code> combination is pressed. This is the default.</td>
</tr>
<tr class="even">
<td><code>→</code> <code>CPX</code> – this is entered by pressing the fifth and sixth function keys (present in nearly every menu), one after the other.</td>
<td><p>Enter or leave complex lock mode.</p>
<ul>
<li><p>When in complex mode a small “C_LK” annunciator is displayed. The “i" or “angle” indicators are continuously present in the dot matrix display.</p></li>
<li><p>Entering complex lock mode sets stack size to 8 to mimic a 4-level complex stack. Pre-existing values in X, Y, Z, T, A, B, C and D are retained. The original stack size is restored on leaving complex lock mode.</p></li>
<li><p>A useful menu (the COMPLEX menu in what follows) is displayed. This menu can be returned to by pressing <code>f</code> <code>STO</code>.</p></li>
<li><p>Other menus can be selected. They should contain only complex-appropriate entries. Some menus (e.g., P.FCN, STATS, BASE) do not exist in complex lock mode.</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Numeric keys (.0123456789)</td>
<td><ul>
<li><p>During numeric entry: do the obvious;</p></li>
<li><p>Outside of numeric entry: start entry of real part or “length” of complex number, depending on the entry mode.</p></li>
</ul></td>
</tr>
<tr class="even">
<td><code>CPX</code> (<code>F6</code> key) (in complex lock mode; operates normally outside this mode)</td>
<td><p>Terminates the entry of the real or imaginary part of a complex number. In more detail:</p>
<ul>
<li><p>During entry of real part: <code>CPX</code> ends it and starts the entry of the imaginary part.</p></li>
<li><p>During entry of imaginary part: <code>CPX</code> terminates it (making it zero if nothing has been entered) and places the real part in the X-register and the imaginary part in the Y-register; enables stack lift.</p></li>
<li><p>Outside of numeric entry: respects the stack lift setting, sets the real part to zero, and starts the entry of the imaginary part. (So 0 + i1 may be entered by simply pressing <code>CPX</code>, 1, then <code>CPX</code>.)</p></li>
<li><p>In POLAR entry mode, <code>CPX</code> works in an analogous way separating and terminating the entry of complex “length” and “angle”.</p></li>
</ul></td>
</tr>
</tbody>
</table>
<table>
<thead>
</thead>
<tbody>
<tr class="header">
<td>ENTER key:</td>
<td><p>Terminates the entry of a number, puts it on the stack and duplicates it. In more detail:</p>
<ul>
<li><p>During entry of real part: finishes real part, sets imaginary part to zero, puts the number into (X, Y) and (Z, T), pushes previous (z, t) into (A, B), pushes previous (a, b) into (C, D), previous contents of (C, D) lost; disables stack lift.</p></li>
<li><p>During entry of imaginary part: finishes imaginary part, puts the number into (X, Y) and (Z, T), pushes previous (z, t) into (A, B), pushes previous (a, b) into (C, D), previous contents of (C, D) lost; disables stack lift.</p></li>
<li><p>Outside of numeric entry: duplicates (x, y) into (Z, T), pushes previous (z, t) into (A, B), pushes previous (a, b) into (C, D), previous contents of (C, D) lost; disables stack lift.</p></li>
<li><p>In POLAR entry mode, works similarly with the “length” and “angle” parts of a complex number.</p></li>
</ul></th>
</td>
<tr class="odd">
<td>CHS key:</td>
<td><ul>
<li><p>During numeric entry: changes the sign of the real or imaginary part being entered;</p></li>
<li><p>Outside of numeric entry: changes the sign of both x and y registers, i.e., of the lowest complex number on the stack.</p></li>
</ul></td>
</tr>
<tr class="even">
<td>EEX key:</td>
<td><ul>
<li><p>During numeric entry: starts entry of exponent;</p></li>
<li><p>Outside of numeric entry: enters 1 E000, waits for entry of exponent.</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>General functions on the keyboard – e.g., arithmetic, trigonometric, STO, RCL, logarithms, ...</td>
<td><ul>
<li><p>During numeric entry of real part: entry is terminated, the function is executed on the value x + i 0, i.e. y automatically set to zero.</p></li>
<li><p>During numeric entry of imaginary part: entry is terminated, the function is executed on the value x + i y, no need to press <code>CPX</code> or ENTER to terminate entry of complex value.</p></li>
<li><p>Outside of numeric entry: carry out operation on the values in x and y using the defined complex version of the function (same as accomplished in normal mode by preceding the desired operation with <code>CPX</code>.)</p></li>
<li><p>Note that whatever the angular mode setting, all trigonometric functions work in and supply results in radians, except for R→P and P→R (described below).</p></li>
</ul></td>
</tr>
<tr class="even">
<td>STO nn, RCL nn</td>
<td>Allows storage of x and y to or recall from a pair of registers nn and nn+1. To prevent inadvertent overwriting of components by multiple STO operations (e.g., STO 00 will store the real and imaginary parts in 00 and 01; subsequent STO 01 of a second value would overwrite the imaginary part of the first stored value with the real part of the second value) only even values may be specified for nn. If an odd value is entered, the message “Use even reg For Complex” will appear. Pressing ← will revert back to STO n_ or RCL n_ and allow an even register to be specified. Pressing STO or RCL will restart the operation. Pressing EXIT will revert to the prior stack display with no STO or RCL operation performed. Stack registers X, Z, A and C, plus lettered registers L and J are considered even.</td>
</tr>
<tr class="odd">
<td>π (f + R↓)</td>
<td>Enters the complex value of 3.1415… + i 0 into x and y, pushes stack.</td>
</tr>
<tr class="even">
<td>fπ (f + F5 in COMPLEX menu)</td>
<td><ul>
<li><p>If pressed before numeric entry begins, pi is put into the real part of the new number. This can be overwritten by entering digits, or the imaginary part can be entered by pressing <code>CPX</code>.</p></li>
<li><p>If pressed immediately after <code>CPX</code>, pi is put into the imaginary part of the new number. It can be overwritten with digits or entry can be completed by pressing <code>CPX</code>, ENTER, or any operator in the usual way.</p></li>
<li><p>If fπ is entered <strong>after</strong> entering some digits into the real or imaginary part, those digits are <strong>multiplied</strong> by pi. So “2 <code>CPX</code> .5 fπ” enters the number 2 + i 0.5 π.</p></li>
<li><p>Note that if you want a fractional multiple of pi such as pi/12 + i4 this can be done by entering 1/12 as a fraction in the real part and then pressing fπ.</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>R→P and P→R (found on the COMPLEX menu):</td>
<td>Convert form of complex number using current angular mode setting. E.g., if DEG mode is chosen, 3 + i 4 will be converted to 5 (angle) 53.13 by R→P. The (angle) symbol will be temporarily displayed. As soon as this value is worked with, or EXIT is pressed, the (angle) symbol will revert to i. It is up to the user to remember that the i value represents an angle in the current angular mode setting. 5 + i 53.13 will be converted to 3 + i4 by P→R .</td>
</tr>
<tr class="even">
<td>POLAR entry / display mode (POLAR in COMPLEX menu):</td>
<td><p>This allows the entry and display of complex numbers in polar form. It is generally <strong>much</strong> more useful than the R→P function described above. In POLAR mode:</p>
<ul>
<li><p>“Length” (modulus) and “Angle” (argument) are displayed in place of REAL and IMAG during numeric entry.</p></li>
<li><p>Calculation results are displayed in polar form too, but are stored in (x, y) form internally so that all complex functions work as they should.</p></li>
<li><p>Contrast this with R→P, which actually replaces the (x, y) form with the polar form in the X and Y registers.</p></li>
<li><p>Angle entry and display is governed by the current angular mode setting.</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>RECTANGULAR entry / display mode (RECT in COMPLEX menu):</td>
<td>Returns from POLAR entry / display mode to RECTANGULAR entry / display mode, with REAL and IMAG parts.</td>
</tr>
<tr class="even">
<td>← key:</td>
<td><ul>
<li><p>During numeric entry: deletes preceding digit;</p></li>
<li><p>Outside of numeric entry: deletes x and y leaving stack lift disabled.</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>EXIT key:</td>
<td><ul>
<li><p>During numeric entry of real or imaginary part: value entry is cancelled and former stack contents restored.</p></li>
<li><p>Outside of numeric entry: same as in normal mode, will exit out of a catalog, exit out of an uncompleted STO or RCL, exit SHOW mode, exit temporary VIEW, etc.</p></li>
<li><p>Will not exit complex mode.</p></li>
</ul></td>
</tr>
<tr class="even">
<td>X.FCN catalogue (g 3)</td>
<td><ul>
<li><p>Enters eXtended-functions catalogue. Only functions defined for complex operation, i.e., those in the full catalog preceded by the superscript c, are displayed. Pressing ENTER or XEQ executes the displayed function.</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>X↔︎Y key:</td>
<td>Swaps (X, Y) with (Z, T) – i.e., complex swap</td>
</tr>
<tr class="even">
<td>Re↔︎Im in COMPLEX menu:</td>
<td>Swaps real and imaginary parts of number (i.e., changes x + i y to y + i x)</td>
</tr>
<tr class="odd">
<td>+/- y (COMPLEX menu):</td>
<td>Changes sign of y only (i.e., complex conjugate, x + i y becomes x – i y)</td>
</tr>
<tr class="even">
<td>+/- x (COMPLEX menu):</td>
<td>Changes sign of x only (x + i y becomes -x + i y)</td>
</tr>
<tr class="odd">
<td>0 → Re (MISC menu):</td>
<td>Sets x to zero (i.e., takes imaginary part, x + i y becomes 0 + i y)</td>
</tr>
<tr class="even">
<td>0 → Im (MISC menu):</td>
<td>Sets y to zero (i.e., takes real part, x + i y becomes x + i 0)</td>
</tr>
<tr class="odd">
<td>Σ+ key:</td>
<td>Enters <sup>c</sup>Hyp prefix for complex hyperbolic functions. Pressing SHIFT after this allows inverse hyperbolic functions. (HYP was assigned to this key on the original calculator.) HYP is also available from the COMPLEX menu and from other places.</td>
</tr>
<tr class="even">
<td>RND (MISC menu):</td>
<td>Rounds X and Y (real and imaginary) to the number of digits specified by the current display setting.</td>
</tr>
<tr class="odd">
<td>|z| (MISC menu):</td>
<td>Returns the magnitude of x + i y to X; y set to zero.</td>
</tr>
<tr class="even">
<td>CONST (g 5):</td>
<td>Enters constants menu. If a constant is selected by pressing ENTER, chosen constant is copied to X, y set to zero. If a number is being entered when this key is pressed the entry of the number is completed before the constant is entered.</td>
</tr>
<tr class="odd">
<td>fCNST (COMPLEX menu):</td>
<td><p>Use this for entering a constant into the real or imaginary part of a complex number,</p>
<ul>
<li><p>If no number is being entered, begins complex entry and enters the constant into the real part of the number.</p></li>
<li><p>If the real part of a number is being entered, overwrites it with the constant.</p></li>
<li><p>If <code>CPX</code> has been pressed so that the imaginary part is being entered, the constant is entered into the imaginary part of the complex number without clearing the real part.</p></li>
</ul></td>
</tr>
<tr class="even">
<td><sup>c</sup>.× (dot times – MISC menu):</td>
<td>Returns z*x to X, t*y to Y, consuming former x + iy and z + it values, drops stack. (Note that complex DOT and CROSS product functions are available in X.FCN menu.)</td>
</tr>
<tr class="odd">
<td><sup>c</sup>./ (dot divide – MISC menu):</td>
<td>Returns z/x to X, t/y to Y, consuming former x + iy and z + it values, drops stack.</td>
</tr>
<tr class="even">
<td>CLx (clear x – CLEAR menu):</td>
<td>Deletes x value, begins real value entry. Pressing <code>CPX</code> completes entry of new x value, begins imaginary value entry. Prior y value will be displayed. Pressing <code>CPX</code> will terminate entry, with former y value retained. Useful for changing only the real part of an existing complex value. Imaginary part can be changed by exchanging x and y before and after this function. (Before pressing <code>CPX</code> the second time, a new y value may be keyed in and terminated with <code>CPX</code>. This will result in the same completely new x + i y as can be obtained by pressing  by itself at the start.)</td>
</tr>
<tr class="odd">
<td>CPXI – found in the MODE catalogue, which is accessed by g +/-.</td>
<td>Second line of display preceded by “i" to indicate the imaginary part of the x + i y value represented by the contents of X and Y. Note that this preference applies both in and out of complex lock mode.</td>
</tr>
<tr class="even">
<td>CPXJ – also in the MODE catalogue.</td>
<td>Second line of display preceded by “j" to indicate the imaginary part of the x + j y value represented by the contents of X and Y. Note that this preference applies both in and out of complex lock mode.</td>
</tr>
</tbody>
</table>

All newly defined and existing functions will show the function name if
the key is held down, and null out if the key is held down long enough.

[Back to top](#introduction)
