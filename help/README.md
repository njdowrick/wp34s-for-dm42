## Documentation Folder

#### Using this program

- General and detailed information about using the calculator, especially about the differences from the original WP34S and the new features added: [`md`](./Using.md) [`pdf`](./Using.pdf) [`html`](./Using.html)
- Quick reference guide booklet for the calculator: [`pdf`](./QRG-booklet_1.2.pdf)
- Picture and explanation of keyboard layout: [`md`](./wp34s_layout_help.md) [`pdf`](./wp34s_layout_help.pdf) [`html`](./wp34s_layout_help.html)
- Help file for the calculator (the HTML version can be loaded onto the calculator itself): [`md`](./wp34s_help.md) [`pdf`](./wp34s_help.pdf) [`html`](./wp34s_help.html) 

#### More detailed explanation of some features

- Details of complex lock mode: [`docx`](Complex_Lock_Mode_Details.docx) [`pdf`](Complex_Lock_Mode_Details.pdf) [`md`](Complex_Lock_Mode_Details.md) 
- Complex lock mode quick reference guide: [pdf](./Complex_Lock_quick_reference_guide.pdf)
- Details of the integration routines available on the calculator: [`docx`](./Integration_routines.docx) [`md`](./Integration_routines.md) [pdf](./Integration_routines.pdf) 

#### Original documentation from the WP34S calculator project

- WP34S User Manual 3.1: [`pdf`](./WP34S_Manual_V3_1.pdf)
- WP34S Beginner's Guide: [`pdf`](./WP34S_Beginners_Guide.pdf)
- WP34S Assembler Tool Suite documentation: [`pdf`](./WP34s_Assembler_Tools_Suite.pdf)
