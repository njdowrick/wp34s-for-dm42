# WP34S for DM42

## Contents

[About this program](#about-this-program)

[Exiting the program\!](#exiting-the-program)

[Notation](#notation)

[Keyboard and shift states](#keyboard-and-shift-states)

[Annunciators](#annunciators)

[Menus](#menus)

  - [Menus on f-shifted keys](#menus-on-f-shifted-keys)
  - [Menus on g-shifted keys](#menus-on-g-shifted-keys)

[Accessing catalogues](#accessing-catalogues)

  - [f-shifted catalogues](#f-shifted-catalogues)
  - [g-shifted catalogues](#g-shifted-catalogues)

[Unshifted keys](#unshifted-keys)

[f-shifted keys](#f-shifted-keys)

[g-shifted keys](#g-shifted-keys)

[Alpha mode](#alpha-mode)

[Working with files](#working-with-files)

[IR Printing](#ir-printing)

[The stopwatch](#the-stopwatch)

[User-defined menus](#user-defined-menus)

[RAM and LIB menus](#ram-and-lib-contents-menu-generator)

[Entry RPN](#entry-rpn)

[Complex Lock mode](#complex-lock-mode)

[User Help Files](#user-help-files)

[Integration functions](#integration-functions)

<span id="about-this-program"></span>

## About this program

This program is a port of the WP34S calculator to the DM42 platform.  
It includes much of the original WP34S code with some changes and
extensions.  
All of the original calculator's standard features are supported, along
with the [stopwatch](#the-stopwatch), [IR printing](#ir-printing),
[complex lock mode](#complex-lock-mode), [entry RPN](#entry-rpn),
[extended file handling](#working-with-files), and [user-defined
menus](#user-defined-menus).  
As on the original calculator the calculator state occupies 2kB. (There
is also a 4kB version.)  
The library space for user programs is 16kB (i.e., 8k program steps).  
There are two versions of the program which differ only in their
displays (see the file README.md for details). Everything that follows
applies to both versions. <span id="exiting-the-program"></span>

<span id="exiting-the-program"></span>

## **Exiting the program**

If you just want to turn the calculator off, press
<span class="button">SHIFT</span> <span class="button">EXIT</span> .  
To get back to the DMCP menu (e.g., in order to reload the Free42
firmware):

  - Press <span class="button">SHIFT</span>
    <span class="button">SETUP</span> . This opens the SETUP menu.
  - Press <span class="button">F1</span> (EXIT).
  - You can then use <span class="lcd">3: Load Program</span> to choose
    a different firmware file to load, or <span class="lcd">2: Run
    Program</span> to return to the already-loaded WP34S.

<span id="notation"></span>

## Notation

In what follows,

  - <span class="button">TAN</span> means the button labelled TAN;
  - <span class="button">f</span> <span class="button">TAN</span> means
    one press of the shift key, followed by the button labelled TAN.
    This may also be shown as <span class="button">f</span>
    <span class="button">ATAN</span> .
  - <span class="button">g</span> <span class="button">TAN</span> means
    two presses of the shift key, followed by the button labelled TAN.
    Where appropriate, this may also be shown as
    <span class="button">g</span> <span class="button">ATAN</span> .
  - <span class="lcd">HELP</span> means the menu button labelled HELP on
    the screen. This may also be shown as
    <span class="button">F2</span> , where F2 means the second menu key
    counting from the left.
  - <span class="lcd">This style</span> is also used to show any
    displayed text or numbers.

<span id="keyboard-and-shift-states"></span>

## Keyboard and Shift states

DM42 keys are mapped to WP34S functions so that wherever possible, the
keys do what they say.  
For example, the <span class="button">TAN</span> key computes the
tangent of an angle; <span class="button">f</span>
<span class="button">ATAN</span> computes the inverse tangent; in alpha
mode the same key enters L, l, lambda, etc.  
The WP34S <span class="button">→</span> and
<span class="button">CPX</span> keys are provided in this program by the
keys <span class="button">F5</span> and
<span class="button">F6</span> .  
Outside of alpha mode there are two shift states:
<span class="button">f</span> (one press of
<span class="button">SHIFT</span> ) and <span class="button">g</span>
(two presses). A third press cancels the shift.  
In alpha mode there is a third shift state
<span class="button">h</span> , invoked by pressing
<span class="button">SHIFT</span> three times.

<span id="annunciators"></span>

## Annunciators

The names and meanings of some annunciators have been changed from those
on the original calculator.  

  - INPUT is now <span class="lcd">ALPHA</span> ; this shows ALPHA mode.
  - The down arrow <span class="lcd">↓</span> still means lower case in
    ALPHA mode.
  - BEG is now either <span class="lcd">S:4</span> or
    <span class="lcd">S:8</span> , depending on the stack size.
  - 360 is now <span class="lcd">DEG</span> , showing degrees mode.
  - The little <span class="lcd">=</span> sign turns on to show that the
    stopwatch is running in the background. It does not flash. The same
    display space shows <span class="lcd">C\_LK</span>  when complex
    lock mode is on.
  - STO is now <span class="lcd">PGM</span> , showing program entry
    (Learn) mode.
  - <span class="lcd">RPN</span> becomes <span class="lcd">eRPN</span>
    in entry RPN mode.
  - RCL is now <span class="lcd">RUN</span> , to show that a program is
    running. This annunciator does not flash.
  - There is a new annunciator <span class="lcd">PRT</span> which
    indicates that the printing commands are enabled.
  - There are annunciators for low battery
    (<span class="lcd">LOW</span>) and USB power
    (<span class="lcd">USB</span>) .

<span id="menus"></span>

## Menus

Menus use the top row of function keys, unshifted and f-shifted, to
provide the full set of WP34S keyboard functions along with some
extras.  
Almost every menu has the <span class="lcd">→</span> and
<span class="lcd">CPX</span> functions on unshifted
<span class="button">F5</span> and <span class="button">F6</span> .

  - Calling the same menu twice brings up the *default menu*.
  - The default menu can also be reached by pressing
    <span class="button">f</span> <span class="button">ASSIGN</span> .
  - Initially the default menu displays only <span class="lcd">→</span>
    and <span class="lcd">CPX</span> keys.
  - Pressing <span class="button">f</span>
    <span class="button">ASSIGN</span> when the default menu is
    displayed toggles between this version and one that includes
    <span class="lcd">Σ+</span> , <span class="lcd">1/x</span> ,
    <span class="lcd">√</span> , and <span class="lcd">LOG</span> ,
    which is helpful when labels A-D are assigned to these keys.
  - Pressing
    <span class="button">SHIFT</span>-hold-<span class="button">▲</span>
    goes to the previous menu.

Most menus are on f-shifted keys; a few are on g-shifted keys.  
See the sections on [Alpha mode](#alpha-mode) and [Complex Lock
mode](#complex-lock-mode) for menus in these modes.

<span id="menus-on-f-shifted-keys"></span>

### Menus on f-shifted keys

  - ANGLES menu <span class="button">f</span>
    <span class="button">MODES</span> - angle modes, H.MS / H.d,
    co-ordinate conversions and hyperbolic prefixes. This menu is also
    available from <span class="button">g</span>
    <span class="button">SIN</span> , <span class="button">g</span>
    <span class="button">COS</span> , <span class="button">g</span>
    <span class="button">TAN</span> .
  - DISPLAY menu <span class="button">f</span>
    <span class="button">DISP</span> - display modes, fraction modes,
    radix choice, and H.MS / H.d.
  - CLEAR menu <span class="button">f</span>
    <span class="button">CLEAR</span> - clear program, clear stats,
    clear x, etc.
  - INTEGRATE menu <span class="button">f</span>
    <span class="button">∫f(x)</span> - SLV, ∫dx, ∫old, ∫QAG, TOG\_A;
    SUM, PROD, f'(x), f''(x).
  - STATS menu <span class="button">f</span>
    <span class="button">STATS</span> - Xbar, Yhat, s (variance) and r
    (correlation coefficient), a link to the SUMS catalogue, etc.
  - BASE menu <span class="button">f</span>
    <span class="button">BASE</span> - BIN, OCT, DEC, HEX, not, and, or,
    xor, and RMDR.
  - FLAGS menu <span class="button">f</span>
    <span class="button">FLAGS</span> - SF, CF, flag tests, comparison
    functions.
  - PROBABILITY menu <span class="button">f</span>
    <span class="button">PROB</span> - factorial, permutations,
    combinations, random numbers, the normal cdf and its inverse.
  - SETUP 1 menu <span class="button">f</span>
    <span class="button">SETUP</span> - Exit to the DMCP menu, System,
    Help, OnSTO and OnRCL, save state to and load state from named file.
  - MISC menu
    <span class="button">SHIFT</span>-hold-<span class="button">▼</span>
    - functions that did not fit anywhere else\! |x|, RND, IPart, FPart,
    2<sup>x</sup>, Log<sub>2</sub> x, llel, delta percent.
  - CUSTOM menu <span class="button">f</span>
    <span class="button">CUSTOM</span> - displays the current user menu.
  - PRINT menu <span class="button">f</span>
    <span class="button">PRINT</span> - a range of printing functions.

<span id="menus-on-g-shifted-keys"></span>

### Menus on g-shifted keys

  - X\<\>Y menu <span class="button">g</span>
    <span class="button">x\<\>y</span> - Show\<-, Show-\>, and exchange
    functions.
  - SELECT USER MENU menu <span class="button">g</span>
    <span class="button">CUSTOM</span> - functions for choosing a
    [user-defined menu](#user-defined-menus).
  - PROGRAM FUNCTION menu<span class="button">g</span>
    <span class="button">PRGM</span> - LBL, RTN, Pause, GTO, DSE, ISG.
  - SETUP 2 menu <span class="button">g</span>
    <span class="button">SETUP</span> - Save library to / load library
    from named file; save program to / load program from named file.

<span id="accessing-catalogues"></span>

## Accessing catalogues

The WP34S makes most of its functions available via catalogues. This is
where to find them on the DM42 keyboard.  
<span id="f-shifted-catalogues"></span>

### f-shifted catalogues:

|        |  |                                                                   |
| ------ |  | ----------------------------------------------------------------- |
| MATRIX |  | <span class="button">f</span> <span class="button">MATRIX</span>  |
| CONV   |  | <span class="button">f</span> <span class="button">CONVERT</span> |
| P.FCN  |  | <span class="button">f</span> <span class="button">PGM.FCN</span> |
| CAT    |  | <span class="button">f</span> <span class="button">CATALOG</span> |


<span id="g-shifted-catalogues"></span>

### g-shifted catalogues:

|        |  |                                                                   |
| ------ |  | ----------------------------------------------------------------- |
| MODE   |  | <span class="button">g</span> <span class="button">MODES</span>   |
| SUMS   |  | <span class="button">g</span> <span class="button">MATRIX</span>  |
| STAT   |  | <span class="button">g</span> <span class="button">STAT</span>    |
| CONST  |  | <span class="button">g</span> <span class="button">CONVERT</span> |
| TEST   |  | <span class="button">g</span> <span class="button">FLAGS</span>   |
| PROB   |  | <span class="button">g</span> <span class="button">PROB</span>    |
| X.FCN  |  | <span class="button">g</span> <span class="button">PGM.FCN</span> |
| SHOW   |  | <span class="button">g</span> <span class="button">EXIT</span>    |
| STATUS |  | <span class="button">g</span> <span class="button">CATALOG</span> |

<span id="unshifted-keys"></span>

## Unshifted keys

These all do much what you would expect from their labels.  
The four keys <span class="button">Σ+</span> ,
<span class="button">1/x</span> , <span class="button">√x</span> , and
<span class="button">LOG</span> on the top row run code starting from
labels A, B, C, and D if defined in the current program. The normal
functions of the keys are always available via
<span class="button">g</span> .  
The Arrow key <span class="button">F5</span> brings up a temporary menu.
Choosing a base from this menu gives a temporary display of the
x-register in that base; choosing an angular unit converts from the
current angular mode to that unit. Conversion to and from HMS is also
possible. 

<span id="f-shifted-keys"></span>

## f-shifted keys

Like the unshifted keys, these do what their labels say or at least
something closely related.  

  - <span class="button">COMPLEX</span> - outside of complex lock mode,
    same function as the CPX key <span class="button">F6</span> .
  - <span class="button">LASTx</span> - does RCL L, which is how Last X
    works on the WP34S.
  - <span class="button">MODES</span> - angles menu (including angle
    modes).
  - <span class="button">DISP</span> - display format menu.
  - <span class="button">CLEAR</span> - menu of clearing operations.
  - <span class="button">SOLVER</span> - the solver function, SLV.
  - <span class="button">∫f(x)</span> - INTEGRATE menu.
  - <span class="button">MATRIX</span> - MATRIX catalogue.
  - <span class="button">STAT</span> - STAT menu.
  - <span class="button">BASE</span> - BASE menu.
  - <span class="button">CONVERT</span> - CONV catalogue.
  - <span class="button">FLAGS</span> - Flags menu.
  - <span class="button">PROB</span> - Probability menu.
  - <span class="button">ASSIGN</span> - Default menu.
  - <span class="button">CUSTOM</span> - current user menu.
  - <span class="button">PGM.FCN</span> - the P.FCN catalogue.
  - <span class="button">PRINT</span> - PRINT menu.
  - <span class="button">SETUP</span> - Setup 1 menu.
  - <span class="button">SHOW</span> - Show←, which shows the full
    precision of a number in single-precision mode.
  - <span class="button">PGRM</span> - toggles programming mode.
  - <span class="button">CATALOG</span> - the CAT catalogue.

<span id="g-shifted-keys"></span>

## g-shifted keys

As with the unshifted keys, these do what their keylabels say, or at
least something closely related.

  - <span class="button">g</span> <span class="button">Σ+</span> ,
    <span class="button">1/x</span> , <span class="button">√</span> ,
    and <span class="button">LOG</span> - unshifted functions of these
    keys, for use when labels A-D are defined.
  - <span class="button">g</span> <span class="button">RCL</span> -
    View.
  - <span class="button">g</span> <span class="button">SIN</span> ,
    <span class="button">COS</span> , <span class="button">TAN</span> -
    Angles menu (also available from <span class="button">f</span>
    <span class="button">MODES</span>).
  - <span class="button">g</span> <span class="button">ENTER</span> -
    Fill stack with X-register value.
  - <span class="button">g</span> <span class="button">X\<\>Y</span> -
    the X\<\>Y menu. Includes the three functions on this key on the
    original calculator.
  - <span class="button">g</span> <span class="button">MODES</span> -
    MODE catalogue.
  - <span class="button">g</span> <span class="button">MATRIX</span> -
    SUMS catalogue.
  - <span class="button">g</span> <span class="button">STAT</span> -
    STAT catalogue.
  - <span class="button">g</span> <span class="button">CONVERT</span> -
    CONST catalogue.
  - <span class="button">g</span> <span class="button">FLAGS</span> -
    TEST catalogue.
  - <span class="button">g</span> <span class="button">PROB</span> -
    PROB catalogue.
  - <span class="button">g</span> <span class="button">CUSTOM</span> -
    menu to choose user menu.
  - <span class="button">g</span> <span class="button">PGM.FCN</span> -
    X.FCN catalogue.
  - <span class="button">g</span> <span class="button">EXIT</span> -
    SHOW catalogue - same location as on the original calculator.
  - <span class="button">g</span> <span class="button">SETUP</span> -
    Setup 2 menu.
  - <span class="button">g</span> <span class="button">PRGM</span> -
    Program function menu.
  - <span class="button">g</span> <span class="button">CATALOG</span> -
    STATUS.

<span id="alpha-mode"></span>

## Alpha mode

To enter alpha mode press <span class="button">f</span>
<span class="button">ALPHA</span> on the
<span class="button">ENTER</span> key.  
To leave, press either <span class="button">ENTER</span>,
<span class="button">f</span> <span class="button">ALPHA</span>, or
<span class="button">EXIT</span>.  
In alpha mode each DM42 key produces the letter or symbol printed on the
DM42 faceplate to the right of the key. This carries over to the Greek
letters - for example, Greek lambda is accessed from the
<span class="button">TAN</span> key (L) rather than the
<span class="button">E</span> key (exponent) (which is L on the
WP34S).  
Apart from this key re-arrangement, alpha mode works in the same way as
on the real WP34S, in the sense that equivalent keys give the same
characters.  
h-shift (3 presses of the shift key) is used in alpha mode.  
A few extra characters have been linked to the keyboard for
convenience.  
For a much fuller description of alpha mode in this program, see the
file **help/Using.md**. 

<span id="working-with-files"></span>

## Working with files

The memory of the original WP34S included 2kB of RAM (user memory) for
the calculator state and 128kB of flash memory.  
The firmware filled most of the flash. Of the remainder, 2kB was used as
a backup region for the calculator state and what was left - the library
- could store user programs.  
  Programs in the backup region or in the library can be run without
  loading them into the 2kB calculator state.  
  This program stores user memory (2kB or 4kB), backup memory (2kB or 4kB)
  and library memory (16kB) in RAM rather than in flash memory.  
  Barring crashes, the content of RAM on the DM42 is permanent until the
  WP34S program is exited (e.g., to re-load Free42); it **may** even
  survive battery replacement.  
  In particular, RAM persists when the calculator is turned off.  
  To provide storage that survives even when the firmware is exited, user
  memory, backup memory, and library memory are mirrored to a set of
  files. These files live in wp34s/ which is created automatically if it
  doesn't exist.  
  The files are:

  - **wp34s.dat** - an image of user memory. Since the RAM contents
    survive when the calculator is turned off, this is **only** written
    when the EXIT button in the Setup 1 menu is pressed.
  - **wp34s-backup.dat** - in the Setup menu the function
    <span class="lcd">OnSTO</span> writes a copy of user memory to
    backup memory and also to this file. (On the original WP34S pressing
    "On" and "STO" together had the same function.)  
    The function <span class="lcd">OnRCL</span> from the same menu
    restores user memory from the backup.
  - **wp34s-lib.dat** - the functions <span class="lcd">PSTO</span> and
    <span class="lcd">PRCL</span> in the P.FCN catalogue read and write
    the current program to and from library memory. When this happens,
    this file is updated too.

  

To sum up:

  - While the calculator is on, backup and library memory are mirrored
    to disk.
  - When the calculator is switched off and on, no files are written or
    read.
  - When the WP34S program is exited using the EXIT button in the SETUP
    1 menu, user memory is written to disk.
  - When the WP34S firmware is loaded or run from the DMCP menu (options
    3 and 2) user, backup, and library memory is reloaded from these
    three files.

The other commands in the Setup and Setup 2 menus are:

  - <span class="lcd">EXIT</span> - exits to the DMCP menu. Saves the
    calculator state.
  - <span class="lcd">System</span> - access the System menu, activate
    USB disk, About.
  - <span class="lcd">HELP</span> - opens this file.
  - <span class="lcd">SvRAM</span> (Save RAM) - writes a copy of user
    memory to a named file (with a .dat extension).
  - <span class="lcd">LdRAM</span> (Load RAM) - loads a named file (with
    a .dat extension) into user memory amd saves its current contents to
    a backup file.
  - <span class="lcd">PrvRAM</span> (Previous RAM) - loads the backup
    file produced by <span class="lcd">LdRAM</span> into user memory.
  - <span class="lcd">SvLIB</span> (Save Library) - writes a copy of
    library memory to a named file (with a .lib extension).
  - <span class="lcd">LdLIB</span> (Load Library) - loads a named file
    (with a .lib extension) into library memory and saves its current
    contents to a backup file.
  - <span class="lcd">PrvLIB</span> (Previous LIB) - loads the backup
    file produced by <span class="lcd">LdLIB</span> into user memory.
  - <span class="lcd">SvCPRG</span> (Save current program) - writes a
    copy of the current program to a named file. Note that the current
    program must begin with a global label (i.e., LBL'A' or LBL'CAT',
    not LBL A or LBL 01).
  - <span class="lcd">LdPRG</span> (Load program) - load a named program
    file into the flash region, from where it can be added to the
    calculator state for editing with the PRCL command.

IMPORTANT: The library and program files are not human-readable. They
contain the opcodes that make up the program or the library, along with
size and checksum information.  
To make them human-readable you need to disassemble them with the
tremendously versatile WP34S assembler suite of tools.  
If you want the files to be portable to other systems, this is vital:
the op-codes in the WP34S can change when the number of commands alters,
making the files partially or completely unreadable.  
The correct opcodes for this system are found in the file
src/tools/wp34s.op, along with the assembler.  
Not only are the disassembled files human-readable, they can be
re-assembled correctly when the op-codes change.  
Transferring files as source listings is the only reliable way to get
them from one WP34S system to another, unless both use the same
op-codes.  
There is a **lot** more information about the file commands and using
the assembler in the file **help/Using.md**.

## IR Printing

This program allows printing to an HP-82240A or B printer via the DM42's
IR hardware. (Print mode 0 may not work on the HP-82240A.)  
The commands behave as documented in the WP34S manual, except as
mentioned below. Note that <span class="lcd">P.</span> represents the
printer icon.  
Many commands are found in the PRINT menu, <span class="button">f</span>
<span class="button">PRINT</span> .

  - <span class="lcd">P.ON</span> and <span class="lcd">P.OFF</span>
    enable and disable printing.
  - <span class="lcd">P.DblSp</span> takes an argument: 1 turns on
    double line spacing, 0 turns it off.
  - <span class="lcd">P.Mode</span> can be 0 (use printer characters
    where possible), 1 (WP34S characters as graphics), or 2 (really
    small WP34S graphic characters). Mode 3 (Serial) is not supported.
  - <span class="lcd">P.Stk</span> and <span class="lcd">P.Regs</span>
    print the stack and a block of registers. Prefixed by CPX, they
    format the register contents as complex numbers.
  - In complex lock mode <span class="lcd">P.Stk</span> and
    <span class="lcd">P.Regs</span> print in complex form automatically.
    They also respect the RECT or POLAR display mode setting.

Printing commands not in this menu can be found in the catalogues.
<span id="the-stopwatch"></span>

## The stopwatch

The stopwatch can be activated with the command
<span class="lcd">STOPW</span> in the X.FCN catalogue.  
If you use it frequently, adding it to a [user-defined
menu](#user-defined-menus) might be helpful.  
All the stopwatch commands in the WP34S manual work as described.  
When the stopwatch is running, the <span class="lcd">=</span>
annunciator shows outside of stopwatch mode but does not blink.  
The stopwatch uses the real-time clock. So switching the calculator off
does not affect it, as the real-time clock still runs.  
With the running stopwatch displayed, the calculator will not switch off
automatically.  
With the stopwatch "running" in the background, the calculator will
switch off in the usual way. 

<span id="user-defined-menus"></span>

## User-defined menus

The menus in this program are a necessity, but have also proved to be a
virtue. I find the "program" menu <span class="button">g</span>
<span class="button">PGRM</span> particularly convenient, and others
agree.  
Because of this, the system lets you define your own menus. These can
contain any WP34S command that can be entered as a program step.  
The idea is to write a program containing the commands you want in your
menu. This program must start with a global label.  
Then the command <span class="lcd">UMEN</span> (found in the menu
<span class="button">g</span> <span class="button">CUSTOM</span> ) takes
the label as an argument and displays the menu.  
The functions <span class="lcd">UM1</span> , ...,
<span class="lcd">UM6</span> provide quick access to menus labelled UM1,
..., UM6.  
<span class="button">f</span> <span class="button">CUSTOM</span> returns
to the current user menu.  
For more details please see the documentation in help/ on the gitlab
site. 

<span id="ram-and-lib-contents-menu-generator"></span>

## RAM and LIB contents menu generator

The <span class="lcd">RAM</span> and <span class="lcd">LIB</span>
commands are found in the "Select user menu" menu
(<span class="button">g</span> <span class="button">CUSTOM</span>).  
These generate a menu showing the first twelve global labels in RAM or
in flash memory (LIB). Pressing any of the menu keys executes code
starting from that label.  
If there are more than twelve global labels, pressing
<span class="button">▼</span> while the RAM or LIB menu is displayed
moves to the next twelve labels.  
Pressing <span class="button">▲</span> while the RAM or LIB menu is
displayed moves back to the start of memory.  
You may not want a particular global label to appear in the menu. If the
first command following the global label is NOP (no operation – it’s in
the P.FCN catalogue) then the label is skipped. (You can insert NOP at
the start of a user-defined menu without causing trouble.)

<span id="entry-rpn"></span>

## Entry RPN

Normally, typing <span class="button">2</span>
<span class="button">ENTER</span> puts 2 in the X and Y registers, and
pushes the top register off the stack.  
In Entry RPN mode, typing <span class="button">2</span>
<span class="button">ENTER</span> puts 2 in the X register and leaves
the rest of the stack unchanged.  
If this is for you, you can enable or disable Entry RPN mode with the
commands <span class="lcd">eRPon</span> and
<span class="lcd">eRPoff</span> , found in the
<span class="button">g</span> <span class="button">MODES</span> menu.  
More details in the documentation in help/.

<span id="complex-lock-mode"></span>

## Complex Lock Mode

The WP34S offers excellent support for complex numbers. Complex Lock
mode aims to make the entry, display, and use of such numbers more
straightforward.  
For more details, refer to the documentation in help/. Only the basics
are outlined below.

  - You must execute <span class="lcd">CPXYES</span> in the
    <span class="button">MODE</span> catalogue before you can enter
    complex lock mode.
  - Having done this, pressing <span class="lcd">→</span> then
    <span class="lcd">CPX</span> enters complex lock mode. The C\_LK
    menu is then displayed.
  - To enter the number (2+3i), type <span class="button">2</span>
    <span class="lcd">CPX</span> <span class="button">3</span>
    <span class="button">ENTER</span> . Note the helpful prompts.
  - Pressing <span class="button">x<sup>2</sup></span> will square this
    number without the need to press the <span class="lcd">CPX</span>
    key. In complex lock mode, functions act on complex numbers by
    default.
  - Now press <span class="lcd">POLAR</span> from the C\_LK menu. This
    displays the result in polar form.
  - Now, pressing <span class="button">4</span>
    <span class="lcd">CPX</span> <span class="button">4</span>
    <span class="button">5</span> <span class="button">/</span> will
    divide the result by the complex number with modulus 4 and argument
    45. Again, note the helpful prompts.
  - You can exit complex lock mode by pressing
    <span class="lcd">→</span> <span class="lcd">CPX</span> . If you
    will not be returning you can execute
    <span class="lcd">CPXNO</span> .

In C\_LK mode, some menus are modified or completely absent. There are
no programming functions, statistics, matrix, or integer commands. This
is a limitation, but it hasn't proved a problem in the past.

<span id="user-help-files"></span>

## User Help files

A user-written html help file can be opened and viewed by first opening
the main help file and then pressing F3 (this is a new DMCP feature).  
This allows a user to document their own (or other people's) programs
without trying to cram information into the limited display space
available on the WP34S.  
To some extent user help files can be opened under program control. For
details of what can be done see **help/Using.md**.
<span id="integration-functions"></span>

## Integration functions

There are now several integration functions available from the Integrate
menu.

  - <span class="lcd">∫old</span> is the previous integration function,
    based on the Double Exponential algorithm.
  - <span class="lcd">∫dx</span>; is the new integration function, based
    on an updated version of the Double Exponential algorithm.
  - <span class="lcd">∫QAG</span> uses the QAGS and QAGI adaptive
    integration routines from the GNU Scientific Library.
  - <span class="lcd">∫ROM</span> is a Romberg integration routine
    similar to that used by Free42.

Both <span class="lcd">∫dx</span> and <span class="lcd">∫QAG</span>
behave differently depending on the state of Flag A.  
When Flag A is clear, all routines expect the lower limit in Y and the
upper limit in X, just like the previous routine did.  
When Flag A is set additional options are available for the new
routines. These are described in the help file documentation.  
For convenience, a menu function <span class="lcd">TOG\_A</span> is
provided to toggle the state of Flag A. When set, a large equals sign is
visible.  
Much more information is available in the
**help/Integration\_routines.\*** files.
