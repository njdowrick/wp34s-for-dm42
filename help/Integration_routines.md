## Introduction

[Back to README](../README.md#description)

The WP34S calculator uses the Double Exponential method to evaluate numerical integrals. A [discussion](https://www.hpmuseum.org/forum/thread-16549.html) on the HP forum some time ago alerted me to an updated version of this. I considered replacing the existing WP34S routine, but I found that writing XROM code on this scale was beyond me. However, writing the new routine in C is straightforward. This works on the DM42 hardware, but for reasons of size it can’t really be backported to the physical machine.
While doing this I took the opportunity to add a couple of other integration routines, described below.

While doing this I took the opportunity to add a couple of other integration routines, described below.

Which should you use? For integrals with no singularities inside the range of integration, use the standard routine (EI2). If there are (integrable) singularities within the range, use QAG. If all else fails, use Romberg!

## The new Double Exponential Integral (EI2) routine

This replaces the old routine. It appears as **ʃ dx** in the
Integrate menu, on the shifted-8 key. In tests it proves to be as good
as or better than the previous routine (which has been left in place as
**ʃ old** in the same menu).

Under the hood, the new code is taken from the “final version of the
improved QUAD routine”, which appears in the document
[qthsh.pdf](https://www.genivia.com/files/qthsh.pdf). This runs a little
faster, and requires fewer function evaluations in some cases for the
same precision. The code also uses a routine from the above document to
generate an “optimised Exp-Sinh integral split point” which improves
integration when one limit is infinite. More details are given in the
above document.

Apart from improvements in speed and accuracy, there are a couple of other
differences between the two routines. First, EI2 sets Flag D so
that infinite results and NaNs don’t stop program execution, and
restores the original flag state on completion. Second, the state of Flag A
affects how the integration routine behaves:

**With Flag A clear:** EI2 acts exactly like the original
double exponential integration routine so far as its interface is
concerned. It takes the limits of integration from the stack, and the
label of the routine to be integrated from its argument. It infers the
precision required from the display mode. It returns the result and the
estimated error to the X and Y registers respectively.

**With Flag A set:** EI2 now takes three arguments, a, c, and b,
from stack levels Z, Y, and X respectively. a and b are the limits of
integration; c is a “split point” with a meaning that changes depending
on whether a and b are finite or infinite. Note: a \< c \< b.

Note that there is a function TOG\_A in the Integration menu, which
toggles the state of Flag A.

**a, b both finite:** The double exponential integration algorithm copes
well with integrable singularities at the limits of integration, but it
does less well with integrable singularities or discontinuities within
the range of integration. It is sensible to break such integrals into
two or more pieces and then add the separate results together. When both
limits of integration are finite and only one break point is needed, the
use of Flag A provides a short-cut.

Suppose we are evaluating $\int_{-2}^{3}x^{-1/3}\,\text{d}x$
and (for some reason) we are using a calculator to do it. This integral
needs to be split at $x = 0$, a singular point. To do this:

  - Set flag A (a big “=” sign should appear on the display);

  - Put -2, 0, 3 on the stack in that order;

  - Press ʃ dx.

The two integrals (from -2 to 0, and from 0 to 3) are evaluated
separately and then added together.

**One of a, b infinite:** When one limit is infinite, the double
exponential routine divides the range of integration into two parts. For
example, an integral from a to infinity might be divided into an
integral from a to a + 1, plus an integral from a + 1 to infinity. These
two integrals are evaluated with the same number of sample points. The
position of the split point affects the number of sample points needed
to reach a given precision.

With Flag A clear, the split point (a + 1 above) is chosen by the new
code automatically. This works well, and normally there is no need to
change it. However, to see the effect of different split points you can:

  - Set flag A (a big “=” sign should appear on the display);

  - Put a, c, b on the stack in that order, where c is the desired split
    point and either a or b is infinite;

  - Call ʃ dx.

**a, b both infinite:** If both limits are infinite, the double
exponential routine splits the range of integration into (-∞, 0) and (0,
∞), evaluating each part with the same number of sample points. This
works well when the function is roughly symmetric about zero. If it
isn’t, EI2 allows the choice of a new split point for the
integration, as follows:

  - Set flag A;

  - Put -∞, c, ∞ on the stack in that order, where c is the desired
    split / centre point for the integration;

  - Call ʃ dx.

Since there is no automatic choice of the optimum centre point this
option is likely to be useful, at least on occasion.

**A few final points:**

  - Unlike the original integration routine, the new routine can be
    included in a program.

  - Using SSIZE 8 is sensible, at least when Flag A is set. The code
    returns two results, and keeps the original three arguments in the
    stack for re-use. If SSIZE 4 is set, the limit a will end up in
    register A rather than stack level A.

  - The routine sets flag D automatically and then restores its previous
    state. (The original routine did not do this.) This means that
    infinite results or NaNs from the function being integrated won’t
    stop the program. This is normally a good idea\!

  - The user routine to be integrated is called with *x* in X, Y, Z, and
    T. Even with SSIZE 8, *x* is not copied into the top four levels.
    This speeds things up a little.

**Display modes**

The Double Exponential Integral routine takes the precision to aim for
from the display mode. It aims to achieve
$$
\left| I - I_{\text{exact}} \right| < \epsilon_{\text{rel}}I_{\text{exact}},
$$
where $\epsilon_{\text{rel}}$ is the relative precision.

| **Display mode**            | Relative precision |
| --------------------------- | ------------------ |
| Single precision, standard: |    $10^{-15}$    |
| Single precision, FIX/SCI/ENG/SF n: |$10^{-n}$ |
| Double precision, any mode: |    $10^{-34}$    |

**The QAG integration routine**

(QAG stands for Quadrature (integration) Adaptive Gaussian.) This is an adaptive Gauss-Konrod routine. It starts with a single
interval and uses a Gauss-Konrod 21-point rule to estimate the integral
over that interval, along with an error. If the error is not small
enough it bisects the interval and repeats the process. At each step it
bisects the interval which has the highest error; every so often it uses
an Wynn-epsilon extrapolation scheme to improve accuracy. All of the
routines used come from the Gnu Scientific Library and are written in C.
I have not modified them to use decNumbers rather than C doubles; this
limits the precision to 13-14 significant figures but it does increase the speed a little.

The routines used are QAGS, which copes with integrable singularities
inside and at the ends of the range of integration, and QAGI, which
copes with infinite limits by a change of variables and then uses QAGS.

The routine keeps track of every interval, which takes memory. The
maximum number of intervals is limited to 200, which is normally plenty
(but see the “scale” parameter, discussed below).

Is it better than the Double Exponential routine? In some cases, yes: it
copes well with singularities within the range of integration, and its
adaptive nature allows it to concentrate on regions where a lot is going
on. Repeated bisection is not necessarily fast, though. By default I
would use the standard routine, but it is nice to have this routine
available.

**Flag D:**

As with the previous routine, Flag D is turned on automatically when
this code is run. This prevents infinite and NaN results from stopping
code execution.

**Flag A:**

For integrals over finite ranges, flag A has no effect. Because QAGS can
cope with singularities within its range of integration there is no need
for a “split point”.

For integrals with one or two infinite limits, setting flag A allows the
user to supply an extra “scale” parameter, *s*. (Note the menu function
TOG\_A in the Integration menu, which toggles the state of Flag A.)

Take the case of an integral from $x = a$ to $x = + \infty$. The
routine changes this to an integral over a variable *t* from $t = 0$
to $t = 1$, where

$$x = a + {1 - t\over t}.$$

So when $t = \frac{1}{2}$, then $x = a + 1$; when
$t = \frac{1}{4}$, $x = a + 3$, etc. This is fine so long as the scale on
which the integrand varies is not too different from 1. But this isn’t
always the case – e.g., integrating
$\exp(-x/10^6)$ from 0 to ∞, or
$9.0 \times 10^{9} \times \left( 1.6 \times 10^{- 19} \right){\vphantom{10}}^2 / x^2$ from $1.0 \times 10^{- 10}$
to infinity.

In both examples it takes a long time for the routine to find its way to
the values of *x* that matter – up around $x \sim 10^{6}$ in the first
case, and down close to $x \sim 10^{- 10}$ in the second case,
although in both cases it gets there in the end so long as the correct
display mode is chosen (see below).

The scale parameter lets us give a hint to the routine as to the
appropriate scale of the problem. It does this by changing the above
formula to

$$x = a + s \times \left(1 - t\over t\right),$$

where *s* is the scale. So this time, when
$t = \frac{1}{2}$, $x$ is $a + s$.

To use the scale factor:

  - Set Flag A – a big “=” sign appears in the display.

  - Load the stack with the scale in Z, the lower limit *a* in Y, and
    the upper limit *b* in X.

  - Run QAG.

**Display modes**

The QAG routines take two precision arguments – one absolute, and one
relative. They aim to achieve either
$$\left| I - I_{\text{exact}} \right| < \epsilon_{\text{abs}}$$
or
$$\left| I - I_{\text{exact}} \right| < \epsilon_{\text{rel}}I_{\text{exact}}.$$
The values of these two arguments are taken from the display mode of the
WP34S when QAG is called, according to these rules:

| **Display mode** | Relative precision | Absolute precision |
| ---------------- | ------------------ | ------------------ |
| Standard         |$10^{-13}$        |0                 |
| Fix *n*          |$10^{-13}$        |$10^{-n}$       |
| Sci/Eng/SF n     |$10^{-n}$         |0                 |

Either Standard or Sci/Eng/Fix modes are fine for most integrals, unless
the integral might be zero. If you think that an integral might be zero
and you want to check, make sure that the integrand is of the order of
one (e.g., by omitting any large or small constants multiplying it) and
use a fixed decimal mode.

**Error Messages**

Lots of things can go wrong\! If they do, a brief message is displayed
when the routine exits. Here are (some?) of the possibilities. (The
numbered error messages are from the GSL code.)

| **Error message** | **Meaning**                                                                   |
| ----------------- | ----------------------------------------------------------------------------- |
| scale0\!          | Scale of zero is not allowed.                                                 |
| \!Malloc          | Memory allocation for the table of intervals failed. Shouldn’t happen, but... |
| Err: 18           | Failed to achieve tolerance due to roundoff error.                            |
| Err: 32           | Backspace pressed; break.                                                     |
| Err: 11           | Maximum number of iterations exceeded.                                        |
| Err: 21           | Bad integrand behaviour.                                                      |
| Err: 22           | Integral is divergent, or converges too slowly.                               |
| Err: 05           | Generic failure. Just couldn’t do it, but don’t know why\!                    |
| Err: 08           | Malloc failure again.                                                         |

**The Romberg Integration Routine**

This, so far as I can tell, is similar to the code used by the
integration routine in the HP-15C, and also in Free42. Like these, this
routine does not sample from the endpoints (so that it can cope with
integrable singularities there) and it performs a non-linear
transformation on the variable of integration so that “aliasing” will
not occur with periodic functions. It is, however, considerably slower
than the similar routine in Free42 due, so far as I can tell, to the use
of the decNumber library in the WP34S code rather than the Intel Decimal
library for Free 42.

The weakness of this routine is that its samples are spread over the
whole integration range rather than being concentrated on challenging
areas (as with QAGS). This is also its strength – slow, but sure.

The code is based on routines from the book Numerical Recipes in FORTRAN
(2<sup>nd</sup> edition) and from the Gnu Scientific Library. It shares
with QAGS the effect of Flag A with its scale parameter, and the effect
of display settings on precision and accuracy. As with the other
routines, it can be interrupted by pressing backspace.

[Back to top](#introduction)
